<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//d($this);
?>
<div id="container">
	<h1>List of the new features / libraries integrated with codeigniter</h1>
	<div id="body">
		<ol>
			<li>Location of <a>index.php</a> changed to public folder.(new CI-4 way)
			<li><a>APP_URL</a> constant added in index.php / url rewrite ready.
			<li><a>CI_ENV</a> set on .htaccess (SetEnvIf Host localhost$ CI_ENV=development)
			<li><a>HMVC</a>
			<li><a>composer</a> ready.
			<li><a>kint</a> installation done and ready to use.
			<li><a>whoops</a> installation done and ready to use.
			<li>Location of <a>Assets</a> folder changed to public folder (public/assets).
			<li><a>assest_helper, _form_helper</a> added.
			<li><a>CLI feature</a> added, you can run application via CLI (system/CI) (new CI-4 way)
			<li><a>Debugbar integrated</a> and activated.
			<li><a>MY_Controller integrated</a> and activated with <a>_remap</a> method.(<a> header.php, body.php, footer.php </a>)
			<li>Ajax integrated and ready play.<button id='btn_ajax'>Check ajax</button>
			<div class="response"></div>
		</ol>
	</div>

<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>

</div>
<div id="container">
	<h1>Site Contstants</h1>
	<div id="body">
		<ol>
			<li>ENVIRONMENT = <?= anchor_popup(ENVIRONMENT,ENVIRONMENT); ?>
			<li>FCPATH = <?= anchor_popup(FCPATH,FCPATH); ?>
			<li>SELF = <?= anchor_popup(SELF,SELF); ?>
			<li>BASEPATH = <?= anchor_popup(BASEPATH,BASEPATH); ?>
			<li>SYSDIR = <?= anchor_popup(SYSDIR,SYSDIR); ?>
			<li>APPPATH = <?= anchor_popup(APPPATH,APPPATH); ?>
			<li>VIEWPATH = <?= anchor_popup(VIEWPATH,VIEWPATH); ?>
			<li>APP_URL = <?= anchor_popup(APP_URL,APP_URL); ?>
			<li>ASSET_URL = <?= anchor_popup(ASSET_URL,ASSET_URL); ?>
			<li>IMG_URL = <?= anchor_popup(IMG_URL,IMG_URL); ?>
			<li>CSS_URL = <?= anchor_popup(CSS_URL,CSS_URL); ?>
			<li>JS_URL = <?= anchor_popup(JS_URL,JS_URL); ?>
			<li>UPLOAD_URL = <?= anchor_popup(UPLOAD_URL,UPLOAD_URL); ?>
		</ol>
	</div>
</div>

