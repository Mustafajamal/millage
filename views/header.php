<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	 <?php echo $css; ?>
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}
li{
	font-size:16px;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	color: #4F5155;
	margin: 0 0 14px 0;
	}
	a {
		color: #ff6666;
		background-color: transparent;
		font-weight: bold;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 22px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
		font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
	<script src="<?php echo JS_URL."jquery-3.2.1.min.js"?>" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo JS_URL."ajax.js"?>" type="text/javascript" charset="utf-8"></script>
</head>
<body>
	<div class="loader loading-image" id="before" style="display: none;">
		<img src="<?php echo base_url('public/assets/images/ajax-spinner.gif'); ?>" height="50%" width="50%">
	</div>