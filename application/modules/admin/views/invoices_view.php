<link rel="stylesheet" type="text/css" href="<?php echo ASSET_URL.'invoice/bootstrap.min.css' ?>">
<link rel="stylesheet" type="text/css" href="<?php echo ASSET_URL.'invoice/print.css' ?>">
<link rel="stylesheet" type="text/css" href="<?php echo ASSET_URL.'invoice/font-awesome.min.css' ?>">
<script src="<?php echo ASSET_URL.'invoice/jQuery.print.js' ?>"></script>
<script src="<?php echo ASSET_URL.'invoice/jQuery-2.1.4.min.js' ?>"></script>
<div class="container-fluid invoice-box">
    <div class="row " id="print_data">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="row">
            <div class="col-md-12">
              <div class="box-header with-border">
                <center>
                  <strong>
                    <img src="<?php echo UPLOAD_URL.'site/'.get_site_info("site_logo"); ?>" class='img img-thumbnail' height="100px" width="100px"/>
                    <h4><?php echo get_site_info("site_title"); ?></h4>
                  </strong>
                </center> 
              </div>
                <hr>
              <div class="box-body">
                <section class="invoice">
                  <!-- info row -->
                  <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                      <b>To,</b>
                      <address>
                        <?php 
                            $customer_data=$this->custom->getSingleRow("users",array("id"=>$order_data[0]->customer_user_id),'array'); 
                            $cust_data=$this->custom->getSingleRow("customer_data",array("user_id"=>$customer_data['id']),'array'); 

                            echo $customer_data['first_name']." ".$customer_data['last_name'];
                         ?>
                        <br><br>
                        <b>Address:</b><br><?php echo $cust_data['customer_address']; ?>,<br>
                        <?php echo $cust_data['customer_city']; ?><br>
                        <?php echo $cust_data['customer_state']; ?><br>
                        <?php echo $cust_data['custoemer_zip_code']; ?><br>
                        <b>Phone:</b> <?php echo $customer_data['phone']; ?><br>
                        <b>Email:</b> <?php echo $customer_data['email']; ?>
                      </address>
                    </div>
                    <div class="col-sm-4 invoice-col"></div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                      
                      <b>Reciver Name :</b> <?php echo $cust_data['customer_ship_name']; ?><br>
                      <br>
                      <b>Address: </b><br><?php echo $cust_data['customer_ship_address']; ?>,<br>
                      <?php echo $cust_data['customer_ship_city']; ?><br>
                        <?php echo $cust_data['customer_ship_state']; ?><br>
                        <?php echo $cust_data['custoemer_ship_zip_code']; ?><br>
                      <b>Phone: </b> <?php echo $cust_data['customer_ship_phone']; ?><br>
                      <b>Email: </b> <?php echo $cust_data['customer_ship_email']; ?><br>
                      <b>Date: </b> <?php echo date('d-m-Y'); ?><br>
                      
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                  <br>
                  <!-- <legend></legend> -->
                  <div class="row">
                    <div class="col-xs-12">
                      <strong> Order Status : </strong><?php echo $order_data[0]->order_status; ?>
                    </div>
                  </div>
                  <br>
                  <!-- <legend></legend> -->
                  <div class="row">
                    <div class="col-xs-12 col-md-5">
                    </div>
                  </div>
                  <br>
                  <!-- <legend></legend> -->
                  <!-- Table row -->
                  <div class="row">
                   <div class="col-xs-12">
                      <strong> Items on Order </strong>
                    </div>
                    <div class="col-xs-12 table-responsive">
                      <table class="table table-striped table-bordered" id="product_table">
                       <thead>
                          <tr>
                            <th style="text-align: center;">Order NO</th>
                            <th style="text-align: center;">SERIES</th>
                            <th style="text-align: center;">PRODUCT NAME</th>
                            <th style="text-align: center;">COLOR</th>
                            <th style="text-align: center;">QUENTITY </th>
                            <th style="text-align: center;">PRICE</th>
                            <th style="text-align: center;">TOTAL </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $orders=array();
                            $i=0;
                          foreach ($order_data as $order) {
                              $watch_option=$this->custom->getSingleRow("watch_options_master",array('watch_option_id'=>$order->watch_option_id));
                              $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_option->watch_id));
                              $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
                              $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
                              $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
                              if (!in_array($order->order_id,$orders)) { 
                                  $orders[]=$order->order_id;
                                  $i++;
                                  
                                  if ($i!=1) {
                                                                         
                                  }
                                   ?>
                      
                        <tr>
                          <td><?php echo strtotime($order->order_date);?></td>
                          <td><?php echo $series_data; ?></td>
                          <td><?php echo $product_data->product_name; ?></td>
                          <td><?php echo $watch_option->dial_color; ?></td>
                          <td><?php echo $order->quantity; ?></td>
                          <td><?php  
                                if($order_data[0]->user_group_id==4)
                                  { $price=$watch_option->retail_price;}
                                if($order_data[0]->user_group_id==3)
                                  { $price=$watch_option->dealer_price;}
                                if($order_data[0]->user_group_id==2)
                                  { $price=$watch_option->distributor_price;}
                                echo $price;
                            ?></td>
                          <td><?php echo $t[]=$order->quantity*$price ?></td>
                           </tr>
              <?php }else{ ?>
              <tr>
             <td><?php echo strtotime($order->order_date);?></td>
                          <td><?php echo $series_data; ?></td>
                          <td><?php echo $product_data->product_name; ?></td>
                          <td><?php echo $watch_option->dial_color; ?></td>
                          <td><?php echo $order->quantity; ?></td>
                          <td><?php  
                                if($order_data[0]->user_group_id==4)
                                  { $price=$watch_option->retail_price;}
                                if($order_data[0]->user_group_id==3)
                                  { $price=$watch_option->dealer_price;}
                                if($order_data[0]->user_group_id==2)
                                  { $price=$watch_option->distributor_price;}
                                echo $price;
                            ?></td>
                          <td><?php echo $t[]=$order->quantity*$price ?></td>  
                          </tr>  
              <?php } ?>
          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                  <div class="row">
                    <!-- accepted payments column -->
                    <!-- /.col -->

                    <div class="col-md-5 col-xs-12" style="margin-left: 50%;width: 50% ">
                      <div class="table-responsive">
                        <table class="table">
                          <tbody>
                            
                            <tr>
                              <th>Item Total:</th>
                              
                              <td class=""><span class="pull-right">$<?php echo number_format(array_sum($t),2);  ?></span></td>
                            </tr>
                            <tr>
                              <th>Shipping Cost:</th>
                             
                              <td class=""><span class="pull-right">$<?php echo number_format($ship_cost=0.0,2);  ?></span></td>
                            </tr><tr>
                              <th>Total Charges:</th>
                              
                              <td class=""><span class="pull-right">$<?php echo number_format(array_sum($t)+$ship_cost,2);  ?></span></td>
                            </tr>
                           
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                  
                   <!-- /.row -->
                  <div class="row">
                    <!-- accepted payments column -->
                    <!-- /.col -->
                    <div class="col-md-3 col-xs-12">
                      <div class="table-responsive">
                              <legend></legend>
                              Customer Signature and Co Stamp<br>
                              Name: <br>
                              Date: <br>
                        </table>
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- this row will not appear when printing -->
                  
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
 <div class="container">
    <div class="pull-right">
      <button class="btn btn-primary" id="print" onclick="print_div()"> Print</button> 
    </div>
</div>
        <br><br>
<script type="text/javascript">
    function print_div() {
        $('#print').hide();
        window.print();
    }
</script>