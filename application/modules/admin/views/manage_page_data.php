<style type="text/css">
  .selected{
    background-color: #C8C8C8 !important;
  }
  .mce-notification,.mce-branding-powered-by{
    display: none;
  }  
</style>
<script src="http://fl.cijagani.in/ci_millage/plugins/ckeditor/ckeditor.js"></script>
<section class="content-header">
  <h1>
    Manage Page Data
    <!-- <small>Preview of UI elements</small> -->
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div id="message_area">
        <?php get_flash_message('message'); ?>
      </div>
      <?php
        echo get_buttons(); 
        echo add_custom_button("Enable","enable",'btn bg-teal','check',TRUE);
        echo add_custom_button("Disable","disable",'btn bg-maroon','ban',TRUE);
      ?>
      <br>
      <br>
      <div class="box box-warning">
        <div class="box-body">
          <div id="list_table">
                	<table class="table " id="datatable" width="100%">
                  		<thead>
                    		<tr>
                     			  <th>Id</th>
                      			<th>Category Name</th>
                            <th>Section Name</th>
                            <th>Status</th>
                            <th>Link</th>
                    		</tr>
                  		</thead>
                	</table>
              	</div>
              	<form autocomplete="off" method="post" action="#" enctype="multipart/form-data" class="validate">
                	<div id="form_data"></div>
              	</form>
         </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
      $(function() {
        base_url="<?php echo base_url(); ?>";
        class_name="manage_page_data";
        manage_buttons(base_url,class_name);
        url="<?php echo base_url('common/datatable/ajax_list/manage_page_data'); ?>"
        table=get_datatable(url,[],0,'ASC',[4]);

      });
    </script>