  <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8 col-md-offset-2">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Site Setting</h3>
            </div>
            <!-- /.box-header -->

 <div class="box-body">

      <?php
        $attributes = array('class' => 'validate', 'id' => 'site_setting_form','method'=>"post");
        echo form_open_multipart($save_url,$attributes);
      ?>

        <!-- <form action="#" method="post" id="login_form" class="validate"> -->


            <?php
               $site_title=array("name"=>"site_title","id"=>"site_title","class"=>"form-control","placeholder"=>"Site Title","required"=>"");
                echo form_input($site_title,get_site_info('site_title'),"Site Title");
               $site_logo=array("name"=>"site_logo","id"=>"site_logo","class"=>"form-control");
                echo _form_common("file",$site_logo,'','Site Logo');
            ?>
              <img src="<?php echo UPLOAD_URL.'site/'.get_site_info('site_logo'); ?>" height=100px width=100px>
              
              <?php
                $slider_images=array("name"=>"slider_images[]","id"=>"site_logo","class"=>"form-control","multiple"=>"multiple");
                echo _form_common("file",$slider_images,"","Slider Images <span class='label label-default'>Image Size:1920px X 700px </span>");

            $slider_images=explode(",",get_site_info('slider_images'));

            foreach($slider_images as $si)
            {
            ?>
            <img src="<?php echo UPLOAD_URL.'site/'."$si"; ?>" height=100px width=100px>
            <?php
            }

              $footer_image=array("name"=>"footer_image","id"=>"footer_image","class"=>"form-control");
                echo _form_common("file",$footer_image,'','Footer Image <span class="label label-default">Image Size:1349px X 461px </span>');
            ?>

          <img src="<?php echo UPLOAD_URL.'site/'.get_site_info('footer_image'); ?>" height=100px width=100px>
  
          <div class="row">

            <div class="col-xs-4 col-xs-offset-8">
              <button type="submit" id="login_btn" class="btn btn-primary btn-block btn-flat">Submit</button>
            </div><!-- /.col -->
          </div>
        </form>

        <!-- <a href="<?php //echo base_url('common/register'); ?>" class="pull-right" id="reg">Register Here</a> -->
</div>

    </div>
          <!-- /.box -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

        <br>
