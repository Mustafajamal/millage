<style type="text/css">
  .selected{

  background-color: #C8C8C8 !important;

}
</style>
<section class="content-header">
  <h1>
    Product Master
    <!-- <small>Preview of UI elements</small> -->
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div id="message_area">
        <?php get_flash_message('message'); ?>
      </div>
      <?php
        echo get_buttons(); 
        echo add_custom_button("Manage Color or Options","active",'btn bg-teal','check',FALSE);
        // echo add_custom_button("Inactive","inactive",'btn bg-maroon','ban',TRUE);
      ?>
      <br>
      <br>
      <div class="box box-warning">
        <div class="box-body">
          <div id="list_table">
                	<table class="table " id="datatable" width="100%">
                  		<thead>
                    		<tr>
                     			  <th>Table Id</th>
                            <th>Series Name</th>
                            <th>Style Name</th>
                            <th>Category Name</th>
                            <th>Product Number</th>
                            <th>Product Name</th>
                            <!-- <th>Movement</th>
                            <th>Crystal</th>
                            <th>Case</th>
                            <th>Case Diameter</th>
                            <th>Case Thickness</th>
                            <th>Bezel Material</th>
                            <th>Bezel Function</th>
                            <th>Water Resistant</th>
                            <th>Calendar</th>
                            <th>Bracelet</th>
                            <th>Bracelet Length</th>
                            <th>Bracelet Width</th>
                            <th>Clasp Type</th> -->
                    		</tr>
                  		</thead>
                	</table>
              	</div>
              	<form autocomplete="off" method="post" action="#" enctype="multipart/form-data" class="validate">
                	<div id="form_data"></div>
              	</form>
         </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
      $(function() {
        // $("#datatable").DataTable();
        base_url="<?php echo base_url(); ?>";
        class_name="manage_products";
        manage_buttons(base_url,class_name);
        url="<?php echo base_url('common/datatable/ajax_list/manage_products'); ?>"
        table=get_datatable(url);

      });
    </script>