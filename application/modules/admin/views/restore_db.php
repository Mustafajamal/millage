<style type="text/css">
  .selected{
    background-color: #C8C8C8 !important;
  }
</style>
<section class="content-header">
  <h1>
    Manage Database Backup
    <!-- <small>Preview of UI elements</small> -->
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div id="message_area">
        <?php get_flash_message('message'); ?>
      </div>
      <?php
        echo get_buttons(0,0,0,0,0);
        echo add_custom_button("Restore","restore",'btn bg-teal','check',TRUE);
        //FCPATH . "uploads/database_backups/";
        ?>
      <br>
      <br>
      <div class="box box-warning">
        <div class="box-body">
          <div id="list_table">
                	<table class="table " id="datatable" width="100%">
                  		<thead>
                    		<tr>
                            <th>Id</th>
                            <th>Backup File</th>
                            <th>Backup Time</th>
                            <th>Backup Ip</th>
                        </tr>
                  		</thead>
                	</table>
              	</div>
              	<form autocomplete="off" method="post" action="#" enctype="multipart/form-data" class="validate">
                	<div id="form_data"></div>
              	</form>
         </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
      $(function() {
        base_url="<?php echo base_url(); ?>";
        class_name="manage_backup";
        manage_buttons(base_url,class_name);
        url="<?php echo base_url('common/datatable/ajax_list/manage_backup'); ?>"
        table=get_datatable(url);

      });
    </script>