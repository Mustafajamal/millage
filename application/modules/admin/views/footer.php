  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!-- <b>Version</b> 2.4.0 -->
    </div>
    Copyright © <?php echo date('Y');?> All rights reserved for <a href="<?php echo  base_url();?>" class="primary-color"><strong> millage.com</strong></a>.
  </footer>

 
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo JS_URL; ?>bootstrap.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- SlimScroll -->
<!-- <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script> -->
<!-- FastClick -->
<!-- <script src="../../bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<script src="<?php echo JS_URL; ?>adminlte.min.js"></script>
<script type="text/javascript" src="<?php echo JS_URL.'jquery.validate.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'ajax_lib.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'custom.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'jquery.dataTables.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'dataTables.bootstrap.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'toastr.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'jquery.confirm.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'datatable.js'; ?>"></script>
<!-- AdminLTE for demo purposes -->


<!-- <script src="../../dist/js/demo.js"></script> -->
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
<script type="text/javascript">
  toastr.options = {
  // "positionClass": "toast-top-center",
  "progressBar": true,
  "showDuration": "300",
  "hideDuration": "10000",
  "timeOut": "10000",
  "extendedTimeOut": "1000",
  "closeButton": true,
  "showMethod": "slideDown",
  "hideMethod": "slideUp",
  "preventDuplicates": true,
  
  }
  // toastr["error"]("message_error","Message");
  var message_error="<?php echo $this->session->flashdata('message-error'); ?>";
    if (message_error!="") {
      toastr["error"](message_error,"Message");
    }
    var message_success="<?php echo $this->session->flashdata('message-success'); ?>";
    if (message_success!="") {
      toastr["success"](message_success,"Message");
    }
    var message_info="<?php echo $this->session->flashdata('message-info'); ?>";
    if (message_info!="") {
      toastr["info"](message_info,"Message");
    }
    var message_warning="<?php echo $this->session->flashdata('message-warning'); ?>";
    if (message_warning!="") {
      toastr["warning"](message_warning,"Message");
    }
</script>
</body>
</html>
