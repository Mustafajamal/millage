 <style type="text/css">
  .selected{
  background-color: #C8C8C8 !important;
}
</style>
<section class="content-header">
  <h1>
    Manage Wholesale Orders
    <!-- <small>Preview of UI elements</small> -->
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div id="message_area">
        <?php get_flash_message('message'); ?>
      </div>
      <?php
        echo get_buttons(1,0,0); 
       
        echo add_custom_button("SUCCESS","success",'btn bg-green','check',TRUE);
        echo add_custom_button("SHIPPED","shipped",'btn bg-aqua','truck',TRUE);
        echo add_custom_button("DELIVERED","delivered",'btn bg-primary','shopping-cart',TRUE);
        echo add_custom_button("CANCELED","canceled",'btn bg-red','times',TRUE);
        echo add_custom_button("INVOICE","invoiced",'bg-olive','line-chart',0);
        
      ?>
      <br>
      <br>
      <div class="box box-warning">
        <div class="box-body">
          <div id="list_table">
                	<table class="table " id="datatable" width="100%">
                  		<thead>
                    		<tr>
                    			<th>ID</th>
                     			<th>Customer Name</th>
                     			<th>Total</th>
                     			<th>Order Status</th>
                     			<th>Order Date</th>
                    		</tr>
                  		</thead>
                	</table>
              	</div>
              	<form autocomplete="off" method="post" action="#" enctype="multipart/form-data" class="validate">
                	<div id="form_data"></div>
              	</form>
         </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
      $(function() {
        // $("#datatable").DataTable();
        base_url="<?php echo base_url(); ?>";
        class_name="manage_wholesale_orders";
        manage_buttons(base_url,class_name);
        url="<?php echo base_url('common/datatable/ajax_list/manage_wholesale_orders'); ?>"
        table=get_datatable(url);

      });
    </script>