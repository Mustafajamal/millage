<!DOCTYPE html> 
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo get_site_info("site_title"); ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo CSS_URL; ?>bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo CSS_URL; ?>font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo CSS_URL; ?>AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo CSS_URL; ?>_all-skins.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>dataTables.bootstrap.min.css" />
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link href="<?php echo CSS_URL.'custom.css'; ?>" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="<?php echo CSS_URL.'toastr.min.css'; ?>" rel="stylesheet" type="text/css" />
  <script src="<?php echo JS_URL; ?>jquery-3.2.1.min.js"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(''); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><?php //echo substr(get_site_info("site_title"),0,1); ?>
       <img height="45px" width="50px" src="<?php echo UPLOAD_URL; ?>/site/<?php echo get_site_info('site_logo'); ?>" alt="Logo" > 
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        <img height="45px" width="50px" src="<?php echo UPLOAD_URL; ?>/site/<?php echo get_site_info('site_logo'); ?>" alt="Logo" ><?php echo get_site_info("site_title"); ?>
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo IMG_URL; ?>user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->username ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo IMG_URL; ?>user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <!-- Alexander Pierce - Web Developer -->
                </p>
              </li>

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url("common/logout/admin"); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo IMG_URL; ?>user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->username ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url('admin/dashboard'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo base_url('admin/manage_categories'); ?>">
            <i class="fa fa-list"></i> <span>Manage Categories</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo base_url('admin/manage_series'); ?>">
            <i class="fa fa-th-list"></i> <span>Manage Series</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo base_url('admin/manage_style'); ?>">
            <i class="fa fa-server"></i> <span>Manage Styles</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo base_url('admin/manage_products'); ?>">
            <i class="fa fa-product-hunt "></i> <span>Manage Products</span>
          </a>
        </li> 
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>User Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($this->session->user_id==1) { ?>
            <li><a href="<?php echo base_url('admin/manage_admin'); ?>"><i class="fa fa-circle-o"></i> Manage Admin</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url('admin/manage_customer'); ?>"><i class="fa fa-circle-o"></i> Manage Customer</a></li>
            <li><a href="<?php echo base_url('admin/manage_dealer'); ?>"><i class="fa fa-circle-o"></i> Manage Dealer</a></li>
            <li><a href="<?php echo base_url('admin/manage_retailer'); ?>"><i class="fa fa-circle-o"></i> Manage Retailer</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tasks"></i> <span>CMS Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            
            <li><a href="<?php echo base_url('admin/manage_page_category'); ?>"><i class="fa fa-circle-o"></i> Manage Page Category</a></li>
            <li><a href="<?php echo base_url('admin/manage_page_data'); ?>"><i class="fa fa-circle-o"></i> Manage Page Data</a></li>
          </ul>
        </li>
          <li>
          <a href="<?php echo base_url('admin/manage_customers_orders'); ?>">
            <i class="fa fa-usd "></i> <span>Manage Customers Orders</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo base_url('admin/manage_retailers_orders'); ?>">
            <i class="fa fa-usd "></i> <span>Manage Retail Orders</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo base_url('admin/manage_wholesale_orders'); ?>">
            <i class="fa fa-usd "></i> <span>Manage Wholesale Orders</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo base_url('admin/site_setting'); ?>">
            <i class="fa fa-cogs "></i> <span>Manage Site Settings</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo base_url('admin/manage_subscriber'); ?>">
            <i class="fa fa-user"></i> <span>Manage Subscriber</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('admin/manage_contact'); ?>">
            <i class="fa fa-user-o"></i> <span>Manage Contact</span>
          </a>
        </li>
         <li>
          <a href="<?php echo base_url('admin/manage_distributor_application'); ?>">
            <i class="fa fa-user"></i> <span>Manage Distributor Application</span>
          </a>
        </li>
         <li>
          <a href="<?php echo base_url('admin/manage_dealer_application'); ?>">
            <i class="fa fa-user"></i> <span>Manage Dealer Application</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('admin/inventory'); ?>">
            <i class="fa fa-file-o "></i> <span>Manage Inventory</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url("admin/backup_db"); ?>">
            <i class="fa fa-database"></i><span>Backup Database</span>
          </a>
        </li><li>
          <a href="<?php echo base_url("admin/restore_db"); ?>">
            <i class="fa fa-database"></i><span>Restore Database</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('admin/change_password'); ?>">
            <i class="fa fa-key"></i> <span>Change Password</span>
          </a>
        </li>
        <!-- <li>
          <a href="<?php echo base_url('admin/dashboard'); ?>">
            <i class="fa fa-cogs "></i> <span>Manage Site Pages</span>
          </a>
        </li>  -->
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li>
          <a href="../widgets.html">
            <i class="fa fa-th"></i> <span>Widgets</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">Hot</small>
            </span>
          </a>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">