<style type="text/css">
  .selected{

  background-color: #C8C8C8 !important;

}
</style>
<section class="content-header">
  <h1>
      <a href="<?php echo base_url('admin/manage_products') ?>" class='btn bg-purple btn-xs'>
          <i class='fa fa fa-arrow-circle-left' aria-hidden='true'></i> Back to Products
      </a> 
    Manage Color / Options / Stock for Product <u><?php echo $watch_data['product_number']."-".$watch_data['product_name'] ?></u>
  </h1>

</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div id="message_area">
        <?php get_flash_message('message'); ?>
      </div>
      <?php echo get_buttons(); 
        echo add_custom_button("Mark as On Sale","active",'btn bg-teal','usd',TRUE);
        echo add_custom_button("Mark as New","active",'btn bg-olive','line-chart',TRUE);
        echo add_custom_button("Mark as Banner Product","active",'btn bg-orange','money',TRUE);
      ?>

      <br>
      <br>
      <div class="box box-warning">
        <div class="box-body">
          <div id="list_table">
                	<table class="table " id="datatable" width="100%">
                  		<thead>
                    		<tr>
                     			  <th>Id</th>
                      			<th>Dial Color</th>
                            <th>Main Stock</th>
                            <th>Retail Price</th>
                            <th>Dealer Price</th>
                            <th>Distributor Price</th>
                            <th>Visible under "On Sale" Tab</th>
                            <th>Visible under "New" Tab</th>
                            <th>Is Banner Product</th>
                    		</tr>
                  		</thead>
                	</table>
              	</div>
              	<form autocomplete="off" method="post" action="#" enctype="multipart/form-data" class="validate">
                	<div id="form_data"></div>
              	</form>
         </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
      $(function() {
        var watch_id="<?php echo $watch_id; ?>";
        // console.log(watch_id);
        // $("#datatable").DataTable();
        base_url="<?php echo base_url(); ?>";
        class_name="manage_product_option";
        manage_buttons(base_url,class_name);
        url="<?php echo base_url('common/datatable/ajax_list/manage_product_option'); ?>"
        table=get_datatable(url,{'watch_id':watch_id});

      });
    </script>