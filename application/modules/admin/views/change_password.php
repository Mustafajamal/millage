<div class="breadcrumbs">

    <!--main banner-->
    <main class="main primary-padding">
        <div class="product-list">
            <div class="container">
                <!-- <h2 class="text-center"><?php echo $category_name.$style_name; ?></h2> -->
                <div class="row">                
                    <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                        <h2>Manage Account</h2>
                        <?php 
                            echo form_open('admin/change_password/save',array("class"=>"validate"));
                            $customer_name_frm=array("name"=>"identity","id"=>"identity","required"=>"","class"=>"form-control","placeholder"=>"Identity","disabled"=>"","readonly"=>"");
                            echo form_input($customer_name_frm,$this->session->identity,"Identity");
                            $customer_name_frm=array("name"=>"old_password","id"=>"old_password","required"=>"","class"=>"form-control","placeholder"=>"Old Password");
                            echo form_password($customer_name_frm,"","Old Password");
                            $customer_name_frm=array("name"=>"new_password","id"=>"new_password","required"=>"","class"=>"form-control","placeholder"=>"New Password");
                            echo form_password($customer_name_frm,"","New Password");
                            $customer_name_frm=array("name"=>"c_new_password","id"=>"c_new_password","required"=>"","class"=>"form-control","placeholder"=>"Confirm New Password","data-rule-equalTo"=>"#new_password");
                            echo form_password($customer_name_frm,"","Confirm New Password");
                            ?>
                            <button type="submit" class="btn btn-success pull-right">Submit</button>
                            <?php
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
    <!--main-->
      <script>
$(document).ready(function(){
    $(".has-menu").click(function(){
        $(this).toggleClass('has-menu');
    });
});
</script>