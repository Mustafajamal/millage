
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Millage Dashboard
        
      </h1>
      
    </section>
<?php 
  // foreach ($dash_data as $cat_name => $cat_data) {
  //   foreach ($cat_data as $key => $icon) {
  //     foreach ($icon as $color => $number) {
  //       // foreach ($data as $number) {
  //   ?>
    <!--  <div class="col-md-4"> -->
       <!-- Default box -->
      <!--  <div class="small-box <?php echo $color; ?>">
  //           <div class="inner">
  //             <h3><?php echo $number; ?></h3>

  //             <p><?php echo ucwords(str_replace('_', ' ', $cat_name)); ?></p>
  //           </div>
  //           <div class="icon">
  //             <i class="fa <?php echo $key; ?>"></i>
  //           </div>
  //           <a href="<?php echo base_url('admin/manage_categories');?>" class="small-box-footer">
  //             More info <i class="fa fa-arrow-circle-right"></i>
  //           </a>
  //         </div>
  //       </div>     -->
     <?php 
  // // }
  // }
  // }  
  // }
 ?>

<!-- Main content -->
<section class="content">

<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"> Millage Dashboard</h3>
              <div class="box-tools pull-right">
                
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<div class="row">
  <?php

add_stat_widget("bg-aqua","total_category",$total_category,"fa-list","manage_categories",'4');
add_stat_widget("bg-green","total_series",$total_series,"fa-th-list","manage_series",'4');
add_stat_widget("bg-yellow","total_style",$total_style,"fa-list-ul","manage_style",'4');

add_stat_widget("bg-olive","total_products",$total_products,"fa-product-hunt","manage_products",'4');

add_stat_widget("bg-red","total_page_category",$page,"fa-server","manage_page_category",'4');
add_stat_widget("bg-purple","total_pages",$page_category,"fa-list-ol","manage_page_data",'4');

add_stat_widget("bg-orange","total_customer",$customer,"fa-user","manage_customer",'4');
add_stat_widget("bg-teal","total_retailer",$retailer,"fa-user-circle-o","manage_retailer",'4');
add_stat_widget("bg-primary","total_dealer",$dealer,"fa-user-secret","manage_dealer",'4');

// add_stat_widget("bg-aqua","total_order",$total_order,"fa-shopping-cart","manage_customers_orders",'4');
?>
</div>
    
  </div>
  <!-- /.box-body -->
</div>


<div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Customer Order Summary</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<?php
add_stat_widget("bg-teal","total_customer_order_shipped",$total_order_customer_shipped,"fa-truck","manage_customers_orders
",'3');
add_stat_widget("bg-green","total_customer_order_success",$total_order_customer_success,"fa-check-square","manage_customers_orders
",'3');
add_stat_widget("bg-red","total_customer_order_canceled",$total_order_customer_canceled,"fa-close","manage_customers_orders
",'3');
add_stat_widget("bg-yellow","total_customer_order_delivered",$total_order_customer_delivered,"fa-shopping-cart","manage_customers_orders
",'3');

?>
</div>
            <!-- /.box-body -->
</div>

<div class="box  box-solid">
            <div class="box-header with-border bg-navy">
              <h3 class="box-title">Dealer Order Summary</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">          
<?php
add_stat_widget("bg-teal","total_dealer_order_shipped",$total_order_dealer_shipped,"fa-truck","manage_wholesale_orders
",'3');
add_stat_widget("bg-green","total_dealer_order_success",$total_order_dealer_success,"fa-check-square","manage_wholesale_orders
",'3');
add_stat_widget("bg-red","total_dealer_order_canceled",$total_order_dealer_canceled,"fa-close","manage_wholesale_orders
",'3');
add_stat_widget("bg-yellow","total_dealer_order_delivered",$total_order_dealer_delivered,"fa-shopping-cart","manage_wholesale_orders
",'3');
?>
</div>
</div>
<div class="box box-info box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Retailer Order Summary</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<?php
add_stat_widget("bg-teal","total_retailer_order_shipped",$total_order_retailer_shipped,"fa-truck","manage_retailers_orders
",'3');
add_stat_widget("bg-green","total_retailer_order_success",$total_order_retailer_success,"fa-check-square","manage_retailers_orders
",'3');
add_stat_widget("bg-red","total_retailer_order_canceled",$total_order_retailer_canceled,"fa-close","manage_retailers_orders
",'3');
add_stat_widget("bg-yellow","total_retailer_order_delivered",$total_order_retailer_delivered,"fa-shopping-cart","manage_retailers_orders
",'3');

?>
</div>
</div>
    </section>
    <!-- /.content -->
