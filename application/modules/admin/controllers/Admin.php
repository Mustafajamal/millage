<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Admin extends MY_Controller {

	public function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
      $this->session->unset_userdata('group_level');
      if ($this->uri->segment(2)!="index" && !is_null($this->uri->segment(2))) {
      		redirect('admin','refresh');
      	}
    }
    else{
		  if ($this->uri->segment(1) !=$this->session->group_level) {
                  $this->session->set_userdata('group_level',"admin");
			 redirect($this->session->group_level,'refresh');
		  }
    }
	}

	public function index(){
    if ($this->ion_auth->logged_in()) {
      redirect("admin/dashboard",'refresh');
    }
	}

	public function dashboard(){
    $this->body_vars['total_category']=$this->custom->getTotalCount("categories_master");
    $this->body_vars['total_series']=$this->custom->getTotalCount("series_master");
	  $this->body_vars['total_style']=$this->custom->getTotalCount("style_master");
    $this->body_vars['total_products']=$this->custom->getTotalCount("watch_master");

    $this->body_vars['retailer']=$this->custom->getCount("users_groups",array("group_id"=>"2"));
    $this->body_vars['dealer']=$this->custom->getCount("users_groups",array("group_id"=>"3"));
    $this->body_vars['customer']=$this->custom->getCount("users_groups",array("group_id"=>"4"));

    $this->body_vars['page']=$this->custom->getTotalCount("cms_page_category");
    $this->body_vars['page_category']=$this->custom->getTotalCount("cms_page_category_section");

    $this->body_vars['total_order']=$this->custom->getTotalCount("order_master");
    
    $this->body_vars['total_order_customer']=$this->custom->getCount("order_master",array("user_group_id"=>'4'));
    $this->body_vars['total_order_customer_shipped']=$this->custom->getCount("order_master",array("user_group_id"=>'4',"order_status"=>"shipped"));
    $this->body_vars['total_order_customer_success']=$this->custom->getCount("order_master",array("user_group_id"=>'4',"order_status"=>"success"));
    $this->body_vars['total_order_customer_canceled']=$this->custom->getCount("order_master",array("user_group_id"=>'4',"order_status"=>"canceled"));
    $this->body_vars['total_order_customer_delivered']=$this->custom->getCount("order_master",array("user_group_id"=>'4',"order_status"=>"delivered"));


    $this->body_vars['total_order_dealer']=$this->custom->getCount("order_master",array("user_group_id"=>'3'));
    $this->body_vars['total_order_dealer_shipped']=$this->custom->getCount("order_master",array("user_group_id"=>'3',"order_status"=>"shipped"));
    $this->body_vars['total_order_dealer_success']=$this->custom->getCount("order_master",array("user_group_id"=>'3',"order_status"=>"success"));
    $this->body_vars['total_order_dealer_canceled']=$this->custom->getCount("order_master",array("user_group_id"=>'3',"order_status"=>"canceled"));
    $this->body_vars['total_order_dealer_delivered']=$this->custom->getCount("order_master",array("user_group_id"=>'3',"order_status"=>"delivered"));

    $this->body_vars['total_order_retailer']=$this->custom->getCount("order_master",array("user_group_id"=>'2'));
    $this->body_vars['total_order_retailer_shipped']=$this->custom->getCount("order_master",array("user_group_id"=>'2',"order_status"=>"shipped"));
    $this->body_vars['total_order_retailer_success']=$this->custom->getCount("order_master",array("user_group_id"=>'2',"order_status"=>"success"));
    $this->body_vars['total_order_retailer_canceled']=$this->custom->getCount("order_master",array("user_group_id"=>'2',"order_status"=>"canceled"));
    $this->body_vars['total_order_retailer_delivered']=$this->custom->getCount("order_master",array("user_group_id"=>'2',"order_status"=>"delivered"));

  }

	public function manage_categories(){
	}

  public function manage_style(){
  }

  public function manage_series(){
  }

  public function manage_products(){
  }

  public function manage_admin(){
    if ($this->session->user_id!=1) {
      redirect('admin/dashboard','refresh');
    }
  }

  public function manage_customer(){
  }

 

  public function manage_dealer(){
  }
  
  public function manage_retailer(){
  }



  public function manage_product_option($id){
    $this->body_vars['watch_id']=$id;
    $this->body_vars['watch_data']=$this->custom->getSingleRow("watch_master",array("watch_id"=>$id),"array");
    $this->session->set_userdata('watch_id', $id);
  }

	public function site_setting($action="form")
	{
		if($action=="form")
		{
    		$this->body_vars['setting']=$product_data=$this->custom->getSingleRow("setting_master",array('setting_id'=>"1"));
    		$this->body_vars['save_url']=base_url("admin/site_setting/save");	
  		}
  		if($action=="save")
  		{
  			$post=$this->input->post();
        
        if(!empty($_FILES['site_logo']['name'][0])):
          $upload_data=file_upload("logo","site_logo","site");
          $post['site_logo']=$upload_data['upload_data']['file_name'];
        endif;

        if(!empty($_FILES['slider_images']['name'][0])):
    			for ($i = 0; $i <count($_FILES['slider_images']['name']) ; $i++) 
    			{
    				$_FILES['one_file']['name']=$_FILES['slider_images']['name'][$i];
    				$_FILES['one_file']['type']=$_FILES['slider_images']['type'][$i];
    				$_FILES['one_file']['tmp_name']=$_FILES['slider_images']['tmp_name'][$i];
    				$_FILES['one_file']['error']=$_FILES['slider_images']['error'][$i];
    				$_FILES['one_file']['size']=$_FILES['slider_images']['size'][$i];
    				$multiple_upload_data=file_upload("slider-".($i+1),"one_file","site");
    				
    				$si[]=$multiple_upload_data['upload_data']['file_name'];	
    			}
    			$post['slider_images']=implode(',',$si);
        endif;
        if(!empty($_FILES['footer_image']['name'][0])):
          $upload_data=file_upload("footer","footer_image","site");
          $post['footer_image']=$upload_data['upload_data']['file_name'];
        endif;
        $this->custom->updateRow("setting_master",$post,array("setting_id"=>1));
  			redirect('admin/site_setting','refresh');
  		}
  	}

    public function change_password($action='form'){
    // if ($this->session->group_level!="admin") {
    //   redirect('','refresh');
    // }
    if ($action=="save") {
      $data=$this->input->post();
      unset($data['identity']);
      if($this->ion_auth->change_password($this->session->identity, $data['old_password'], $data['new_password'])){
        $this->ion_auth->logout();
        $this->session->set_flashdata("message-success"," Password Changed Successfully");
        redirect('','refresh');
      }
      else
      {
        $this->session->set_flashdata("message-error","Your Old Password is Not match with your Current Password");
        redirect("admin/change_password");
      }
    } 
  }


  public function manage_customers_orders() {   
  }

  public function manage_retailers_orders() {    
  }

  public function manage_wholesale_orders() {    
  }

  public function manage_page_data(){
  }

  public function manage_page_category(){
  }

  public function manage_page_category_section(){
  }

  public function manage_subscriber(){
  }

  public function manage_contact(){
  }

  public function manage_distributor_application(){
  }
  
  public function manage_dealer_application(){
  }
  public function backup_db()
  {

     $file=databaseBackup();
      $date=date("Y-m-d h:i:s");
      $ip_address=$this->input->ip_address();
      $this->custom->insertRow("backup_master",array("backup_file"=>$file.".sql","backup_time"=>$date,"backup_ip"=>$ip_address));
      $this->body_file="blank";
      redirect("admin/restore_db"); 
  }

  public function restore_db(){    
  }

  public function inventory(){

  }
  public function invoices_view($id)
  {
    $this->header_file="blank.php";
    $this->footer_file="blank.php";
        $this->body_vars["mode"]="print";
        $this->body_vars['order_data']=$full_order_data=$this->custom->getRowsWhereJoin("order_master",array("order_master.order_id"=>$id),array("order_item_master"),array("order_master.order_id=order_item_master.order_id"));
        
        
  } 
}
/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */