<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Retailer extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
        	if ($this->uri->segment(2)!="index" && !is_null($this->uri->segment(2))) {
        		redirect('retailer','refresh');
        	}
        }
        else
        {
    		  if ($this->uri->segment(1) !=$this->session->group_level) {
    			 // redirect('retailer/dashboard','refresh');
    		  }
        }
	}

	public function index()
	{
		if ($this->ion_auth->logged_in()) {
			redirect('retailer/dashboard','refresh');
		}
	}

	public function dashboard(){
	}

	public function logout($redirect="")
	{
		$this->ion_auth->logout();
		redirect($redirect,'refresh');
	}
}

/* End of file Dealer.php */
/* Location: ./application/controllers/Dealer.php */