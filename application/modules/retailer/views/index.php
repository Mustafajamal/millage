<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo CSS_URL.'bootstrap.min.css'; ?>" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo CSS_URL.'font-awesome.min.css'; ?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo CSS_URL.'AdminLTE.css'; ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo CSS_URL.'custom.css'; ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo CSS_URL.'toastr.min.css'; ?>" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <script type="text/javascript" src="<?php echo JS_URL.'jquery-3.1.1.min.js'; ?>"></script>
    <!-- <link href="../../plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" /> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page background-load">
    <div class="login-box">
      <div class="login-logo">
        <img src="<?php echo UPLOAD_URL.'site/'.get_site_info('site_logo'); ?>" height=100px width=100px>

          <!-- <img src="<?php //echo IMG_URL.'logo_final.png' ?>" height=100px width=100px> -->

        <br>
        <b><?php echo get_site_info('site_title'); ?> Retailer Login</b>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
      <?php if(!is_null($this->session->flashdata('message'))){ ?>
        <div class="alert alert-warning"><?php echo get_flash_message('message'); ?></div>
      <?php } ?>


      <?php
        $attributes = array('class' => 'validate', 'id' => 'login_form','method'=>"post");
        echo form_open('#',$attributes);
      ?>

        <!-- <form action="#" method="post" id="login_form" class="validate"> -->


          <div class="has-feedback">
            <?php
               $frm_username=array("name"=>"username","id"=>"username","class"=>"form-control","placeholder"=>"Email","required"=>"");
                                echo form_input($frm_username);
            ?>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div id="password" class="has-feedback">
            <?php
                  $frm_password=array("name"=>"password","class"=>"form-control","placeholder"=>"Password","required"=>"");
                  echo form_password($frm_password)
            ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-8">
              <button type="submit" id="login_btn" class="btn btn-primary btn-block btn-flat">Submit</button>
            </div><!-- /.col -->
          </div>
        </form>

        <a href="#" class="" id="f_pass">Forgot Password?</a><a href="#" class="" id="login">Login?</a>
        <!-- <a href="<?php //echo base_url('common/register'); ?>" class="pull-right" id="reg">Register Here</a> -->

        <br>

      </div><!-- /.login-box-body -->
  
    </div><!-- /.login-box -->

    <!-- Bootstrap 3.3.2 JS -->
     <script type="text/javascript" src="<?php echo JS_URL.'bootstrap.min.js'; ?>"></script>
     <script type="text/javascript" src="<?php echo JS_URL.'jquery.form.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo JS_URL.'jquery.validate.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo JS_URL.'custom.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo JS_URL.'toastr.min.js'; ?>"></script>
  
    <script type="text/javascript">

$(document).ready(function() {
  $("#login").hide();

  var message="<?php echo $this->session->flashdata('message'); ?>";
          if (message!="") {
            toastr["info"](message,"Message");
          }

  var login_options = { 
    url : "<?php echo base_url('common/ajax_controller/login_check') ?>",
    beforeSubmit: function() {
      $("#login_btn").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>");
    },
    success:function (data) {
      $("#login_btn").html("Submit");  
        data=$.trim(data);
        $("#login_btn").html("Sign In");
        data=$.trim(data);
        if (data=="retailer") {
            toastr["success"]("Successfully Logged In <br> Redirecting.....","");
            window.location.href = "<?php echo base_url() ?>"+data+"/dashboard";
        }
        else{
            toastr["error"]("Check Your Credentials","Login Failed");
        }
      },
    }; 
    // bind form using 'ajaxForm' 
    
    var forgot_options = { 
    url : "<?php echo base_url('retailer/retailer_ajax/forgot_password') ?>",
    beforeSubmit: function() {
      $("#login_btn").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>");
    },
    success:function (data) {
        $("#login_btn").html("Submit");  
        $("#error").hide();
        $("#msg").html(data).slideDown();
        $("#msg").slideUp(5000);
      },
    }; 
    // bind form using 'ajaxForm' 
    $("#login_btn").click(function(event) {
      var form_id=$("form").attr('id');
      if (form_id=="login_form") {
        $('#login_form').ajaxForm(login_options); 
      }
      if (form_id=="forgot_password_form") {
        $('#forgot_password_form').ajaxForm(forgot_options); 
      }
    });



    $("#f_pass").click(function(event) {
      $("#password").slideUp();
      $("#login").show();
      $("#f_pass").hide();
      $("form").attr('id', 'forgot_password_form');
      $("#username").attr({
        placeholder: 'E-mail',
        type: 'email',
      });
      $("#username").val('');
    });
    $("#login").click(function(event) {
      $("#password").slideDown();
      $("#f_pass").show();
      $("#login").hide();
      $("form").attr('id', 'login_form');
      $("#username").attr({
        placeholder: 'Email',
        type: 'text',
      });
      $("#username").val('');
    });
}); 
</script>
  </body>
</html>
