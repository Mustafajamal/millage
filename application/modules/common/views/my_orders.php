<!-- <link rel="stylesheet" type="text/css" href="<?php //echo CSS_URL; ?>base.css">
<link rel="stylesheet" type="text/css" href="<?php //echo CSS_URL.'myOrders.min.css' ?>"> -->
<main class="main primary-padding">
<div class="breadcrumbs">

    <!--main banner-->
    <main class="main ">
        <div class="">
            <div class="container"><br>
                <!-- <h2 class="text-center"><?php echo $category_name.$style_name; ?></h2> -->
                <div class="row">                
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php
                            $orders=array();
                            $class="btn-primary";
                            $i=0;
                            // d($order_data);
                            foreach ($order_data as $order) {
                                $watch_option=$this->custom->getSingleRow("watch_options_master",array('watch_option_id'=>$order->watch_option_id));
                                $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_option->watch_id));
                                $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
                                $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
                                $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
                                if($this->session->userdata("group_level")=="dealer")
                                {
                                   $price= $watch_option->dealer_price;
                                }
                                else if($this->session->userdata("group_level")=="retailer")
                                {
                                  $price= $watch_option->distributor_price;
                                }
                                else
                                {
                                   $price= $watch_option->retail_price;
                                }
                                if (!in_array($order->order_id,$orders)) { 
                                    $orders[]=$order->order_id;
                                    $i++;
                                    if ($order->order_status=="SHIPPED") {
                                        $class="btn-info";
                                    }
                                    if ($order->order_status=="SUCCESS") {
                                        $class="btn-success";
                                    }
                                    if ($order->order_status=="CANCELED") {
                                        $class="btn-danger";
                                    }
                                    if ($order->order_status=="PENDING") {
                                        $class="btn-warning";
                                    }
                                    if ($order->order_status=="DELIVERED") {
                                        $class="btn-primary";
                                    }
                                    if ($i!=1) {
                                      echo "</div>
                                          </div>";                                      
                                    }
                                     ?>
                                       <!--  <h2>Panel Group</h2>
                                        <p>The panel-group class clears the bottom-margin. Try to remove the class and see what happens.</p> -->

                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                              <div class="pull-right">
                                                  <?php if ($order->order_status=="CANCELED" || $order->order_status=="PENDING") { ?>
                                                  <form action="<?php echo base_url($this->session->group_level."/payment") ?>" class="form-inline" method="post">
                                                  <input type="hidden" name="order_id_table" value="<?php echo $order->order_id ?>">
                                                  <input type="hidden" name="payable_amount" value="<?php echo $order->total ?>">
                                                  <button type="button" class="btn <?php echo $class ?>"><?php echo $order->order_status ?></button>
                                                  <!-- <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">PAY NOW
                                                    <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                      <li><a href="#">Paypal</a></li>
                                                      <li><a href="#">Stripe</a></li>
                                                    </ul>
                                                  </div> -->
                                                  <button type="button" onclick="$('#payment_<?php echo $order->order_id ?>').slideToggle();" class="btn btn-primary">PAY NOW</button><br>
                                                  <span class="pull-right" id="payment_<?php echo $order->order_id ?>" style="display: none;">
                                                  <button class="btn-xs btn-danger" name="payment_method" value="paypal">Paypal</button> 
                                                  <button class="btn-xs btn-danger" name="payment_method" value="stripe">Stripe</button>
                                                  </form>
                                                  </span>
                                                  <?php }else{ ?>
                                                  <button type="button" class="btn <?php echo $class ?>"><?php echo $order->order_status ?></button>
                                                  <?php } ?>
                                                </div>
                                              OrderId : <?php echo strtotime($order->order_date);?><br>Date: <?php echo $order->order_date;?><br><br>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                      <div class="col-md-3">
                                                          <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>" height="150px" width="150px"><br><br>
                                                          <?php if(!is_null($order->order_note)){?>
                                                          <div class="row" style="overflow: auto;">
                                                            <div class="col-md-12">
                                                          <b> Your Order Note :</b><br><?php echo $order->order_note;?>
                                                          </div>
                                                          </div>
                                                          <?php } ?>
                                                      </div>
                                                      <div class="col-md-6">


                                                          <h2><?php echo $product_data->product_name." "; ?> 
                                                        
                                                          </h2>
                                                          <p>Color : <?php echo $watch_option->dial_color ?></p>
                                                          <p>Series : <?php echo $series_data ?></p>
                                                          <p>Category : <?php echo $category_data ?></p>
                                                      </div>
                                                      <div class="col-md-3">
                                                            <h5 class="pull-right">$<?php echo $price;?> X <?php echo $order->quantity ?>  = $<?php echo number_format($watch_option->retail_price*$order->quantity,2) ?></h5>
                                                      </div>
                                                </div>
                                                
                                <?php }else{ ?>
                                <hr>
                                        <div class="row">
                                                      <div class="col-md-3">
                                                          <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>" height="150px" width="150px">
                                                          <br><br>
                                                          <?php if(!is_null($order->order_note)){?>
                                                          <div class="row" style="overflow: auto;">
                                                            <div class="col-md-12">
                                                          <b> Your Order Note :</b><br><?php echo $order->order_note;?>
                                                          </div>
                                                          </div>
                                                          <?php } ?>
                                                      </div>
                                                      <div class="col-md-6">


                                                          <h2><?php echo $product_data->product_name." "; ?> 
                                                        
                                                          </h2>
                                                          <p>Color : <?php echo $watch_option->dial_color ?></p>
                                                          <p>Series : <?php echo $series_data ?></p>
                                                          <p>Category : <?php echo $category_data ?></p>
                                                      </div>
                                                     <div class="col-md-3">
                                                            <h5 class="pull-right">$<?php echo $price;?> X <?php echo $order->quantity ?>  = $<?php echo number_format($watch_option->retail_price*$order->quantity,2) ?></h5>
                                                      </div>
                                                </div>
                                                            
                                <?php } ?>
                            <?php } ?>
                </div>
            </div>
        </div>
    
</div>
    <!--main-->
      <script>
$(document).ready(function(){
    $(".has-menu").click(function(){
        $(this).toggleClass('has-menu');
    });
});
</script>