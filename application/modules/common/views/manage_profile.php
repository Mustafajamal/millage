<div class="breadcrumbs">
    <div class="container">
        <ul class="items">
                <li class="item home">
                    <a href="<?php echo base_url() ?>" title="Go to Home Page">Home</a>
                </li>
                <li class="item category9">
                    <a href="<?php echo current_url(); ?>"><?php echo ucwords(" > MY PROFILE"); ?></a>
                </li>
                <!-- /*******************************/ -->
        </ul>
    </div>
</div>
    <!--main banner-->
    <main class="main primary-padding">
        <div class="product-list">
            <div class="container">
                <!-- <h2 class="text-center"><?php echo $category_name.$style_name; ?></h2> -->
                <div class="row">                
                    <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                        <h2>Manage Account</h2>
                        <?php 
                            echo form_open($this->session->group_level.'/manage_profile/save',array("class"=>"validate"));
                            $customer_name_frm=array("name"=>"customer_name","id"=>"customer_name","required"=>"","class"=>"form-control","placeholder"=>"customer name");
                            echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data->customer_name:"","Customer Name");

                            $customer_name_frm=array("name"=>"phone","id"=>"phone","required"=>"","class"=>"form-control","placeholder"=>"Contact No");
                            echo form_input($customer_name_frm, $ion_data->phone,"Contact No");
                            $customer_name_frm=array("name"=>"username","id"=>"username","required"=>"","class"=>"form-control","placeholder"=>"Username");
                            echo form_input($customer_name_frm, $ion_data->username,"Username");
                            $customer_name_frm=array("name"=>"email","id"=>"email","required"=>"","class"=>"form-control","placeholder"=>"Username","disabled"=>"","readonly"=>"");
                            echo form_input($customer_name_frm, $ion_data->email,"E-mail");
                            /******************************************************************************************/
                            echo "<div class=row>";
                            echo "<div class=col-md-6 col-sm-12 col-xs-12>";
                          
                              
                            $customer_address_frm=array("name"=>"customer_address","id"=>"customer_address","required"=>"","class"=>"form-control","placeholder"=>"customer address");
                            echo form_textarea($customer_address_frm, (!is_null($cust_data))?$cust_data->customer_address:"","Address");
                            $customer_city_frm=array("name"=>"customer_city","id"=>"customer_city","required"=>"","class"=>"form-control","placeholder"=>"customer city");
                            echo form_input($customer_city_frm, (!is_null($cust_data))?$cust_data->customer_city:"","Town / City");
                            $customer_state_frm=array("name"=>"customer_state","id"=>"customer_state","required"=>"","class"=>"form-control","placeholder"=>"customer state");
                            echo form_input($customer_state_frm, (!is_null($cust_data))?$cust_data->customer_state:"","State / Proviance");
                            $custoemer_zip_code_frm=array("name"=>"custoemer_zip_code","id"=>"custoemer_zip_code","required"=>"","class"=>"form-control","placeholder"=>"custoemer zip code","min"=>0);
                            echo form_number($custoemer_zip_code_frm, (!is_null($cust_data))?$cust_data->custoemer_zip_code:"","Postcode / Zip");
                            $customer_country_frm=array("name"=>"customer_country","id"=>"customer_country","required"=>"","class"=>"form-control","placeholder"=>"customer country");
                            if (!is_null($cust_data)) {
                                echo form_dropdown($customer_country_frm,$countries, array($cust_data->customer_country),"customer country");
                            }else{
                                echo form_dropdown($customer_country_frm,$countries, array(230),"customer country");
                            }
                            echo "</div>";
                            echo "<div class=col-md-6 col-sm-12 col-xs-12>";
                            $customer_ship_address_frm=array("name"=>"customer_ship_address","id"=>"customer_ship_address","required"=>"","class"=>"form-control","placeholder"=>"customer ship address");
                            echo form_textarea($customer_ship_address_frm, (!is_null($cust_data))?$cust_data->customer_ship_address:"","Shipping Address");
                            $customer_ship_city_frm=array("name"=>"customer_ship_city","id"=>"customer_ship_city","required"=>"","class"=>"form-control","placeholder"=>"customer ship city");
                            echo form_input($customer_ship_city_frm, (!is_null($cust_data))?$cust_data->customer_ship_city:"","Shipping City");
                            $customer_ship_state_frm=array("name"=>"customer_ship_state","id"=>"customer_ship_state","required"=>"","class"=>"form-control","placeholder"=>"customer ship state");
                            echo form_input($customer_ship_state_frm, (!is_null($cust_data))?$cust_data->customer_ship_state:"","Shipping State");
                            $custoemer_ship_zip_code_frm=array("name"=>"custoemer_ship_zip_code","id"=>"custoemer_ship_zip_code","required"=>"","class"=>"form-control","placeholder"=>"custoemer ship zip code");
                            echo form_input($custoemer_ship_zip_code_frm, (!is_null($cust_data))?$cust_data->custoemer_ship_zip_code:"","custoemer ship zip code");
                            $customer_ship_country_frm=array("name"=>"customer_ship_country","id"=>"customer_ship_country","required"=>"","class"=>"form-control","placeholder"=>"customer ship country");
                            if (!is_null($cust_data)) {
                                echo form_dropdown($customer_ship_country_frm,$countries, array($cust_data->customer_ship_country),"Shipping Country");
                            }else{
                                echo form_dropdown($customer_ship_country_frm,$countries,array(230),"Shipping Country");
                            }
                        
                            echo "</div>";
                            echo "</div>";
                            ?>
                            <button type="submit" class="btn btn-success pull-right">Submit</button>
                            <?php
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--main-->
      <script>
$(document).ready(function(){
    $(".has-menu").click(function(){
        $(this).toggleClass('has-menu');
    });
});
</script>