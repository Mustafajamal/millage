 <main class="main">
 <div class="container" id="back">
 <div class="">
    <div class="container">
        <ul class="items">
                <li class="item home">
                    <a href="<?php echo base_url() ?>" title="Go to Home Page">Home</a>
                </li>
                <?php if(!is_null($this->uri->segment(3))){ ?>
                <li class="item">
                    <a href="<?php echo base_url($this->session->group_level.'/product_list/'.$this->uri->segment(3)) ?>" title=""><?php echo $category_name=$this->custom->getSingleValue("categories_master","category_name",array("category_id"=>$this->uri->segment(3))) ?></a>
                </li>
                <?php }else{$category_name="";} ?>
                <!-- /*******************************/ -->
                <?php if(!is_null($this->uri->segment(4))){ ?>
                <li class="item">
                    <a href="<?php echo base_url($this->session->group_level.'/product_list/'.$this->uri->segment(3).'/'.$this->uri->segment(4)) ?>"><?php echo $style_name=$this->custom->getSingleValue("style_master","style_name",array("style_id"=>$this->uri->segment(4))) ?></a>
                </li>
                <?php }else{$style_name="";} ?>
                <!-- /*******************************/ -->
                <?php if(!is_null($this->input->get('series'))){ ?>
                <li class="item">
                    <a href="<?php echo current_url().'?'.$this->input->server('QUERY_STRING'); ?>"><?php echo $style_name=" > ".$this->custom->getSingleValue("series_master","series_name",array("series_id"=>$this->input->get('series'))) ?></a>
                </li>
                <?php }else{$style_name="";} ?>
                <!-- /*******************************/ -->
                 <?php if(!is_null($this->input->get('color'))){ ?>
                <li class="item">
                    <a href="<?php echo current_url().'?'.$this->input->server('QUERY_STRING'); ?>"><?php echo ucwords($this->input->get('color')); ?></a>
                </li>
                <?php }else{$style_name="";} ?>
                <!-- /*******************************/ -->
        </ul>
    </div>
</div>
    <!--main banner-->
    <main class="main secondary-padding">
        <div class="product-list">
            <div class="container">
                <!-- <h2 class="text-center"><?php echo $category_name.$style_name; ?></h2> -->
                <div class="row">                
                    <div class="col-md-9 col-sm-9 col-xs-12 pull-right">
                        <!-- <div class="storefront-sorting"> -->
                            <!-- <form class="woocommerce-ordering" method="get">
                                <span class="">Sort:</span>
                                <select name="orderby" class="orderby">
                                    <option value="menu_order" selected="selected">Default sorting</option>
                                    <option value="date">Sort by newness</option>
                                    <option value="price">Sort by price: low to high</option>
                                    <option value="price-desc">Sort by price: high to low</option>
                                </select> 
                                <span class="glyphicon glyphicon-chevron-down select-dropdown"></span>
                            </form> -->
                            <!--form-->
                            <!--short by-->
                           <!--  <div class="woocommerce-result-count">
                               Items 1-9 of 10
                            </div> -->
                            <!--result-->
                            <!-- <nav class="woocommerce-pagination">
                                <ul class="page-numbers">
                                    <li><a class="prev page-numbers" href="#">&lt;</a></li>
                                    <li><span class="page-numbers current">1</span></li>
                                    <li><a class="page-numbers" href="#">2</a></li>
                                    <li><a class="page-numbers" href="#">3</a></li>
                                    <li><a class="next page-numbers" href="#">&gt;</a></li>
                                </ul>
                            </nav> -->
                             <!-- <div class="short-by pull-right">
                                <a href="07-rab-shop-left-sidebar-list.html" class="btn btn-default bdr">
                                    <i class="fa fa-list-ul"></i>
                                </a>
                                <a href="05-rab-shop-left-sidebar-grid.html" class="btn btn-default bdr active">
                                    <i class="fa fa-th-large"></i>
                                </a>
                            </div> -->
                        <!-- </div> -->
                        <!--storefront-sorting-->
                            <?php //d($all_products_options);
                            // exit();
                             ?>
                        <ul class="products clearfix">
                            <?php
                                $i=0;
                                foreach ($all_products_options as $value) {
                                    foreach ($value as $watch_one) {
                                        $i++;
                                        if ($i%3==0) {
                                          $class="last";
                                        }else{
                                           $class=""; 
                                        }
                                        $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_one->watch_id));
                                        $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
                                        $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
                                        $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
                                        if($this->session->userdata("group_level")=="dealer")
                                        {
                                           $price=$watch_one->dealer_price;
                                        }
                                        else if($this->session->userdata("group_level")=="retailer")
                                        {
                                           $price=$watch_one->distributor_price;
                                        }
                                        else
                                        {
                                           $price=$watch_one->retail_price;
                                        }
                                    ?>
                                    <li class="product <?php echo $class; ?>">
                                        <figure>
                                            <div class="icons">
                                                 <?php if (isset($this->session->user_id) && $price>0 && $watch_one->dial_stock>0) { ?>
                                                <a href="#" class="btn add_cart " id="<?php echo $watch_one->watch_option_id; ?>"><i class="pe-7s-cart"></i></a>
                                                <?php } ?>
                                                <!-- <a href="#" class="btn"><i class="pe-7s-like"></i></a> -->
                                            </div>
                                            <!--icons-->
                                            <div class="product-wrap base-align"> <a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$watch_one->watch_option_id) ?>">&nbsp;</a> <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_one->dial_color.'/'.$watch_one->main_image; ?>" alt="Product"  style="height: 260px;"> </div>
                                        </figure>
                                        <!--figure-->
                                        <div class="content">
                                           <h6><a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$watch_one->watch_option_id) ?>"><?php echo $series_data.' '.$watch_one->dial_color ?></a></h6>
                                            <div class="bottom">
                                                <div class="price"> <ins>

                                                    <?php
                                                        if ($price>0) {
                                                            echo "$ ".$price;
                                                        }
                                                    ?>
                                                        
                                                

                                                    </ins> </div>
                                                
                                                <!--price-->
                                                <!-- <span class="star-rating">
                                                   <i class="fa fa-star"></i> 
                                                   <i class="fa fa-star"></i> 
                                                   <i class="fa fa-star"></i> 
                                                   <i class="fa fa-star"></i> 
                                                   <i class="fa fa-star-half-empty "></i> 
                                                </span> -->
                                                <!--star-->
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                    }
                                } ?>
                            <!--single product-->
                        </ul>
                        <!--product list-->
                        <!-- <nav class="woocommerce-pagination text-center">
                            <ul class="page-numbers">
                                <li><a class="prev page-numbers" href="#">&lt;</a></li>
                                <li><span class="page-numbers current">1</span></li>
                                <li><a class="page-numbers" href="#">2</a></li>
                                <li><a class="page-numbers" href="#">3</a></li>
                                <li><a class="next page-numbers" href="#">&gt;</a></li>
                            </ul>
                        </nav> -->
                        <!--pegination-->
                    </div>
                    <!--left-->
                    <div class="col-md-3 col-sm-3 col-xs-12 sidebar shop-sidebar left-block">
                        <h2 class="text-center">Shop By</h2>
                        <h5 class="text-center">Shopping Options</h5>
                        
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Styles<span class="glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                                <?php foreach ($style as $one_style) { ?>
                                    <li>
                                        <a href="<?php echo base_url().uri_string().'?style='.$one_style->style_id ?>"><?php echo $one_style->style_name ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Series<span class="glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                                <?php foreach ($series as $one_series) { ?>
                                    <li>
                                        <a href="<?php echo base_url().uri_string().'?series='.$one_series->series_id ?>"><?php echo $one_series->series_name ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Color <span class="glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                               <?php foreach ($color as $one_color) { ?>
                                    <li>
                                        <a href="<?php echo base_url().uri_string().'?color='.$one_color->dial_color ?>"><?php echo $one_color->dial_color ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="box mb-30">
                            <h5 class="sec-title">Price</h5>
                             <form action="#">
                                <div id="slider-range"></div>
                                <input name="amount1" type="hidden" id="amount1" value="<?php echo $max_price;?>">
                                <input name="amount2" type="hidden" id="amount2" value="<?php echo $min_price;?>">
                                <div id="amount" class="mb-45"></div>
                                <button type="submit" class=" btn btn-default filter-btn faa-parent animated-hover">filter now <i class="fa fa-long-arrow-right faa-passing"></i></button>
                             </form>
                        </div>
                    </div>
                    <!--right-->
                </div>
            </div>
        </div>
    </main>
    <!--main-->
      <script>
$(document).ready(function(){
    $(".has-menu").click(function(){
        $(this).toggleClass('has-menu');
    });
   /* length=$(".items").children().length;
    child=$(".items").children();
    console.log(child[length-1]);
    $(child[length-1]).css('color', 'red');
*/});
</script>