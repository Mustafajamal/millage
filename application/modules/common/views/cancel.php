
    <main class="main primary-padding">
         <section class="checkout-steps">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="wrap first active">
                        <span class="circle rounded-crcl"> 01 </span>
                        <h6>Check Out</h6>
                    </div>
                    <!--steps-->
                    <div class="wrap second active">
                        <span class="circle rounded-crcl"> 02 </span>
                        <h6>Payment</h6>
                    </div>
                    <!--steps-->
                    <div class="wrap final full">
                        <span class="circle rounded-crcl"> 03 </span>
                        <h6>Order Complete</h6>
                    </div>
                    <!--steps-->
                </div>
            </div>
        </div>
    </section>
        <!--check out steps-->

        <section class="thank-you p-pb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 text-center ">
                        <figure class="mb-45">
                            <img src="<?php echo IMG_URL; ?>/cancel.jpg" alt="" height="104px" width="128px">
                        </figure>

                        <h4 class="mb-70 text-danger">Transaction Is Canceled </h4>

                        <a href="<?php echo base_url(); ?>" class="btn btn-default btn-bg text-uppercase">homepage</a>
                        <a href="<?php echo base_url('customer/my_orders/canceled'); ?>" class="btn btn-default btn-bg text-uppercase">Try Again</a>
                    </div>
                </div>
            </div>
        </section>
        <!--cart block-->
    </main>
    <!--main-->
