
     <div class="main-banner inner-banner banner-scroll">
  <div class="fixed-banner">
    <div class="slider-wrapper">
      <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel"> 
        <!-- Carousel items -->
        <div class="carousel-inner">

          <?php 
          for ($i=0; $i <count($slider_images) ; $i++) { ?>
            <?php if($i==0): ?>
            <div class="active item"> <img src="<?php echo UPLOAD_URL.'site/'.$slider_images[$i]; ?>" alt=""></div>   
          <?php else: ?>
            <div class="item"> <img src="<?php echo UPLOAD_URL.'site/'.$slider_images[$i]; ?>" alt=""></div>   
          <?php endif; ?>
          <?php } ?>
          <!-- <div class="item"> <img src="<?php echo IMG_URL; ?>slide2.jpg" alt=""> -->
            <!-- <div class="carousel-caption">
              <h5 class="text-uppercase mb-0 text-white slide-sd layer1">Give a new looks to your feet</h5>
              <h2 class="text-uppercase mb-30 text-white slide-sd layer2">summer collection</h2>
              <span class="primary-bg text-uppercase dis text-white slide-sd layer3"> 50% discount</span> </div> -->
          </div>
          <!--item--> 
        </div>
        
        <!-- Carousel nav --> 
        <a class="carousel-control left" href="#carousel" data-slide="prev"> <i class="pe-7s-angle-left"></i> </a> <a class="carousel-control right" href="#carousel" data-slide="next"> <i class="pe-7s-angle-right"></i> </a> </div>
    </div>
    <!--banner wrapper--> 
  </div>
</div>
    <!--main banner-->

    <main class="main">
        <section class="checkout-steps">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="wrap first active">
                            <span class="circle rounded-crcl"> 01 </span>
                            <h6>Shopping Cart</h6>
                        </div>
                        <!--steps-->

                        <div class="wrap second half">
                            <span class="circle rounded-crcl"> 02 </span>
                            <h6>Check Out</h6>
                        </div>
                        <!--steps-->

                        <div class="wrap final">
                            <span class="circle rounded-crcl"> 03 </span>
                            <h6>Order Complete</h6>
                        </div>
                        <!--steps-->
                    </div> 
                </div>
            </div>
        </section>
        <!--check out steps-->

        <section class="cart-block p-pb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="woocommerce">
                            <form action="#">
                                <table class="shop_table shop_table_responsive cart">
                                    <thead>
                                        <tr>
                                            <th class="product-name">Product Detail</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                            <th class="product-subtotal">Total</th>
                                            <th class="product-remove"> </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="cart_item">
                                            <td class="product-name" data-title="Product">
                                                <a href="#" class="cart-product">
                                                    <img src="assets/images/cart-product1.jpg" alt="Product thumbnail">
                                                </a>

                                                <div class="product-info">
                                                    <h6> Ladies outer fit style  </h6>
                                                    <ul class="product-info">
                                                        <li>Category: Women’s Wear </li>
                                                        <li>Size: XL </li>
                                                        <li>Color: Orange </li>
                                                    </ul>
                                                </div>
                                                <!--product info-->
                                            </td>
                                            <td class="product-price" data-title="Price">
                                                $190
                                            </td>
                                            <td class="product-quantity" data-title="Quantity">
                                               <div class="quantity-wrap">
                                                    <span class="minus"> - </span>
                                                    <input type="number" value="1" title="Qty" class="input-text qty" size="4">
                                                    <span class="add"> + </span>
                                                </div>
                                            </td>
                                            <td class="product-subtotal" data-title="Total">
                                                $190
                                            </td>
                                            <td class="product-remove" data-title="Remove">
                                                <a href="#" class="remove" title="Remove this item">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <!--cart item-->

                                        <tr class="cart_item">
                                            <td class="product-name" data-title="Product">
                                                <a href="#" class="cart-product">
                                                    <img src="assets/images/cart-product1.jpg" alt="Product thumbnail">
                                                </a>

                                                <div class="product-info">
                                                    <h6> Ladies outer fit style  </h6>
                                                    <ul class="product-info">
                                                        <li>Category: Women’s Wear </li>
                                                        <li>Size: XL </li>
                                                        <li>Color: Orange </li>
                                                    </ul>
                                                </div>
                                                <!--product info-->
                                            </td>
                                            <td class="product-price" data-title="Price">
                                                $190
                                            </td>
                                            <td class="product-quantity" data-title="Quantity">
                                               <div class="quantity-wrap">
                                                    <span class="minus"> - </span>
                                                    <input type="number" value="1" title="Qty" class="input-text qty" size="4">
                                                    <span class="add"> + </span>
                                                </div>
                                            </td>
                                            <td class="product-subtotal" data-title="Total">
                                                $190
                                            </td>
                                            <td class="product-remove" data-title="Remove">
                                                <a href="#" class="remove" title="Remove this item">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <!--cart item-->

                                        <tr class="cart_item">
                                            <td class="product-name" data-title="Product">
                                                <a href="#" class="cart-product">
                                                    <img src="assets/images/cart-product1.jpg" alt="Product thumbnail">
                                                </a>

                                                <div class="product-info">
                                                    <h6> Ladies outer fit style  </h6>
                                                    <ul class="product-info">
                                                        <li>Category: Women’s Wear </li>
                                                        <li>Size: XL </li>
                                                        <li>Color: Orange </li>
                                                    </ul>
                                                </div>
                                                <!--product info-->
                                            </td>
                                            <td class="product-price" data-title="Price">
                                                $190
                                            </td>
                                            <td class="product-quantity" data-title="Quantity">
                                               <div class="quantity-wrap">
                                                    <span class="minus"> - </span>
                                                    <input type="number" value="1" title="Qty" class="input-text qty" size="4">
                                                    <span class="add"> + </span>
                                                </div>
                                            </td>
                                            <td class="product-subtotal" data-title="Total">
                                                $190
                                            </td>
                                            <td class="product-remove" data-title="Remove">
                                                <a href="#" class="remove" title="Remove this item">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <!--cart item-->

                                        <tr class="cart_item">
                                            <td class="product-name" data-title="Product">
                                                <a href="#" class="cart-product">
                                                    <img src="assets/images/cart-product1.jpg" alt="Product thumbnail">
                                                </a>

                                                <div class="product-info">
                                                    <h6> Ladies outer fit style  </h6>
                                                    <ul class="product-info">
                                                        <li>Category: Women’s Wear </li>
                                                        <li>Size: XL </li>
                                                        <li>Color: Orange </li>
                                                    </ul>
                                                </div>
                                                <!--product info-->
                                            </td>
                                            <td class="product-price" data-title="Price">
                                                $190
                                            </td>
                                            <td class="product-quantity" data-title="Quantity">
                                               <div class="quantity-wrap">
                                                    <span class="minus"> - </span>
                                                    <input type="number" value="1" title="Qty" class="input-text qty" size="4">
                                                    <span class="add"> + </span>
                                                </div>
                                            </td>
                                            <td class="product-subtotal" data-title="Total">
                                                $190
                                            </td>
                                            <td class="product-remove" data-title="Remove">
                                                <a href="#" class="remove" title="Remove this item">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <!--cart item-->
                                        <tr>
                                            <td colspan="5" class="actions">
                                                <div class="coupon">
                                                    <label for="coupon_code">Coupon:</label>
                                                    <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code">

                                                    <input type="submit" class="button" name="apply_coupon" value="Apply Coupon">
                                                </div>
                                                <!--cupon-->

                                                <a href="#" class="btn btn-default btn-cart">Continue Shopping </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                            <!--cart form-->

                            <div class="cart-collaterals">
                                <div class="cart_totals">
                                    <h2>Cart Totals</h2>
                                    <table class="shop_table shop_table_responsive">
                                        <tbody>
                                            <tr class="cart-subtotal">
                                                <th>Cart Sub-total </th>
                                                <td data-title="Subtotal">$378.00
                                                </td>
                                            </tr>

                                            <tr class="shipping">
                                                <th> Shipping + Handling  </th>
                                                <td data-title="Shipping">
                                                    <span class="shipping text-green">$18.00</span>
                                                </td>
                                            </tr>

                                            <tr class="order-total">
                                                <th>grand Total</th>
                                                <td data-title="Total"> $190</td>
                                            </tr>
                                        </tbody>
                                    </table> 
                                    <div class="wc-proceed-to-checkout">
                                        <a href="#" class="checkout-button button alt wc-forward">Proceed to Checkout</a>
                                    </div>
                                </div>
                            </div>
                            <!--collaterals-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--cart block-->
 
    <!--main-->
