<main class="main primary-padding">
        <div class="product-list">
            <div class="container">
                <div class="row"> 
                    <div class="col-md-9 col-sm-9 col-xs-12 pull-right">
                        <h3><?php echo (!empty($page_details))?$page_details->section_name:''; ?></h3>
                        <?php if (!empty($page_details)) { 
                            if(!is_null($page_details->page_data) && $page_details->page_data!=""){
                                echo $page_details->page_data;
                            }else{
                             ?>
                                <h5><a href="<?php echo UPLOAD_URL.'cms_files/'.$page_details->file_content; ?>" target="_blank">Click Here to Open <strong><u><?php echo $page_details->section_name; ?></strong></u> File</a></h5>
                                <script type="text/javascript">
                                    window.open("<?php echo UPLOAD_URL.'cms_files/'.$page_details->file_content; ?>")
                                </script>
                            <?php }
                        } ?>
                    </div>
                    <!--left-->

                    <div class="col-md-3 col-sm-3 col-xs-12 sidebar shop-sidebar left-block">
                        <div class="box mb-30">
                            <?php foreach ($page_categories as $value) { ?>
                                <h5 class="sec-title"><?php echo $value->page_category_name; ?></h5>
                                <?php 
                                    $sections=$this->custom->getRows("cms_page_category_section",array("page_category_id"=>$value->page_category_id,"status"=>1));
                                    foreach ($sections as $one_section) { 
                                ?>
                                <ul class="shop-sidebar clearfix">
                                    <li>
                                        <a href="<?php echo base_url($this->session->group_level.'/page/'.$value->page_category_id.'/'.$one_section->section_name); ?>"><?php echo $one_section->section_name ?> <span class="no"></span></a>
                                    </li>
                                </ul>
                            <?php }
                                }
                             ?>

                        </div>
                        <!--category-->

                    </div>
                    <!--right-->
                </div>
            </div>
        </div>
  
    <script type="text/javascript">
        var curr='<?php echo urldecode(base_url(uri_string())); ?>';
        $(".shop-sidebar").each(function(index, el) {
            var actual=$(this).children('li').children('a').attr('href');
            if (curr==actual) {
                $(this).children('li').children('a').addClass('text-danger');
            }
        });
    </script>