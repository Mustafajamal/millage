<?php if($this->uri->segment(4)==1): ?>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/4.0.2/bootstrap-material-design.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<?php endif; ?>
<main class="main primary-padding" id="a">
  <div class="container">
    <div class="col-md-1"></div>
    <div class="col-md-10">
      <?php 
        $attributes = array('class' => 'form-horizontal validate', 'id' => 'setting_form','method'=>"post");
        echo form_open('#',$attributes);
        ?>
      <!-- Form Name -->
            <legend><font><u>
      <?php 
                if($this->uri->segment(4)=="usa")
                {
                  echo "United States Dealer Application";
                }
                else
                {
                  echo "Dealer Application";
                }
      ?></u></font><a href="<?php echo base_url().$this->session->group_level.'/apply/dealership/1' ?>" class="pull-right btn btn-primary"><i class="fa fa-print"></i> Printable Form</a></legend>
      <p>
<b>How to Apply for a Millage Dealership:</b><br>
Please provide your company information with the form below. We will process your application as soon as possible, and will issue your user id and password by sending it to the email address provided. Approval as a dealer usually takes about 7 - 10 business days. If you would prefer to print this application and send it by mail or fax, <u><a href="<?php echo base_url().$this->session->group_level.'/apply/dealership/1' ?>" style="color: red">Click Here</a></u>
<br>
If you are already a Millage dealer and would like an online account, you may either use this form to apply or contact your Millage sales rep directly.
</p>
      <?php
        $corporation=array("name"=>"apply_for","id"=>"apply_for","class"=>"form-control input-md");
        echo _form_common("hidden",$corporation,"dealer");
        
        $millage_account_no=array("name"=>"millage_account_no","id"=>"millage_account_no","class"=>"form-control","placeholder"=>"Millage Account No");
        echo form_input($millage_account_no,'','Millage Account No','',"<span class='help-block label-danger label'>Please leave blank if you do not already have an account. </span>");
        
        ?>
      <?php

        $corporation=array("name"=>"corporation","id"=>"textinput","class"=>"form-control input-md","required"=>"required","placeholder"=>"Corporation or Owner");
        echo form_input($corporation,"","Corporation or Owner");
        
        $dba_name=array("name"=>"dba_name","id"=>"dba_name","class"=>"form-control input-md","required"=>"required","placeholder"=>"DBA Name");
        echo form_input($dba_name,"","DBA Name");
        
        $federal_tax_id=array("name"=>"federal_tax_id","id"=>"federal_tax_id","class"=>"form-control input-md","placeholder"=>"Federal Tax ID");
        echo form_input($federal_tax_id,"","Federal Tax ID:");
        
        $contact_person=array("name"=>"contact_person","id"=>"contact_person","class"=>"form-control input-md","required"=>"required","placeholder"=>"Contact Person : ");
        echo form_input($contact_person,"","Contact Person");
        
        $contact_person_position=array("name"=>"contact_person_position","id"=>"contact_person_position","class"=>"form-control input-md","required"=>"required","placeholder"=>"Contact Person's Position:");
        echo form_input($contact_person_position,"","Contact Person's Position");
        
        $business_start_date=array("name"=>"business_start_date","id"=>"business_start_date","class"=>"form-control input-md","required"=>"required","placeholder"=>"Business start date");
        echo form_input($business_start_date,"","Business start date");
        ?>
      <!-- Form Name -->
      <legend><u>Business Address</u></legend>
      <div class="row">
      <div class="col-md-6">
        <?php
            $address=array("name"=>"address","id"=>"address","class"=>"form-control","required"=>"required","rows"=>"09");      
            echo form_textarea($address,'',"Address : ");
            ?>
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-5">
        <?php
             $city=array("name"=>"city","id"=>"city","class"=>"form-control input-md","required"=>"required","placeholder"=>"City : ");
            echo form_input($city,"","City");
            
            $zipcode=array("name"=>"zipcode","id"=>"zipcode","class"=>"form-control input-md","required"=>"required","placeholder"=>"ZipCode : ");
            echo form_input($zipcode,"","ZipCode : ");
            
            $state=array("name"=>"state","id"=>"state","class"=>"form-control input-md","required"=>"required");
            echo form_input($state,"","State");
        ?>
      </div>
    </div>
         <div class="row">
            <div class="col-md-6">
              <div class=" form-group">
              <label class="control-label" for="state"><strong>Country</strong></label>
              <div class="error_block">

              <?php 
                if($this->uri->segment(4)=="usa")
                {
              ?>
                <select name='country_names' id='country' class='form-control input-md' required='required' disabled="">
                    <?php echo $country_array;?>
                </select>
                <input type="hidden" name="country" value="US">
              <?php
                }
                else
                {
                ?>
                <select name='country' id='country' class='form-control input-md' required='required'>
                  <?php echo $country_array;?>
                </select>
                <?php 
                }
              ?>  
              </div>
            </div>
            </div>
             <div class="col-md-1"></div>
            <div class="col-md-5">
              <?php
                  $at_this_location_since=array("name"=>"at_this_location_since","id"=>"at_this_location_since","class"=>"form-control input-md","required"=>"required","placeholder"=>"At This Location Since : ");
                  echo form_input($at_this_location_since,"","At This Location Since : ");
              ?>
            </div>
         </div>
      <legend><u>Communication Data</u></legend>
      <div class="row">
        <div class="col-md-5">
          <?php
            $telephone=array("name"=>"telephone","id"=>"telephone","class"=>"form-control","required"=>"required");      
              echo form_number($telephone,'',"Telephone ");
            ?>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-5">
          <?php
            $fax=array("name"=>"fax","id"=>"fax","class"=>"form-control");      
            echo form_input($fax,'',"Fax");
            ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-5">
          <?php
            $email=array("name"=>"email","id"=>"dealership_email","class"=>"form-control","required"=>"required");      
            echo form_email($email,'',"Email");
            ?>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-5">
          <?php
            $website=array("name"=>"website","id"=>"website","class"=>"form-control");      
            echo form_input($website,'',"Website");
            ?>
        </div>
      </div>
      <div class="row">
      <legend><u>Business Profile Data</u></legend>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="form-group">
        <label class="control-label" for="state"><strong>Business Location Type:</strong></span></label>
        <div class="error_block">
              <input name="dealer_location_list[]"  required="" value="destination_store" type="checkbox"> Destination Store<br>
              <input name="dealer_location_list[]" value="website" type="checkbox"> Web Site <br>
              <input name="dealer_location_list[]" value="kiosk" type="checkbox"> Kiosk <br>
              <input name="dealer_location_list[]" value="mall_location" type="checkbox"> Mall Location <br>
              <input name="dealer_location_list[]" value="shopping_district" type="checkbox"> Shopping District <br>
              <input name="dealer_location_list[]" value="swap_outdoor_market" type="checkbox"> Swap / Outdoor Market <br>
        </div>
      </div>
      </div>
      <div class=" col-md-4 col-sm-4 col-xs-12">
        <div class="form-group">
        <label class="control-label" for="state"><strong>Type of retail:</strong></span></label>
        <div class="error_block">
              <input name="type_of_retail[]" required="" value="fashion_boutique" type="checkbox"> Fashion Boutique<br>
              <input name="type_of_retail[]" value="accessories" type="checkbox"> Accessories <br>
              <input name="type_of_retail[]" value="sporting_goods" type="checkbox">  Sporting Goods <br>
              <input name="type_of_retail[]" value="jewelry_store" type="checkbox"> Jewelry Store <br>
              <input name="type_of_retail[]" value="general_gifts" type="checkbox"> General Gifts <br>
              <input name="type_of_retail[]" value="discount_value_store" type="checkbox"> Discount Value Store <br>
              <input name="type_of_retail[]" value="watch_store" type="checkbox"> Watch Store <br>
        </div>
      </div>
      </div>
    
       <div class=" col-md-3 col-sm-3 col-xs-12">
        <div class="form-group">
        <label class="control-label" for="state"><strong>Customer age range:</strong></span></label>
        <div class="error_block">
            <input name="customer_age_range[]" required="" value="0_17" type="checkbox">  0 - 17 <br>
            <input name="customer_age_range[]" value="18_23" type="checkbox"> 18 - 23 <br>
            <input name="customer_age_range[]" value="24_30" type="checkbox"> 24 - 30 <br>
            <input name="customer_age_range[]" value="31_40" type="checkbox"> 31 - 40 <br>
            <input name="customer_age_range[]" value="41_55" type="checkbox"> 41 - 55 <br>
            <input name="customer_age_range[]" value="55" type="checkbox"> 55+ <br><br>
        </div>
      </div>
      </div>

       <div class="col-md-2 col-sm-2 col-xs-12">
        <div class="form-group">
        <label class="control-label" for="state"><strong>Target customers:</strong></span></label>
        <div class="error_block">
              <input name="target_customers[]" required="" value="male" type="checkbox"> Male <br>
              <input name="target_customers[]" value="female" type="checkbox"> Female <br>
              <input name="target_customers[]" value="unisex" type="checkbox"> Unisex <br>
        </div>
      </div>
      </div>     

    </div>
      <input type="submit" value="Submit" id="save_dealership" class="btn btn-primary col-md-offset-5 col-sm-offset-5">
      <?php echo form_close(); ?>
    </div>
    <div class="col-md-2">
    </div>
  </div>

<script type="text/javascript" src="<?php echo JS_URL.'bootstrap-datepicker.min.js'; ?>"></script>
<script type="text/javascript">
  $('#business_start_date').datepicker({
    endDate:'-1d',
    format:'yyyy-mm-dd',
  });
  $('#at_this_location_since').datepicker({
    endDate: '-1d',
    format:'yyyy-mm-dd',
  });
</script> 
<script type="text/javascript">
  $(function() {
    var options = { 
        url : "<?php echo base_url('common/ajax_controller/dealership_process') ?>",
        beforeSubmit: function() {
         // $("#save_dealership").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>").attr('disabled', true);
  
        },
        success:function (data) {
            //$("#contactus_btn").html('<span>Update Settings</span>').attr('disabled', false);
            
            data=$.trim(data);
            
            if (data=="success") {
                toastr["success"]("Add Successfully","Message");
                //window.location="<?php echo base_url();?>";
                location.reload();
            }
            else{
              toastr["error"](data,"Message");
  
            }
        },
      }
      $("#save_dealership").click(function(event) {
        if ($('#setting_form').valid()) {
          $('#setting_form').ajaxForm(options); 
        }
      });
    
  });
</script>
<script type="text/javascript">
  var print='<?php echo $this->uri->segment(4); ?>';
  if (print=="1") {
    $("p,legend > a , .help-block,.btn").hide();
    $(":input").attr('placeholder', '');
    $("textarea").removeClass('form-control');
    $("select").html('<input name="country" type="text" value="" id="country" class="form-control" placeholder="">');
    $('font').replaceWith( "<h1>" + $('font').text() + "</h1>" );
    printDiv();
  }
  function printDiv() 
  {
    var divToPrint=document.getElementById('a');
    var newWin=window.open('');
    newWin.document.open();
    newWin.document.write('<html><style>.form-control {display: block;width: 100%;height: 34px;margin-bottom:15px;padding: 6px 12px;font-size: 14px;background-color: #fff;background-image: -webkit-linear-gradient(rgba(255, 255, 255, 0), rgba(255, 255, 255, 0));border-radius: 0;color: #7a8081;border: 1px solid #9d9a9b;line-height: 1;border-top:0px;border-right:0px;border-left:0px}textarea{width: 100%;padding: 25px 12px !important;border-top:0px;border-right:0px;border-left:0px} </style><body onload="window.print()">Please provide your company information with the form below. We will process your application as soon as possible, and will issue your user id and password by sending it to the email address provided. Approval as a dealer usually takes about 7 - 10 business days. Please mail or fax your completed application to: <br><b>Millage, Inc.</b><br><b>1027 S Broadway St.</b><br><b>Los Angeles, CA 90015</b><br><br><br>'+divToPrint.innerHTML+'</body></html>');
    newWin.document.close();
    window.location.href = "<?php echo base_url().$this->session->group_level.'/apply/'.$for ?>";
  }
</script>