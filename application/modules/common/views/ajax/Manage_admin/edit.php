		<h2><b>{mode} Admin</b></h2>
		<?php 
			$frm_id=array("name"=>"id","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
			echo _form_common("hidden",$frm_id,$user_data['id']);
            
            $name=array("name"=>"username","id"=>"username","class"=>"form-control form-control-line","required"=>"");
			echo form_input($name,$user_data['username'],'Username');

			$email_frm=array("name"=>"email","id"=>"email","class"=>"form-control form-control-line","required"=>"","disabled"=>"");
			echo form_email($email_frm,$user_data['email'],'E-mail');

			$first_name_frm=array("name"=>"first_name","id"=>"first_name","class"=>"form-control form-control-line","required"=>"");
			echo form_input($first_name_frm,$user_data['first_name'],'First Name');

			$last_name_frm=array("name"=>"last_name","id"=>"last_name","class"=>"form-control form-control-line","required"=>"");
			echo form_input($last_name_frm,$user_data['last_name'],'Last Name');

			$phone_frm=array("name"=>"phone","id"=>"phone","class"=>"form-control form-control-line","required"=>"");
			echo form_input($phone_frm,$user_data['phone'],'Phone');
                    
?>
<script type="text/javascript">
	if("{mode}"=="View"){
		$("form :input").prop("disabled", true);
	}
</script>

