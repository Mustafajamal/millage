		<h2><b>{mode} Dealer </b></h2>
		<?php 
			$frm_id=array("name"=>"id","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
			echo _form_common("hidden",$frm_id,$user_data['id']);
            
            $name=array("name"=>"username","id"=>"username","class"=>"form-control form-control-line","required"=>"");
			echo form_input($name,$user_data['username'],'Username');

			$email_frm=array("name"=>"email","id"=>"email","class"=>"form-control form-control-line","required"=>"","disabled"=>"");
			echo form_email($email_frm,$user_data['email'],'E-mail');
 
			$first_name_frm=array("name"=>"first_name","id"=>"first_name","class"=>"form-control form-control-line","required"=>"");
			echo form_input($first_name_frm,$user_data['first_name'],'First Name');

			$last_name_frm=array("name"=>"last_name","id"=>"last_name","class"=>"form-control form-control-line","required"=>"");
			echo form_input($last_name_frm,$user_data['last_name'],'Last Name');

			$phone_frm=array("name"=>"phone","id"=>"phone","class"=>"form-control form-control-line","required"=>"");
			echo form_input($phone_frm,$user_data['phone'],'Phone');
                    
?>
<div class="row">
		<div class="col-md-6">
			<h3>Billing Details</h3>
               <?php 
               // d($cust_data);
                $customer_country_frm=array("name"=>"customer_country","id"=>"customer_country","required"=>"","class"=>"form-control","placeholder"=>"customer country");
                if (!is_null($cust_data)) {
                    echo form_dropdown($customer_country_frm,$countries, array($cust_data['customer_country']),"customer country");
                }else{
                    echo form_dropdown($customer_country_frm,$countries, array(230),"customer country");
                }
                $customer_name_frm=array("name"=>"customer_name","id"=>"customer_name","required"=>"","class"=>"form-control","placeholder"=>"customer name");
                echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data['customer_name']:"","Customer Name");
                $customer_address_frm=array("name"=>"customer_address","id"=>"customer_address","required"=>"","class"=>"form-control","placeholder"=>"customer address");
                echo form_textarea($customer_address_frm, (!is_null($cust_data))?$cust_data['customer_address']:"","Address");
               ?>

                <div class="form-row form-row form-row-first">
                   <!--  <label>Town / City
                        <abbr class="required">*</abbr>
                    </label>
                    <input type="text" class="form-control" placeholder="Town / City"> -->
                    <?php
                        $customer_city_frm=array("name"=>"customer_city","id"=>"customer_city","required"=>"","class"=>"form-control","placeholder"=>"Town / City");
                        echo form_input($customer_city_frm, (!is_null($cust_data))?$cust_data['customer_city']:"","Town / City");
                    ?>
                </div>
                <!--town-->

                <div class="form-row form-row form-row-last">
                    <!-- <label>State / Proviance
                        <abbr class="required">*</abbr>
                    </label>
                    <input type="text" class="form-control" placeholder="State / Proviance"> -->
                    <?php 
                        $customer_state_frm=array("name"=>"customer_state","id"=>"customer_state","required"=>"","class"=>"form-control","placeholder"=>"State / Proviance");
                        echo form_input($customer_state_frm, (!is_null($cust_data))?$cust_data['customer_state']:"","State / Proviance");
                    ?>
                </div>
                <!--city-->

                <div class="clear"></div>
                <div class="form-row form-row form-row-first">
                    <!-- <label>TPostcode / Zip
                        <abbr class="required">*</abbr>
                    </label>
                    <input type="text" class="form-control" placeholder="Postcode / Zip"> -->
                    <?php 
                        $custoemer_zip_code_frm=array("name"=>"custoemer_zip_code","id"=>"custoemer_zip_code","required"=>"","class"=>"form-control","placeholder"=>"Postcode / Zip");
                        echo form_input($custoemer_zip_code_frm, (!is_null($cust_data))?$cust_data['custoemer_zip_code']:"","Postcode / Zip");
                    ?>
                </div>                                            
                <!--zip-->

                <div class="form-row form-row form-row-last">
                    <!-- <label>Phone Number
                        <abbr class="required">*</abbr>
                    </label>
                    <input type="text" class="form-control" placeholder="Phone Number"> -->
                    <?php 
                        $customer_name_frm=array("name"=>"phone","id"=>"phone","required"=>"","class"=>"form-control","placeholder"=>"Phone Number");
                        echo form_input($customer_name_frm, $user_data['phone'],"Phone Number");
                    ?>
                </div>
                <!--postanl-->

                <div class="clear"></div>
                <div class="form-row form-row form-row-wide">
                    <?php 
                        $customer_name_frm=array("name"=>"email","id"=>"user_email","required"=>"","class"=>"form-control","placeholder"=>"Email Address","readonly"=>"");
                        echo form_input($customer_name_frm, $user_data['email'],"Email Address");
                    ?>
                </div>
		</div>
		<div class="col-md-6">
			<h3 id="ship-to-different-address"> Delivery Details 
                <span class="checkbox-wrap pull-right">
                    <input type="checkbox" value="1" id="ship-to-different-address-checkbox">
                    <label>&nbsp;</label>
                </span> 
            </h3>
            <?php 
            $customer_country_frm=array("name"=>"customer_ship_country","id"=>"customer_ship_country","required"=>"","class"=>"form-control ship_data","placeholder"=>"Receiver country");
            if (!is_null($cust_data)) {
                echo form_dropdown($customer_country_frm,$countries, array($cust_data['customer_ship_country']),"Receiver country");
            }else{
                echo form_dropdown($customer_country_frm,$countries, array(230),"Receiver country");
            }
            $customer_name_frm=array("name"=>"customer_ship_name","id"=>"customer_ship_name","required"=>"","class"=>"form-control ship_data","placeholder"=>"Receiver name");
            echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data['customer_ship_name']:"","Receiver Name");
            $customer_address_frm=array("name"=>"customer_ship_address","id"=>"customer_ship_address","required"=>"","class"=>"form-control ship_data","placeholder"=>"Receiver Address");
            echo form_textarea($customer_address_frm, (!is_null($cust_data))?$cust_data['customer_ship_address']:" ","Receiver Address");
           ?>

            <div class="form-row form-row form-row-first">
               <!--  <label>Town / City
                    <abbr class="required">*</abbr>
                </label>
                <input type="text" class="form-control" placeholder="Town / City"> -->
                <?php
                    $customer_city_frm=array("name"=>"customer_ship_city","id"=>"customer_ship_city","required"=>"","class"=>"form-control ship_data","placeholder"=>"Town / City");
                    echo form_input($customer_city_frm, (!is_null($cust_data))?$cust_data['customer_ship_city']:" ","Town / City");
                ?>
            </div>
            <!--town-->

            <div class="form-row form-row form-row-last">
                <!-- <label>State / Proviance
                    <abbr class="required">*</abbr>
                </label>
                <input type="text" class="form-control" placeholder="State / Proviance"> -->
                <?php 
                    $customer_state_frm=array("name"=>"customer_ship_state","id"=>"customer_ship_state","required"=>"","class"=>"form-control ship_data","placeholder"=>"State / Proviance");
                    echo form_input($customer_state_frm, (!is_null($cust_data))?$cust_data['customer_ship_state']:" ","State / Proviance");
                ?>
            </div>
            <!--city-->

            <div class="clear"></div>
            <div class="form-row form-row form-row-first">
                <!-- <label>TPostcode / Zip
                    <abbr class="required">*</abbr>
                </label>
                <input type="text" class="form-control" placeholder="Postcode / Zip"> -->
                <?php 
                    $custoemer_zip_code_frm=array("name"=>"custoemer_ship_zip_code","id"=>"custoemer_ship_zip_code","required"=>"","class"=>"form-control ship_data","placeholder"=>"Postcode / Zip");
                    echo form_input($custoemer_zip_code_frm, (!is_null($cust_data))?$cust_data['custoemer_ship_zip_code']:" ","Postcode / Zip");
                ?>
            </div>                                            
            <!--zip-->

            <div class="form-row form-row form-row-last">
                <!-- <label>Phone Number
                    <abbr class="required">*</abbr>
                </label>
                <input type="text" class="form-control" placeholder="Phone Number"> -->
                <?php 
                    $customer_name_frm=array("name"=>"customer_ship_phone","id"=>"customer_ship_phone","required"=>"","class"=>"form-control ship_data","placeholder"=>"Phone Number");
                    echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data['customer_ship_phone']:" ","Phone Number");
                ?>
            </div>
            <!--postanl-->

            <div class="clear"></div>
            <div class="form-row form-row form-row-wide">
                <?php 
                    $customer_name_frm=array("name"=>"customer_ship_email","id"=>"customer_ship_email","required"=>"","class"=>"form-control ship_data","placeholder"=>"Email Address",);
                    echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data['customer_ship_email']:" ","Email Address");
                ?>
            </div>
		</div> 
	</div>
<script type="text/javascript">
	if("{mode}"=="View"){
		$("form :input").prop("disabled", true);
	}
	$("#ship-to-different-address-checkbox").click(function(event) {
        // console.log($(this).prop('checked', true));
        // console.log($(this).prop('checked', false));
        if(this.checked){
            $(".ship_data").attr('readonly', true);
            $("#customer_ship_country").val($("#customer_country").val());
            $("#customer_ship_name").val($("#customer_name").val());
            $("#customer_ship_address").val($("#customer_address").val());
            $("#customer_ship_city").val($("#customer_city").val());
            $("#customer_ship_state").val($("#customer_state").val());
            $("#custoemer_ship_zip_code").val($("#custoemer_zip_code").val());
            $("#customer_ship_phone").val($("#user_phone").val());
            $("#customer_ship_email").val($("#user_email").val());
            
            console.log($("#checkout_form").serialize());
        }else{
            $(".ship_data").attr('readonly', false);
        }
    });
</script>

