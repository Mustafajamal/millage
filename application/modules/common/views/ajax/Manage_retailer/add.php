<h2><b>Add Customer</b></h2>
<?php 
	$name=array("name"=>"username","id"=>"username","class"=>"form-control form-control-line","required"=>"");
	echo form_input($name,'','Username');

	$email_frm=array("name"=>"email","id"=>"email","class"=>"form-control form-control-line","required"=>"");
	echo form_email($email_frm,'','E-mail');

	$first_name_frm=array("name"=>"first_name","id"=>"first_name","class"=>"form-control form-control-line","required"=>"");
	echo form_input($first_name_frm,'','First Name');

	$last_name_frm=array("name"=>"last_name","id"=>"last_name","class"=>"form-control form-control-line","required"=>"");
	echo form_input($last_name_frm,'','Last Name');
	
	$phone_frm=array("name"=>"phone","min"=>0,"id"=>"phone","class"=>"form-control form-control-line","required"=>"");
	echo form_number($phone_frm,'','Phone');

	$password_frm=array("name"=>"password","id"=>"password","class"=>"form-control form-control-line","required"=>"");
	echo form_password($password_frm,'','Password');

	$c_password_frm=array("name"=>"c_password","id"=>"c_password","class"=>"form-control form-control-line","required"=>"","data-rule-equalTo"=>"#password");
	echo form_password($c_password_frm,'','Confirm Password');
?>
<div class="row">
		<div class="col-md-6">
			<h3>Billing Details</h3>
               <?php 
               // d($cust_data);
                $customer_country_frm=array("name"=>"customer_country","id"=>"customer_country","required"=>"","class"=>"form-control customer_country ","placeholder"=>"customer country");
                
                    echo form_dropdown($customer_country_frm,$countries, array(230),"customer country");
                
                $customer_name_frm=array("name"=>"customer_name","id"=>"customer_name","required"=>"","class"=>"form-control","placeholder"=>"customer name");
                echo form_input($customer_name_frm,"","Customer Name");
                $customer_address_frm=array("name"=>"customer_address","id"=>"customer_address","required"=>"","class"=>"form-control","placeholder"=>"customer address");
                echo form_textarea($customer_address_frm,"","Address");
               ?>

                <div class="form-row form-row form-row-first">
                   <!--  <label>Town / City
                        <abbr class="required">*</abbr>
                    </label>
                    <input type="text" class="form-control" placeholder="Town / City"> -->
                    <?php
                        $customer_city_frm=array("name"=>"customer_city","id"=>"customer_city","required"=>"","class"=>"form-control","placeholder"=>"Town / City");
                        echo form_input($customer_city_frm,"","Town / City");
                    ?>
                </div>
                <!--town-->

                <div class="form-row form-row form-row-last">
                    <!-- <label>State / Proviance
                        <abbr class="required">*</abbr>
                    </label>
                    <input type="text" class="form-control" placeholder="State / Proviance"> -->
                    <?php 
                        $customer_state_frm=array("name"=>"customer_state","id"=>"customer_state","required"=>"","class"=>"form-control","placeholder"=>"State / Proviance");
                        echo form_input($customer_state_frm,"","State / Proviance");
                    ?>
                </div>
                <!--city-->

                <div class="clear"></div>
                <div class="form-row form-row form-row-first">
                    <!-- <label>TPostcode / Zip
                        <abbr class="required">*</abbr>
                    </label>
                    <input type="text" class="form-control" placeholder="Postcode / Zip"> -->
                    <?php 
                        $custoemer_zip_code_frm=array("name"=>"custoemer_zip_code","id"=>"custoemer_zip_code","required"=>"","class"=>"form-control","placeholder"=>"Postcode / Zip");
                        echo form_input($custoemer_zip_code_frm,"","Postcode / Zip");
                    ?>
                </div>                                            
                <!--zip-->

                <div class="form-row form-row form-row-last">
                    <!-- <label>Phone Number
                        <abbr class="required">*</abbr>
                    </label>
                    <input type="text" class="form-control" placeholder="Phone Number"> -->
                    <?php 
                        $customer_name_frm=array("name"=>"phone","min"=>0,"id"=>"user_phone","required"=>"","class"=>"form-control","placeholder"=>"Phone Number");
                        echo form_number($customer_name_frm, "","Phone Number");
                    ?>
                </div>
                <!--postanl-->

                <div class="clear"></div>
                <div class="form-row form-row form-row-wide">
                    <?php 
                        $customer_name_frm=array("name"=>"email","id"=>"user_email","required"=>"","class"=>"form-control","placeholder"=>"Email Address");
                        echo form_input($customer_name_frm, "","Email Address");
                    ?>
                </div>
		</div>
		<div class="col-md-6">
			<h3 id="ship-to-different-address"> Delivery Details 
                <span class="checkbox-wrap pull-right"><small>Same As Above</small>
                    <input type="checkbox" value="1" id="ship-to-different-address-checkbox">
                    <label>&nbsp;</label>
                </span> 
            </h3>
            <?php 
            $customer_country_frm=array("name"=>"customer_ship_country","id"=>"customer_ship_country","required"=>"","class"=>"form-control ship_data customer_country","placeholder"=>"Receiver country");
            
                echo form_dropdown($customer_country_frm,$countries, array(230),"Receiver country");
           
            $customer_name_frm=array("name"=>"customer_ship_name","id"=>"customer_ship_name","class"=>"form-control ship_data","placeholder"=>"Receiver name");
            echo form_input($customer_name_frm,"","Receiver Name");
            $customer_address_frm=array("name"=>"customer_ship_address","id"=>"customer_ship_address","class"=>"form-control ship_data","placeholder"=>"Receiver Address");
            echo form_textarea($customer_address_frm," ","Receiver Address");
           ?>

            <div class="form-row form-row form-row-first">
               <!--  <label>Town / City
                    <abbr class="required">*</abbr>
                </label>
                <input type="text" class="form-control" placeholder="Town / City"> -->
                <?php
                    $customer_city_frm=array("name"=>"customer_ship_city","id"=>"customer_ship_city","class"=>"form-control ship_data","placeholder"=>"Town / City");
                    echo form_input($customer_city_frm," ","Town / City");
                ?>
            </div>
            <!--town-->

            <div class="form-row form-row form-row-last">
                <!-- <label>State / Proviance
                    <abbr class="required">*</abbr>
                </label>
                <input type="text" class="form-control" placeholder="State / Proviance"> -->
                <?php 
                    $customer_state_frm=array("name"=>"customer_ship_state","id"=>"customer_ship_state","class"=>"form-control ship_data","placeholder"=>"State / Proviance");
                    echo form_input($customer_state_frm," ","State / Proviance");
                ?>
            </div>
            <!--city-->

            <div class="clear"></div>
            <div class="form-row form-row form-row-first">
                <!-- <label>TPostcode / Zip
                    <abbr class="required">*</abbr>
                </label>
                <input type="text" class="form-control" placeholder="Postcode / Zip"> -->
                <?php 
                    $custoemer_zip_code_frm=array("name"=>"custoemer_ship_zip_code","id"=>"custoemer_ship_zip_code","class"=>"form-control ship_data","placeholder"=>"Postcode / Zip");
                    echo form_input($custoemer_zip_code_frm," ","Postcode / Zip");
                ?>
            </div>                                            
            <!--zip-->

            <div class="form-row form-row form-row-last">
                <!-- <label>Phone Number
                    <abbr class="required">*</abbr>
                </label>
                <input type="text" class="form-control" placeholder="Phone Number"> -->
                <?php 
                    $customer_name_frm=array("name"=>"customer_ship_phone","min"=>0,"id"=>"customer_ship_phone","class"=>"form-control ship_data","placeholder"=>"Phone Number");
                    echo form_number($customer_name_frm," ","Phone Number");
                ?>
            </div>
            <!--postanl-->

            <div class="clear"></div>
            <div class="form-row form-row form-row-wide">
                <?php 
                    $customer_name_frm=array("name"=>"customer_ship_email","id"=>"customer_ship_email","class"=>"form-control ship_data","placeholder"=>"Email Address",);
                    echo form_input($customer_name_frm,'',"Email Address");
                ?>
            </div>
		</div> 
	</div> 
<script type="text/javascript">
	$("#phone").change(function(event) {
		$("#user_phone").val($("#phone").val());
        $("#user_email").val($("#email").val());
        $("#customer_name").val($("#first_name").val()+" "+$("#last_name").val());
	});
	
	$("#ship-to-different-address-checkbox").click(function(event) {
        // console.log($(this).prop('checked', true));
        // console.log($(this).prop('checked', false));
        if(this.checked){
            $(".ship_data").attr('readonly', true);
            $("#customer_ship_country").val($("#customer_country").val());
            $("#customer_ship_name").val($("#customer_name").val());
            $("#customer_ship_address").val($("#customer_address").val());
            $("#customer_ship_city").val($("#customer_city").val());
            $("#customer_ship_state").val($("#customer_state").val());
            $("#custoemer_ship_zip_code").val($("#custoemer_zip_code").val());
            $("#customer_ship_phone").val($("#user_phone").val());
            $("#customer_ship_email").val($("#user_email").val());
            
            console.log($("#checkout_form").serialize());
        }else{
            $(".ship_data").attr('readonly', false);
        }
    });
    $('.customer_country').select2();
</script>


<!-- customer_city
customer_state
custoemer_zip_code
customer_country
customer_ship_address
customer_ship_city
customer_ship_state
custoemer_ship_zip_code
customer_ship_country -->