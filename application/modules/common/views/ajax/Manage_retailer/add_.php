<h2><b>Add Retailer</b></h2>
<?php 
	$name=array("name"=>"username","id"=>"username","class"=>"form-control form-control-line","required"=>"");
	echo form_input($name,'','Username');

	$email_frm=array("name"=>"email","id"=>"email","class"=>"form-control form-control-line","required"=>"");
	echo form_email($email_frm,'','E-mail');

	$first_name_frm=array("name"=>"first_name","id"=>"first_name","class"=>"form-control form-control-line","required"=>"");
	echo form_input($first_name_frm,'','First Name');

	$last_name_frm=array("name"=>"last_name","id"=>"last_name","class"=>"form-control form-control-line","required"=>"");
	echo form_input($last_name_frm,'','Last Name');

	$phone_frm=array("name"=>"phone","id"=>"phone","class"=>"form-control form-control-line","required"=>"");
	echo form_input($phone_frm,'','Phone');

	$password_frm=array("name"=>"password","id"=>"password","class"=>"form-control form-control-line","required"=>"");
	echo form_password($password_frm,'','Password');

	$c_password_frm=array("name"=>"c_password","id"=>"c_password","class"=>"form-control form-control-line","required"=>"","data-rule-equalTo"=>"#password");
	echo form_password($c_password_frm,'','Confirm Password');
?>