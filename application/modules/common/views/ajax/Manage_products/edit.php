<h2><b>{mode} Product</b></h2>
<?php 
$frm_id=array("name"=>"watch_id","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
echo _form_common("hidden",$frm_id,$product_data['watch_id']);
?>
<div class="form-group">
  <label class="control-label" for="product_number">Series</label>
  <div class="error_block">
      <select name="series_id" id="series_id" class="form-control form-control-line" required="">
      	{series_data}
      </select> 
      
  </div>
</div>
<div class="form-group">
  <label class="control-label" for="product_number">Style</label>
  <div class="error_block">
      <select name="style_id" id="style_id" class="form-control form-control-line" required="">
      	{style_data}
      </select>
      
  </div>
</div>
<div class="form-group">
  <label class="control-label" for="product_number">Category</label>
  <div class="error_block">
      <select name="category_id" id="category_id" class="form-control form-control-line" required="">
      	{category_data}
      </select>
      
  </div>
</div>

<?php 
	$name=array("name"=>"product_number","id"=>"product_number","class"=>"form-control form-control-line","required"=>"","placeholder"=>"Product Number");
	echo form_input($name,$product_data['product_number'],'Product Number');
	$name=array("name"=>"product_name","id"=>"product_name","class"=>"form-control form-control-line","required"=>"","placeholder"=>"Product Name");
	echo form_input($name,$product_data['product_name'],'Product Name');
?>
<div class="row">
  <?php 
    foreach ($features as $name) {
        echo ' <div class="col-md-4">';
        $name_frm=array("name"=>$name,"id"=>$name,"class"=>"form-control form-control-line","required"=>"","placeholder"=>ucwords(str_replace('_', ' ',$name)));
        echo form_input($name_frm,$product_data[$name],ucwords(str_replace('_', ' ',$name)));
        echo ' </div>';
      ?>
        <!-- <div class="form-group col-md-2">
          <label class="control-label" for="product_number"><?php echo ucwords(str_replace('_', ' ',$name)); ?></label>
          <div class="error_block">
          <input id='toggle-one' type='checkbox' name="<?php echo $name; ?>" class='toggle_button'>
          </div>
        </div> -->
      <?php
    }
  ?>
</div>
<script type="text/javascript">
	$('.toggle_button').bootstrapToggle({
                  on: 'Yes',
                  off: 'No'
                });
	$('select').select2();

</script>
<script type="text/javascript">
  if("{mode}"=="View"){
    $("form :input").prop("disabled", true);
  }
</script>
