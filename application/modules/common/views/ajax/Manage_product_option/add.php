<h2><b>Add Product Option / Color</b></h2>
<?php 
	$name=array("name"=>"dial_color","id"=>"dial_color","class"=>"form-control form-control-line","required"=>"");
	echo form_input($name,'','Dial Color');
	$name=array("name"=>"dial_stock","step"=>"any","id"=>"dial_stock","class"=>"form-control form-control-line","required"=>"");
	echo form_number($name,'','Item Quantity');
 
 /************************** Retail Price ****************************************/
  	$retail_price_radio=array("name"=>"retail_price_radio","id"=>"retail_price_radio","class"=>" form-control-line","required"=>"");
  	echo custom_form_checkbox("radio",$retail_price_radio,array("0"=>"Add Price","1"=>"Generate Inquiry"),array(0),"Select Option for Retail Price"); 
  	
  	
	$name=array("name"=>"retail_price","step"=>"any","min"=>0.0,"id"=>"retail_price","class"=>"form-control form-control-line","required"=>"","placeholder"=>"0.0");
	echo form_number($name,'','Retail Price');
	
/**************************** Dealer Price *****************************************/
	$dealer_price_radio=array("name"=>"dealer_price_radio","id"=>"dealer_price_radio","class"=>" form-control-line","required"=>"");
  	echo custom_form_checkbox("radio",$dealer_price_radio,array("0"=>"Add Price","1"=>"Generate Inquiry"),array(0),"Select Option for Dealer Price"); 
  	
  	
	$name=array("name"=>"dealer_price","step"=>"any","min"=>0.0,"id"=>"dealer_price","class"=>"form-control form-control-line","required"=>"","placeholder"=>"0.0");
	echo form_number($name,'','Dealer Price');
	
/**************************** Distributor Price *************************************/
	$distributor_price_radio=array("name"=>"distributor_price_radio","id"=>"distributor_price_radio","class"=>" form-control-line","required"=>"");
  	echo custom_form_checkbox("radio",$distributor_price_radio,array("0"=>"Add Price","1"=>"Generate Inquiry"),array(0),"Select Option for Distributor Price"); 
  	
  	
	$name=array("name"=>"distributor_price","step"=>"any","min"=>0.0,"id"=>"distributor_price","class"=>"form-control form-control-line","required"=>"","placeholder"=>"0.0");
	echo form_number($name,'','Distributor Price');
	
/********************************* Images ********************************************/
	$name=array("name"=>"main_image","id"=>"main_image","class"=>"form-control form-control-line","required"=>"");
	echo _form_common("file",$name,'','Main Image');	

	$name=array("name"=>"other_image[]","id"=>"other_image","class"=>"form-control form-control-line","multiple"=>"");
	echo _form_common("file",$name,'','Other Images <span class="label label-info">You can Select Multiple</span>');	
?>
<script type="text/javascript">
	$("input[type='radio']").click(function() {
	  var retail = $("input[name='retail_price_radio']:checked").val();
	  var dealer = $("input[name='dealer_price_radio']:checked").val();
	  var distributor = $("input[name='distributor_price_radio']:checked").val();
	  if(retail==0)
	  {
	    $("#retail_price").attr('disabled',false);
	  }
	  else
	  {

	    $("#retail_price").attr('disabled',true).val("0.0");
	  }
	  if(dealer==0)
	  {
	    $("#dealer_price").attr('disabled',false);
	  }
	  else
	  {
	    $("#dealer_price").attr('disabled',true).val("0.0");
	  }
	  if(distributor==0)
	  {
	    $("#distributor_price").attr('disabled',false);
	  }
	  else
	  {
	    $("#distributor_price").attr('disabled',true).val("0.0");
	  }
	});

</script>