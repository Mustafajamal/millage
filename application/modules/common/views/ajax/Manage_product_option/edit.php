<h2><b>{mode} Product Option / Color</b></h2>
<?php 
	$frm_id=array("name"=>"watch_option_id","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
	echo _form_common("hidden",$frm_id,$product_option_data['watch_option_id']);

	$name=array("name"=>"dial_color","id"=>"dial_color","class"=>"form-control form-control-line","required"=>"");
	echo form_input($name,$product_option_data['dial_color'],'Dial Color');
	$name=array("name"=>"dial_stock","step"=>"any","id"=>"dial_stock","class"=>"form-control form-control-line","required"=>"");
	echo form_number($name,$product_option_data['dial_stock'],'Item Quantity');

/************************** Retail Price ****************************************/
  	$retail_price_radio=array("name"=>"retail_price_radio","id"=>"retail_price_radio","class"=>" form-control-line","required"=>"");
  	echo custom_form_checkbox("radio",$retail_price_radio,array("0"=>"Add Price","1"=>"Generate Inquiry"),array(($product_option_data['retail_price']!=0.0)?0:1),"Select Option for Retail Price"); 

	$name=array("name"=>"retail_price","step"=>"any","id"=>"retail_price","class"=>"form-control form-control-line","required"=>"");
	echo form_number($name,$product_option_data['retail_price'],'Retail Price');
	/**************************** Dealer Price *****************************************/
	$dealer_price_radio=array("name"=>"dealer_price_radio","id"=>"dealer_price_radio","class"=>" form-control-line","required"=>"");
  	echo custom_form_checkbox("radio",$dealer_price_radio,array("0"=>"Add Price","1"=>"Generate Inquiry"),array(($product_option_data['dealer_price']!=0.0)?0:1),"Select Option for Dealer Price"); 

	$name=array("name"=>"dealer_price","step"=>"any","id"=>"dealer_price","class"=>"form-control form-control-line","required"=>"");
	echo form_number($name,$product_option_data['dealer_price'],'Dealer Price');
	/**************************** Distributor Price *************************************/
	$distributor_price_radio=array("name"=>"distributor_price_radio","id"=>"distributor_price_radio","class"=>" form-control-line","required"=>"");
  	echo custom_form_checkbox("radio",$distributor_price_radio,array("0"=>"Add Price","1"=>"Generate Inquiry"),array(($product_option_data['distributor_price']!=0.0)?0:1),"Select Option for Distributor Price"); 

	$name=array("name"=>"distributor_price","step"=>"any","id"=>"distributor_price","class"=>"form-control form-control-line","required"=>"");
	echo form_number($name,$product_option_data['distributor_price'],'Distributor Price');

	$name=array("name"=>"main_image","id"=>"main_image","class"=>"form-control form-control-line");
	echo _form_common("file",$name,'','Main Image');
	?>
		<img src="<?php echo UPLOAD_URL.'products/'.$watch_data["product_number"].'/'.$product_option_data["dial_color"].'/'.$product_option_data["main_image"]; ?>" height=100px width=100px>
	<?php
	$name=array("name"=>"other_image[]","id"=>"other_image","class"=>"form-control form-control-line","multiple"=>"");
	echo _form_common("file",$name,'','Other Images <span class="label label-info">You can Select Multiple</span>');	
	$other_image_arr=explode(',',$product_option_data['other_image']);
	foreach ($other_image_arr as $value) {
		?>
		<img src="<?php echo UPLOAD_URL.'products/'.$watch_data["product_number"].'/'.$product_option_data["dial_color"].'/'.$value; ?>" height=100px width=100px>
		<?php
	}
?>

<script type="text/javascript">
	if("{mode}"=="View"){
		$("form :input").prop("disabled", true);
	}
	$("input[type='radio']").click(function() {
	  var retail = $("input[name='retail_price_radio']:checked").val();
	  var dealer = $("input[name='dealer_price_radio']:checked").val();
	  var distributor = $("input[name='distributor_price_radio']:checked").val();
	  if(retail==0)
	  {
	    $("#retail_price").attr('disabled',false);
	  }
	  else
	  {

	    $("#retail_price").attr('disabled',true).val("0.0");
	  }
	  if(dealer==0)
	  {
	    $("#dealer_price").attr('disabled',false);
	  }
	  else
	  {
	    $("#dealer_price").attr('disabled',true).val("0.0");
	  }
	  if(distributor==0)
	  {
	    $("#distributor_price").attr('disabled',false);
	  }
	  else
	  {
	    $("#distributor_price").attr('disabled',true).val("0.0");
	  }
	});
	$("input[type='radio']").each(function(index, el) {
		if($(this).attr("checked")=="checked"){
			if($(this).val()=="1"){
				id=$(this).attr('id');
				id=id.replace("_radio","");
				// alert(id);
				$("#"+id).attr('disabled',true).val("0.0");
			}
		}
	});
</script>