 <h2><b>{mode} Customer Orders </b></h2>
                
  <?php
          $orders=array();
          $class="btn-primary";
          $i=0;
          
          // d($order_data);
          foreach ($order_data as $order) {
           $frm_id=array("name"=>"order_id","class"=>"form-control","placeholder"=>"order_id","required"=>"");
      echo _form_common("hidden",$frm_id,$order->order_id);
              $watch_option=$this->custom->getSingleRow("watch_options_master",array('watch_option_id'=>$order->watch_option_id));
              $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_option->watch_id));
              $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
              $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
              $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
              if (!in_array($order->order_id,$orders)) { 
                  $orders[]=$order->order_id;
                  $i++;
                  // if ($order->order_status=="SHIPPED") {
                  //     $class="btn-warning";
                  // }
                  // if ($order->order_status=="SUCCESS") {
                  //     $class="btn-success";
                  // }
                  // if ($order->order_status=="CANCELED") {
                  //     $class="btn-danger";
                  // }
                  if ($i!=1) {
                    echo "</div>
                        </div>";                                      
                  }
                   ?>
                     <!--  <h2>Panel Group</h2>
                      <p>The panel-group class clears the bottom-margin. Try to remove the class and see what happens.</p> -->
                        <div class="panel panel-default">
                          <div class="panel-heading">OrderId : <?php echo strtotime($order->order_date);?><span class="pull-right">Date: <?php echo $order->order_date;?></span></div>
                          <div class="panel-body">
                              <div class="row">
                                <?php 
                        $frm_id=array("name"=>"watch_op_id[".$order->watch_option_id."]","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
                          echo _form_common("hidden",$frm_id,$order->watch_option_id);

                         $frm_id=array("name"=>"price[".$order->watch_option_id."]","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
                        echo _form_common("hidden",$frm_id,$watch_option->retail_price);?>
                                    <div class="col-md-3">
                                        <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>" height="150px" width="150px">
                                        <br><br>
                                        <?php if(!is_null($order->order_note)){?>
                                        <div class="row" style="overflow: auto;">
                                          <div class="col-md-12">
                                        <b>Order Note :</b><br><?php echo $order->order_note;?>
                                        </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-5">


                                        <h2><?php echo $product_data->product_name." "; ?> 
                                      
                                        </h2>
                                        <p>Color : <?php echo $watch_option->dial_color ?></p>
                                        <p>Series : <?php echo $series_data ?></p>
                                        <p>Category : <?php echo $category_data ?></p>
                                    </div>
                                    <div class="col-md-4">
                                          <h5 class="pull-right">Quantity 
                                          <input type="number" min="1" max="<?php echo $watch_option->dial_stock;?>"  name="quantity[<?php echo $order->watch_option_id;?>]" style="width: 20%" class="input-text qty " value="<?php echo $order->quantity;  ?>"> 
                                             X&nbsp; $ <?php echo $watch_option->retail_price ?> = $<?php echo number_format($watch_option->retail_price*$order->quantity,2) ?></h5>
                                    </div>
                              </div>
                              
              <?php }else{ ?>
              <HR>
                      <div class="row">
                        <?php 
                        $frm_id=array("name"=>"watch_op_id[".$order->watch_option_id."]","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
                          echo _form_common("hidden",$frm_id,$order->watch_option_id);

                         $frm_id=array("name"=>"price[".$order->watch_option_id."]","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
                        echo _form_common("hidden",$frm_id,$watch_option->retail_price);?>
                                    <div class="col-md-3">
                                        <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>" height="150px" width="150px"><br><br>
                                        <?php if(!is_null($order->order_note)){?>
                                        <div class="row" style="overflow: auto;">
                                          <div class="col-md-12">
                                        <b>Order Note :</b><br><?php echo $order->order_note;?>
                                        </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-5">


                                        <h2><?php echo $product_data->product_name." "; ?> 
                                      
                                        </h2>
                                        <p>Color : <?php echo $watch_option->dial_color ?></p>
                                        <p>Series : <?php echo $series_data ?></p>
                                        <p>Category : <?php echo $category_data ?></p>
                                    </div>
                                    <div class="col-md-4">
                                          <h5 class="pull-right">Quantity 
                                          <input type="number" min="1" max="<?php echo $watch_option->dial_stock;?>" name="quantity[<?php echo $order->watch_option_id;?>]" style="width: 20%" class="input-text qty " value="<?php echo $order->quantity;  ?>"> 
                                             X&nbsp; $ <?php echo $watch_option->retail_price ?> = $<?php echo number_format($watch_option->retail_price*$order->quantity,2) ?></h5>
                                    </div>
                              </div>
                                        
              <?php } ?>
          <?php } ?>

<script type="text/javascript">
  if("{mode}"=="View"){
    $("form :input").prop("disabled", true);
  }
  $("form:input").val();
</script>
