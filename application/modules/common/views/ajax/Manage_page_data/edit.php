<h2><b>{mode} Product</b></h2>
<?php 
$frm_id=array("name"=>"section_id","class"=>"form-control","placeholder"=>"Department Name","required"=>"");
echo _form_common("hidden",$frm_id,$cat_data['section_id']);?>

<div class="form-group">
  <label class="control-label" for="product_number">Page Category</label>
  <div class="error_block">
      <select name="page_category_id" id="page_category_id" class="form-control form-control-line" required="">
      	{category_data}
      </select>
      
  </div>
</div>

<?php
$name=array("name"=>"section_name","id"=>"section_name","class"=>"form-control form-control-line","required"=>"","placeholder"=>"Section Name");
	echo form_input($name,$cat_data['section_name'],'Section Name');

?>
<div class="form-group">
  <label class="control-label" for="product_number">Is File / Content</label>
  <div class="error_block">
      <input type="radio" class="radio_content" name="file_contents" <?php if (!is_null($cat_data['file_content']) && $cat_data['file_content']!="") {echo "checked";} ?> required="" value="file" >File
      <input type="radio" class="radio_content"  name="file_contents" <?php if (!is_null($cat_data['page_data']) &&  $cat_data['page_data']!="") {echo "checked";} ?> required="" value="content" style="margin-left: 10px">Content
  </div>
</div>
  <div class="form-group" id='file'>
  <label class="control-label" for="product_number">Browse File</label>
  <div class="error_block">
       <input  type='file' name="file_content">
  </div>
  <a href="<?php echo UPLOAD_URL.'cms_files/'.$cat_data["file_content"]; ?>">Existing File</a>
</div>
<div class="form-group" id="page_data_1">
  <label class="control-label" for="product_number">Page Content</label>
  <div class="error_block">
    <textarea name="page_data" id="page_data" rows="10" cols="80"><?php echo $cat_data["page_data"] ?></textarea>
  </div>
</div>

<script type="text/javascript">
  CKEDITOR.replace( 'page_data',{
    removeButtons : 'Underline,Subscript,Superscript,Source,Image',
  } );
	if("{mode}"=="View"){
		$("form :input").prop("disabled", true);
	}
	$('select').select2();
  $("#page_data_1").hide();
  $("#file").hide();
  $(".radio_content").each(function(index, el) {
        var rdvalue = "";
        rdvalue_check=$(this).attr('checked');
        if (rdvalue_check=="checked") {
          rdvalue =$(this).val();  
        }
      if(rdvalue=="file")
      {
        $("#page_data_1").hide();
        $("#file").show();
      }
      if(rdvalue=="content")
      {
        $("#page_data_1").show();
        $("#file").hide();
      }
    });
    $("input[type='radio']").click(function() {
      $("#file_content").attr('disabled', true);
      $("#page_data").attr('disabled', true);
      var rdvalue = $("input[name='file_contents']:checked").val();
      if(rdvalue=="file")
      {
        $("#page_data_1").hide();
        $("#file").show();
        $("#file_content").attr('disabled', false);
      }
      if(rdvalue=="content")
      {
        $("#page_data_1").show();
        $("#file").hide();
        $("#page_data").attr('disabled', false);
      }
    });
  
 	
 	
</script>