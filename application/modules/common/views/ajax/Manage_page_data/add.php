<h2><b>Add Page Data</b></h2>
<div class="form-group">
  <label class="control-label" for="product_number">Page Category</label>
  <div class="error_block">
      <select name="page_category_id" id="page_category_id" class="form-control form-control-line" required="">
      	{category_data}
      </select>
      
  </div>
</div>
<?php 
  $name=array("name"=>"section_name","id"=>"section_name","class"=>"form-control form-control-line","required"=>"","placeholder"=>"section name");
  echo form_input($name,'','Section Name');
?>
<div class="form-group">
  <label class="control-label" for="product_number">Is File / Content</label>
  <div class="error_block">
      <input type="radio"  name="file_contents"  required="" value="file" >File
      <input type="radio"  name="file_contents"  required="" value="content" checked="true" style="margin-left: 10px">Content
  </div>
</div>
<div class="form-group" id='file'>
  <label class="control-label" for="product_number">Browse File</label>
  <div class="error_block">
       <input  type='file' name="file_content">
  </div>
</div>
<div class="form-group" id="page_data_1">
  <label class="control-label" for="product_number">Page Content</label>
  <div class="error_block">
  <!-- <textarea name="page_data" ></textarea> -->
  <textarea name="page_data" id="page_data" rows="10" cols="80"></textarea>
  </div>
</div>
<script type="text/javascript">
$('select').select2();
$("#file").hide();
$("input[type='radio']").click(function() {
  var rdvalue = $("input[name='file_contents']:checked").val();
  if(rdvalue=="file")
  {
    $("#page_data_1").hide();
    $("#file").show();
  }
  if(rdvalue=="content")
  {
    $("#page_data_1").show();
    $("#file").hide();
  }
});
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  CKEDITOR.replace( 'page_data',{
    removeButtons : 'Underline,Subscript,Superscript,Source,Image',
  } );
</script>