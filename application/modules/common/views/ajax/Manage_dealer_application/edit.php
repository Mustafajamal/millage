		<h2><b>{mode} Information </b></h2>
    <div class="row">
      <div class="col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-md-10">
    		<?php 
    			$millage_account_no=array("name"=>"millage_account_no","id"=>"millage_account_no","class"=>"form-control","placeholder"=>"Millage Account No");
            echo form_input($millage_account_no,$dealership["millage_account_no"],'Millage Account No','');
        
            $corporation=array("name"=>"corporation","id"=>"textinput","class"=>"form-control input-md","required"=>"required","placeholder"=>"Corporation or Owner");
            echo form_input($corporation,$dealership["corporation"],"Corporation or Owner");
            
            $dba_name=array("name"=>"dba_name","id"=>"dba_name","class"=>"form-control input-md","required"=>"required","placeholder"=>"DBA Name");
            echo form_input($dba_name,$dealership["dba_name"],"DBA Name");
            
            $federal_tax_id=array("name"=>"federal_tax_id","id"=>"federal_tax_id","class"=>"form-control input-md","placeholder"=>"Federal Tax ID");
            echo form_input($federal_tax_id,$dealership["federal_tax_id"],"Federal Tax ID:");
            
            $contact_person=array("name"=>"contact_person","id"=>"contact_person","class"=>"form-control input-md","required"=>"required","placeholder"=>"Contact Person : ");
            echo form_input($contact_person,$dealership["contact_person"],"Contact Person");
            
            $contact_person_position=array("name"=>"contact_person_position","id"=>"contact_person_position","class"=>"form-control input-md","required"=>"required","placeholder"=>"Contact Person's Position:");
            echo form_input($contact_person_position,$dealership["contact_person_position"],"Contact Person's Position");
            
            $business_start_date=array("name"=>"business_start_date","id"=>"business_start_date","class"=>"form-control input-md","required"=>"required","placeholder"=>"Business start date");
            echo form_input($business_start_date,$dealership["business_start_date"],"Business start date");
            ?>
          <!-- Form Name -->
          <legend><u>Business Address</u></legend>
              <?php
                $address=array("name"=>"address","id"=>"address","class"=>"form-control","required"=>"required");      
                echo form_textarea($address,$dealership["address"],"Address : ");

                $city=array("name"=>"city","id"=>"city","class"=>"form-control input-md","required"=>"required","placeholder"=>"City : ");
                echo form_input($city,$dealership["city"],"City");
                
                $zipcode=array("name"=>"zipcode","id"=>"zipcode","class"=>"form-control input-md","required"=>"required","placeholder"=>"ZipCode : ");
                echo form_input($zipcode,$dealership["zipcode"],"ZipCode : ");
                
                $state=array("name"=>"state","id"=>"state","class"=>"form-control input-md","required"=>"required");
                echo form_input($state,$dealership["state"],"State");
                
                $state=array("name"=>"country","id"=>"country","class"=>"form-control input-md","required"=>"required");
              ?>
              <div class="form-group">
                <label class="control-label" for="state">Country</span></label>
                <div class="error_block">
                    <select name='country' id='country' class='form-control input-md' required='required'>
                    <?php echo "<option value='{$country}'>{$country}</option>}";
                    ?>
                </select>
                </div>
              </div>
              <?php
                $at_this_location_since=array("name"=>"at_this_location_since","id"=>"at_this_location_since","class"=>"form-control input-md","required"=>"required","placeholder"=>"At This Location Since : ");
                        echo form_input($at_this_location_since,$dealership["at_this_location_since"],"At This Location Since : ");
                        ?>
           
          <legend><u>Communication Data</u></legend>
          <div class="row">
            <div class="col-md-6">
              <?php
                $telephone=array("name"=>"telephone","id"=>"telephone","class"=>"form-control","required"=>"required");      
                  echo form_number($telephone,$dealership["telephone"],"Telephone ");
                ?>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
              <?php
                $fax=array("name"=>"fax","id"=>"fax","class"=>"form-control");      
                echo form_input($fax,$dealership["fax"],"Fax");
                ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <?php
                $email=array("name"=>"email","class"=>"form-control","required"=>"required");      
                echo form_email($email,$dealership["email"],"Email");
                ?>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
              <?php
                $website=array("name"=>"website","id"=>"website","class"=>"form-control");      
                echo form_input($website,$dealership["website"],"Website");
                ?>
            </div>
          </div>
          <div class="row">
            <legend><u>Business Profile Data</u></legend>

            <div class="row col-md-6 col-sm-6 col-xs-12">
             
              <div class="col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                <?php
                    $dealer_location_list=array("name"=>"dealer_location_list","id"=>"dealer_location_list");  
                    $dealer_location=explode(",", $dealership["dealer_location_list"]);  
                    echo custom_form_checkbox("checkbox",$dealer_location_list,array("destination_store"=>"Destination Store","website"=>"Web Site","kiosk"=>"Kiosk","mall_location"=>"Mall Location","shopping_district"=>"Shopping District","swap_outdoor_market"=>"Swap / Outdoor Market"),$dealer_location,'Business Location Type:','','','<br/>');
                 ?>
              </div>
            </div>
            <div class="row col-md-6 col-sm-6 col-xs-12">
             <div class="col-md-offset-1 col-sm-offset-1 col-xs-offset-1">   
              <?php
                $type_of_retail=array("name"=>"type_of_retail","id"=>"type_of_retail");  
                $dealer_location=explode(",", $dealership["type_of_retail"]);  
                echo custom_form_checkbox("checkbox",$type_of_retail,array("fashion_boutique"=>"Fashion Boutique","accessories"=>"Accessories","sporting_goods"=>"Sporting Goods","jewelry_store"=>"Jewelry Store","general_gifts"=>"General Gifts","discount_value_store"=>"Discount Value Store","watch_store"=>"Watch Store"),$dealer_location,'Type of retail:','','','<br/>');
             ?>
            </div>
          </div>
        </div>
          <div class="row">
             <div class="row col-md-6 col-sm-6 col-xs-12">
                <div class="col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                   
                    <?php
                        $target_customers=array("name"=>"target_customers","id"=>"target_customers");
                        $target_customer=explode(",", $dealership["target_customers"]);  
                        echo custom_form_checkbox("checkbox",$target_customers,array("male"=>"Male","female"=>"Female","unisex"=>"Unisex"),$target_customer,'Target customers:','','','<br/>');
                     ?>
              </div>
            </div>
            <div class=" row col-md-6 col-sm-6 col-xs-12">
              <div class=" col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                  <?php
                      $customer_age_range=array("name"=>"customer_age_range","id"=>"customer_age_range");
                      $customer_age=explode(",", $dealership["customer_age_range"]);  
                      echo custom_form_checkbox("checkbox",$customer_age_range,array("0_17"=>"0 - 17","18_23"=>"18 - 23","24_30"=>"24 - 30","31_40"=>"31 - 40","41_55"=>"41 - 55","55"=>"55+"),$customer_age,'Customer age range:','','','<br/>');
                   ?>
            </div>
          </div>
    </div>
</div>
<script type="text/javascript">
	if("{mode}"=="View"){
		$("form :input").prop("disabled", true);
	}
</script>

