<div class="breadcrumbs">

    <!--main banner-->
    <main class="main primary-padding">
        <div class="product-list">
            <div class="container">
                <!-- <h2 class="text-center"><?php echo $category_name.$style_name; ?></h2> -->
                <div class="row">                
                    <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                        <h2>MY ORDERS</h2>
                        <?php
                            $orders=array();
                            $class="box-primary";
                            $i=0;
                            // d($order_data);
                            foreach ($order_data as $order) {
                                $watch_option=$this->custom->getSingleRow("watch_options_master",array('watch_option_id'=>$order->watch_option_id));
                                $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_option->watch_id));
                                $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
                                $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
                                $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
                                if (!in_array($order->order_id,$orders)) { 
                                    $orders[]=$order->order_id;
                                    $i++;
                                    if ($order->order_status=="SHIPPED") {
                                        $class="box-warning";
                                    }
                                    if ($order->order_status=="SUCCESS") {
                                        $class="box-success";
                                    }
                                    if ($order->order_status=="CANCELED") {
                                        $class="box-danger";
                                    }
                                    if ($i!=1) {
                                        echo "
                                                </table>
                                            </div>
                                        </div>
                                        ";
                                    }
                                    ?>
                                    <div class="box <?php echo $class; ?>">
                                        <div class="box-header">
                                            Order Id: <?php echo strtotime($order->order_date); ?>
                                        </div>
                                        <div class="box-body">
                                            <table>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                    <th>Total</th>
                                                </tr>
                                                <tr>
                                                    <th><img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>" alt="" height="500px" width="150px"> </th>
                                                    <th><?php echo $order->quantity ?></th>
                                                    <th>$<?php echo $watch_option->retail_price ?></th>
                                                    <th>$<?php echo number_format($watch_option->retail_price*$order->quantity,2) ?></th>
                                                </tr>
                                <?php }else{ ?>
                                        <tr>
                                                    <th><img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>" alt="" height="500px" width="150px"> </th>
                                                    <th><?php echo $order->quantity ?></th>
                                                    <th>$<?php echo $watch_option->retail_price ?></th>
                                                    <th>$<?php echo number_format($watch_option->retail_price*$order->quantity,2) ?></th>
                                                </tr>
                                <?php } ?>
                        <?php } ?>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<link rel="stylesheet" type="text/css" href="https://i4.sdlcdn.com/css/15nov1511154358580/metroUI/myOrders.min.css">
<link rel="stylesheet" type="text/css" href="https://i2.sdlcdn.com/css/15nov1511154358580/metroUILess/final/base.css">
<div class="outerOrderDetails">
   <input type="hidden" value="7990058275" class="orderCode">
   <div class="orderId">Order ID: 7990058275 (1 Item)
      <span class="OrdDetails sd-tour" data-step="4" data-tour-content="Payment and order details">
      <a href="/orderSummary?order=7990058275&amp;code=75370dc2fbc525e1e472cd8f20134620">
      <span class="viewOrder btn btn-theme-secondary">DETAILS</span>
      </a>
      </span>
   </div>
   <div class="ordDate">Placed on 19 Aug, 2015</div>
   <div class="subOrderDetails borderSubOrder">
      <input type="hidden" value="" class="neftAccName">
      <input type="hidden" value="" class="neftAccNo">
      <input type="hidden" value="" class="neftAccIFSC">
      <input type="hidden" id="referenceCode" value="FCDA5394594">
      <input type="hidden" id="ndrDetailsDTO" value="">
      <input type="hidden" id="ndrDetailsDTO" value="">
      <input type="hidden" id="ndrDetailsDTO" value="">
      <div class="ordContent">
         <input type="hidden" value="10257442911" class="softBundle_id">
         <span class="subOrdImage" pogid="634407752835" orderid="7990058275">
         <a href="https://www.snapdeal.com/product/micromax-canvas-spark/634407752835" v="p" ispartialsearch="" categoryid="12" class="subOrderImage" pogid="" pos=";" hidomntrack="">
         <img alt="" title="" class="gridViewImage" anim="true" border="0" src="https://n2.sdlcdn.com/imgs/a/y/z/CanvasWH-41b50.jpg">
         </a>
         </span>
         <span class="softBundleParent">
            <a class="subOrdName" href="https://www.snapdeal.com/product/micromax-canvas-spark/634407752835" pogid="634407752835" orderid="7990058275">
               <div>Micromax Canvas Spark (8GB)</div>
            </a>
            <!-- For the O2O pickup flow -->
            <div class="pinnacle-product-attrs">
               <span class="pinnacle-product-attr">
               <span class="pinn-attr-key">Color:</span>
               <span class="pinn-attr-value">White</span>
               </span>
            </div>
            <!-- For the O2O pickup flow -->
            <span class="subOrdContent">
               <div class="actionButtons sd-tour" data-step="1" data-tour-content="Replace/Return/Cancel, and other actions">
                  <a class=" disabledActionBtn  " target="_self" href="" subordcode="" data-ordercode="7990058275" subordstatus="" encode="" actioncode="RETURN_REPLACE" supc="" subordcat="" price="" paidamount="" name="" email="" mobile="" address1="" address2="" state="" city="" pincode="" data-caseid="" tpcode="" withintrustpay="false" pre-action-message="">
                  <input type="hidden" value="7-Day Easy Returns Policy period has ended. You cannot return / replace your product now." class="disabledMessageStore">
                  <span class="statusButtons btn btn-theme-secondary btn-line"> Return / Replace</span>
                  </a>
                  <span class="primaryActionBttns actionBttnPosition ">
                     <!-- BSC2.0 change starts-->
                     <a class=" downloadInvce  " encode="fd04d01f02650bcdeb8729aac8786dc9" target="_self" href="javascript:void(0);" subordcode="10257442911" data-ordercode="7990058275" subordstatus="DEL" actioncode="GET_INVOICE" subordcat="Mobiles &amp; Tablets" supc="SDL237853376" price="4999" paidamount="5099" name="" email="" mobile="" address1="" address2="" state="" city="" pincode="" data-caseid="" tpcode="" ordercode="7990058275" bsc-ticketid="" bsc-sendmessageenabled="false" withintrustpay="false" pre-action-message="">
                     <span class="statusButtons btn btn-theme-secondary btn-line"> Get Invoice</span>
                     </a>
                  </span>
                  <li class="downloadMessageContainer hidden">
                     <span class="downloadErrMsg"> Sorry, An error occured while downloading your invoice. </span>
                  </li>
                  <input type="hidden" value="" class="userMobileNumber exchangeReceipt">
               </div>
               <div class="disableCancelMsg max hidden"></div>
               <a href="/helpcenter?subOrderId=10257442911&amp;orderId=7990058275&amp;pageNumber=1" class="needMoreHelp clear"><span class="sd-icon sd-icon-nhelpfilled"></span>Need help?</a>
            </span>
         </span>
         <span class="rateReviewSubOrd">
            <div>
               <span class="writeReviewImg"></span>
               <a class="ReviewText" href="/review/create?productId=634407752835&amp;suborderCode=10257442911&amp;source=myorders">Write a Review!</a>
            </div>
            <div class="ratingStarsBigBg" style="padding: 2px 17px;">
               <div rating="5" url="/review/create?productId=634407752835&amp;suborderCode=10257442911&amp;source=myorders" id="rateTheProduct1" class="reviewNow ratingStarsBig margnCenter" style="width:70px; background-position:0 -179px;">
                  <a href="/review/create?productId=634407752835&amp;suborderCode=10257442911&amp;source=myorders&amp;rating=1" class="rStar ratingStars" pos="-34" id="ratingStarBig-1"></a>
                  <a href="/review/create?productId=634407752835&amp;suborderCode=10257442911&amp;source=myorders&amp;rating=2" class="rStar ratingStars" pos="-70" id="ratingStarBig-2"></a>
                  <a href="/review/create?productId=634407752835&amp;suborderCode=10257442911&amp;source=myorders&amp;rating=3" class="rStar ratingStars" pos="-107" id="ratingStarBig-3"></a>
                  <a href="/review/create?productId=634407752835&amp;suborderCode=10257442911&amp;source=myorders&amp;rating=4" class="rStar ratingStars" pos="-143" id="ratingStarBig-4"></a>
                  <a href="/review/create?productId=634407752835&amp;suborderCode=10257442911&amp;source=myorders&amp;rating=5" class="rStar ratingStars" pos="-179" id="ratingStarBig-5"></a>
               </div>
            </div>
         </span>
         <div data-step="3" data-tour-content="Check latest status and important dates" class=" sd-tour trackingDetails   bottomBorderTrack">
            <span class="subOrdStatus">Status: <span class="subOrdStatusText">Delivered</span></span>
            <span class="expectDel ">
            Delivered On: <span class="subOrdStatusText  "> 21 Aug, 2015</span>
            </span>
         </div>
         <div class="subOrdTimeLine">
            <span class="statusLine statusLine2 greenStatusLine  hidden"></span>
            <span class="statusCircle greenStatusCircle circleVal2"></span>
            <span class="statusLine statusLine2 greenStatusLine "></span>
            <span class="statusCircle greenStatusCircle circleVal2"></span>
            <span class="statusLine statusLine2 greenStatusLine "></span>
            <span class="statusCircle greenStatusCircle circleVal2"></span>
         </div>
         <div class="timeLineText">
            <span class="timelineStatusText  timelineText3">PLACED</span>
            <span class="timelineStatusText  timelineText3">DISPATCHED</span>
            <span class="timelineStatusText  timelineText3">DELIVERED</span>
         </div>
         <div class="trackHistoryDetails">
            <div class="realtrackTimeline">
               <div class="educativeInfo ">
                  <div class="  plusInfo  expandCollapse">
                     <div class="educativeInfoId">
                        <i class="sd-icon sd-icon-plus  blackLabel"></i>
                        <i class="sd-icon sd-icon-minus hidden blackLabel"></i>
                        <span class="educativeStageId blackLabel  ">DELIVERED</span>
                        <input type="hidden" name="stageId" class="educativeStageId" value="DELIVERED">
                     </div>
                     <ul class="educativeSteps fnt12 hidden">
                        <li class="educativeDetailedSteps educativeStep">
                           You can get your invoice from 'My Orders'.
                        </li>
                        <li class="educativeDetailedSteps educativeStep">
                           For any queries with the order, use 'Need More Help' in My Orders.
                        </li>
                        <li class="educativeDetailedSteps educativeStep">
                           In case you wish to Return/Replace, please keep all original packing, labels, freebies and accessories intact.
                        </li>
                     </ul>
                  </div>
                  <ul class="stageSteps  noBullets">
                     <li class="educativeDetailedSteps">Your item has been successfully delivered!<span class="educativeDates">04:29 PM, 21 Aug, 2015</span>
                     </li>
                  </ul>
               </div>
               <div class="educativeInfo ">
                  <div class="  plusInfo  expandCollapse">
                     <div class="educativeInfoId">
                        <i class="sd-icon sd-icon-plus  blackLabel"></i>
                        <i class="sd-icon sd-icon-minus hidden blackLabel"></i>
                        <span class="educativeStageId blackLabel  ">DISPATCHED</span>
                        <input type="hidden" name="stageId" class="educativeStageId" value="DISPATCHED">
                     </div>
                     <ul class="educativeSteps fnt12 hidden">
                        <li class="educativeDetailedSteps educativeStep">
                           Please check 'My Orders' for latest updates.
                        </li>
                     </ul>
                  </div>
                  <ul class="stageSteps  noBullets">
                     <li class="educativeDetailedSteps">Courier is transporting your item to a hub near you<span class="educativeDates">05:02 PM, 20 Aug, 2015</span>
                     </li>
                     <li class="educativeDetailedSteps">Item has been given to the courier<span class="educativeDates">10:43 PM, 19 Aug, 2015</span>
                     </li>
                  </ul>
               </div>
               <div class="educativeInfo ">
                  <div class="  plusInfo  expandCollapse">
                     <div class="educativeInfoId">
                        <i class="sd-icon sd-icon-plus  blackLabel"></i>
                        <i class="sd-icon sd-icon-minus hidden blackLabel"></i>
                        <span class="educativeStageId blackLabel  ">PLACED</span>
                        <input type="hidden" name="stageId" class="educativeStageId" value="PLACED">
                     </div>
                     <ul class="educativeSteps fnt12 hidden">
                        <li class="educativeDetailedSteps educativeStep">
                           You will receive an SMS once your order is verified.
                        </li>
                     </ul>
                  </div>
                  <ul class="stageSteps  noBullets">
                     <li class="educativeDetailedSteps">Item is packed and will be dispatched soon<span class="educativeDates">01:02 PM, 19 Aug, 2015</span>
                     </li>
                     <li class="educativeDetailedSteps">Order is confirmed and being readied for dispatch<span class="educativeDates">12:08 PM, 19 Aug, 2015</span>
                     </li>
                     <li class="educativeDetailedSteps">Order needs to be verified. Please check email and SMS<span class="educativeDates">12:07 PM, 19 Aug, 2015</span>
                     </li>
                     <li class="educativeDetailedSteps">Order processing has been initiated<span class="educativeDates">12:07 PM, 19 Aug, 2015</span>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="treak-btn js-real-track-timeline btn btn-theme-secondary btn-line">TRACK</div>
         <input type="hidden" id="orderId" value="7990058275">
         <input type="hidden" id="subOrderId" value="10257442911">
         <input type="hidden" id="preState" value="Gujarat">
         <input type="hidden" id="preZipcode" value="360004">
         <input type="hidden" id="preCity" value="Rajkot">
         <div data-step="5" data-tour-content="Additional useful information" class="promotionalBtn ">
         </div>
         <div>
         </div>
         <div>
         </div>
      </div>
   </div>
   <div class="s-modal-cont hidden" id="s-modal-cont-10257442911">
      <div class="slot-cont">
         <input type="hidden" value="10257442911" id="subOdrCode">
         <div class="s-head">Select Delivery Slot</div>
         <div class="sub-head">Estimated Delivery Date <span class="date-val"></span></div>
         <div class="err-msg hidden">
            Sorry! The delivery slots are not available currently due to technical issues. Please try again later.
         </div>
         <div class="success-msg hidden">
            <span class="succ-symbl"><i class="sd-icon sd-icon-checkmark"></i></span> Your preferred delivery slot request has been successfully placed.
         </div>
         <div class="date-cont clearfix aligncenter">
         </div>
      </div>
   </div>
</div>
    <!--main-->
      <script>
$(document).ready(function(){
    $(".has-menu").click(function(){
        $(this).toggleClass('has-menu');
    });
});
</script>