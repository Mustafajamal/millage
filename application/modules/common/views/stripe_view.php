<?php
/**
 * Stripe - Payment Gateway integration example (Stripe Checkout)
 * ==============================================================================
 *
 * @version v1.0: stripe_pay_checkout_demo.php 2016/10/05
 * @copyright Copyright (c) 2016, http://www.ilovephp.net
 * @author Sagar Deshmukh <sagarsdeshmukh91@gmail.com>
 * You are free to use, distribute, and modify this software
 * ==============================================================================
 *
 */

// Stripe library

$params = array(
	"testmode"   => "on",
	"private_live_key" => "sk_test_oyz1Q7jpobOz0yhnPtSIfx1w",
	"public_live_key"  => "pk_test_KXCLYceBgnJKQysFVHHjGmr0",
	"private_test_key" => "sk_test_gYo684GbBN0GNdI5EkicTmb7",
	"public_test_key"  => "pk_test_j1nIalKisYAoMURkBQA6MbhC"
);

if ($params['testmode'] == "on") {
	Stripe::setApiKey($params['private_test_key']);
	$pubkey = $params['public_test_key'];
} else {
	Stripe::setApiKey($params['private_live_key']);
	$pubkey = $params['public_live_key'];
}

if(isset($_POST['stripeToken']))
{
	$amount_cents = str_replace(".","","10.52");  // Chargeble amount
	$invoiceid = "14526321";                      // Invoice ID
	$description = "Invoice #" . $invoiceid . " - " . $invoiceid;

	try {
		$charge = Stripe_Charge::create(array(
			  "amount" => $amount_cents,
			  "currency" => "usd",
			  "source" => $_POST['stripeToken'],
			  "description" => $description)
		);

		if ($charge->card->address_zip_check == "fail") {
			throw new Exception("zip_check_invalid");
		} else if ($charge->card->address_line1_check == "fail") {
			throw new Exception("address_check_invalid");
		} else if ($charge->card->cvc_check == "fail") {
			throw new Exception("cvc_check_invalid");
		}
		// Payment has succeeded, no exceptions were thrown or otherwise caught

		$result = "success";

	} catch(Stripe_CardError $e) {

	$error = $e->getMessage();
		$result = "declined";

	} catch (Stripe_InvalidRequestError $e) {
		$result = "declined";
	} catch (Stripe_AuthenticationError $e) {
		$result = "declined";
	} catch (Stripe_ApiConnectionError $e) {
		$result = "declined";
	} catch (Stripe_Error $e) {
		$result = "declined";
	} catch (Exception $e) {

		if ($e->getMessage() == "zip_check_invalid") {
			$result = "declined";
		} else if ($e->getMessage() == "address_check_invalid") {
			$result = "declined";
		} else if ($e->getMessage() == "cvc_check_invalid") {
			$result = "declined";
		} else {
			$result = "declined";
		}
	}

	echo "<BR>Stripe Payment Status : ".$result;

	echo "<BR>Stripe Response : ";
	echo "<pre>";
	print_r($charge); exit;

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Stripe Checkout <?php echo get_site_info('site_title') ?></title>
</head>
<body>
<h1 class="bt_title" align="center">Stripe Checkout <?php echo get_site_info('site_title') ?></h1>
<div align="center">
  <form action="" method="POST" id="myForm">
  	<input type="hidden" name="custom" value="<?php echo $custom ?>">
  	<!-- <script src="https://checkout.stripe.com/checkout.js"></script> -->
  	<!-- <button id="customButton">Purchase</button> -->
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="<?php echo $params['public_test_key']; ?>"
    data-amount="<?php echo $amount*100 ?>"
    data-name="<?php echo get_site_info('site_title') ?>"
    data-currency="USD"
    data-description=""
    data-label="Pay Now"
    data-image="<?php echo UPLOAD_URL; ?>/site/<?php echo get_site_info('site_logo') ?>"
    data-locale="auto"
    data-zip-code="true"
    
    >
</script>
</form>
</div>
</body>
</html>
