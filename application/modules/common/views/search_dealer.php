
<main class="main primary-padding">
<div class="container">
	<h2><u>Search <?php echo ucwords($this->uri->segment(3));?></u></h2>
	<div class="row">
	<!-- <form action="<?php echo base_url($this->session->group_level."/search_dealer");?>">
	   -->
	   <div class="col-md-6 col-sm-6 col-xs-12">
	   	<select name='country_name' id="country_name" id='country' class='form-control input-md' required='required'>
	    	<?php echo $country_array;?>
	  	</select>
	  </div>
	  <button type="button" id="search_dealer" class="btn btn-primary">Search <?php echo ucwords($this->uri->segment(3));?></button>
	<!-- </form> -->
	</div>

<hr style="margin-top: 10px;">
	<?php
	if(isset($dealer_data))
	{
		foreach ($dealer_data as $v) 
		{
	?> 
		<div class="sidebar shop-sidebar ">
	        <div class="box mb-30">
	        	<div class="row">
	        		<div class="col-md-6 col-sm-6">
			            <h5 class="sec-title">Contact Person</h5>
			            <ul class="shop-sidebar clearfix">
					        <li>
					            <a>Name : <?php echo ucwords($v->customer_name);?> <span class="no"></span></a>
					        </li>
					        <li>
					            <a>Email : <?php echo $v->email;?><span class="no"></span></a>
					        </li>
					        <li>
					            <a>Phone : <?php echo $v->phone;?><span class="no"></span></a>
					        </li>
					    </ul>
					</div>
					<div class="col-md-6 col-sm-6">
			            <h5 class="sec-title">Address</h5>
			            <ul class="shop-sidebar clearfix">
					        <li>
					            <a>Address : <?php echo $v->customer_address;?> <span class="no"></span></a>
					        </li>
					        <li>
					            <a>City : <?php echo ucwords($v->customer_city);?><span class="no"></span></a>
					        </li>
					        <li>
					            <a>State : <?php echo ucwords($v->customer_state);?><span class="no"></span></a>
					        </li>
					        <li>
					            <a>Zip Code : <?php echo $v->custoemer_zip_code;?><span class="no"></span></a>
					        </li>
					    </ul>
					</div>
				</div>
	        </div>
	    </div>
		<hr>
	<?php
		}
	?>
	<?php
	}
	?>
	

</div>
<div class="secondary-padding"></div>


<script type="text/javascript">
	$("#search_dealer").click(function()
	{
		window.location="<?php echo base_url($this->session->group_level."/search_dealer/".$this->uri->segment(3)."/");?>"+$("#country_name").val();
	})
</script>