<style type="text/css">
   .select2-dropdown{
      z-index: 1049 !important;
   }
</style>
<?php
   if($this->session->userdata("group_level")=="dealer")
   {
      $price=$watch_detail->dealer_price;
   }
   else if($this->session->userdata("group_level")=="retailer")
   {
      $price=$watch_detail->distributor_price;
   }
   else
   {
      $price=$watch_detail->retail_price;
   }
?>
<main class="main primary-padding single-pg p-pb">
   <section class="product-single">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="product">
                  <div class="images">
                     <ul class="thumb-slider">
                        <li> <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_detail->dial_color.'/'.$watch_detail->main_image; ?>" alt="image"> </li>
                        <?php
                           $other_img_arr=explode(',', $watch_detail->other_image);
                           $other_img_arr=array_diff($other_img_arr,array(""));
                           for ($i=0; $i <count($other_img_arr) ; $i++) {  ?>
                        <li> <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_detail->dial_color.'/'.$other_img_arr[$i]; ?>" alt="image"> </li>
                        <?php }
                           ?>
                     </ul>
                     <div id="thumb-pager"> 
                        <a data-slide-index="0" href="#"> 
                        <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_detail->dial_color.'/'.$watch_detail->main_image; ?>" alt="thumb"> </a> 
                        <?php for ($i=0; $i <count($other_img_arr) ; $i++) {  ?>
                        <a data-slide-index="<?php echo $i+1 ?>" href="#"> 
                        <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_detail->dial_color.'/'.$other_img_arr[$i]; ?>" alt="thumb"> </a> 
                        <?php } ?>
                     </div>
                  </div>
                  <!--images-->
                  <div class="summary entry-summary">
                     <h4 class="product_title entry-title mb-10"> <?php echo $series_data.' '.$product_data->product_name.' '.$watch_detail->dial_color ?></h4>
                     <h6>Style : <?php echo ucwords($style_data); ?></h6>
                     <h6>Color : <?php echo ucwords($watch_detail->dial_color);?></h6>
                     <div class="product-info-stock-sku">
                        <!-- <div class="availability only" title="Only 979 left"> Only <strong>979</strong> left </div> -->
                        <div class="product attribute sku"> <strong class="type"></strong> </div>
                     </div>
                     <div class="price "> <ins>

                   <?php
                       if ($price>0) {
                           echo "$ ".$price;
                       }
                       $quantity=0;
                       if($watch_detail->dial_stock>0)
                       {
                           $quantity=$watch_detail->dial_stock;
                       }
                   ?>
                        <?php //echo $watch_detail->retail_price; ?>
                           

                        </ins> </div>
                     <!--price-->
                     <!-- <div class="woocommerce-product-rating mb-10"> <span class="star-rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span> </div> -->
                     <!--star rating-->
                     <!-- <div class="stock-info mb-10"> <span> <i class="fa fa-check-square"></i> Instock </span> </div> -->
                     <!-- <div class="item-desc mb-20">
                        <p> Our elegant products are only of premium quality and we hope that you`ll appreciate all best features of our goods. </p>
                        </div> -->
                     <!--item desc-->
                     <?php if (isset($this->session->user_id)) { ?>
                     <form class="validate">
                     <?php } ?>
                        <!--variation-->
                        <?php if ($price>0 && $quantity): ?>
                        <div class="single_variation_wrap">
                           <div class="variations_button">
                              <div class="box-tocart">
                                 <div class="fieldset">
                                    <input type="hidden" name="watch_option_id" value="<?php echo $watch_detail->watch_option_id ?>">
                                    <div class="form-group field qty">
                                      <label class="control-label" for="quantity">Quantity : </label>
                                      <div class="error_block">
                                          <select class="" name="quantity" id="quantity" required="required">
                                              <?php 
                                             //  if($watch_detail->dial_stock<=5)
                                             // {
                                                for($p=1;$p<=$quantity;$p++)
                                                {
                                                   echo "<option value='".$p."'>".$p."</option>";
                                                }
                                             // }
                                             // else
                                             // { 
                                             //    for($i=1;$i<=5;$i++)
                                             //    {
                                             //       echo "<option value='".$i."'>".$i."</option>";
                                             //    }
                                             //  }
                                                ?>
                                             
                                          </select>
                                          <!-- <input name="quantity" value="1" id="quantity" class="input-text qty" required="" type="number" min=1 max="<?php echo $watch_detail->dial_stock; ?>"> -->
                                      </div>
                                    </div>
                                    <div class="actions">
                                       <?php if (isset($this->session->user_id)) { ?>
                                       <button type="button" title="Add to Cart" class="action primary tocart btn_cart" id="product-addtocart-button">
                                       <span>Add to Cart</span>
                                       </button>
                                       <?php }else{ ?>
                                       <button type="button" title="Add to Cart" class="action primary tocart btn_cart trigger-modal" data-show="#login" id="product-addtocart-button">
                                       <span>Add to Cart</span>
                                       </button>
                                       <?php } ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php else: 
                           echo anchor(base_url($this->session->group_level.'/contactus'), 'Call or E-mail',array("class"=>"btn btn-danger btn-block btn-lg"));
                        endif; ?>
                        <!--single variation-->
                        <!-- <ul class="social-icons">
                           <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                           <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                           <li> <a href="#"> <i class="fa fa-linkedin"></i> </a> </li>
                           <li> <a href="#"> <i class="fa fa-instagram "></i> </a> </li>
                        </ul> -->
                     <?php if (isset($this->session->user_id)) { ?>
                     </form>
                     <?php } ?>
                     <!--form-->
                  </div>
                  <!--summery-->
                  <div class="woocommerce-tabs wc-tabs-wrapper hide-mobile">
                     <ul class="tabs wc-tabs nav nav-tabs" role="tablist">
                        <!-- <li class="nav-item active"> <a class="nav-link" href="#add-info" data-toggle="tab">Information</a> </li> -->
                        

                        <li class="nav-item active"> <a class="nav-link active" href="#features" data-toggle="tab">Features</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="#technical_data" data-toggle="tab">Technical Data</a> </li>
                        <?php 
                        if($product_data->product_catalogue!=null)
                        {
                        ?>
                        <li class="nav-item"> <a class="nav-link" href="<?php echo UPLOAD_URL."products/".$product_data->product_number."/".$product_data->product_catalogue;?>">Catalouge</a> </li>
                        <?php
                        }
                        ?>
                        
                           <!-- <li class="nav-item"> <a class="nav-link" href="#review" data-toggle="tab">Reviews (0) </a> </li> -->
                     </ul>
                     <div class="tab-content">
                        <!-- <div class="tab-pane active panel" id="description">
                        </div> -->
                        <!--description-->

                        <div class="tab-pane active panel" id="features">
                           <h4>Features</h4>
                           <table class="table table-striped">
                              <tbody>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('bracelet'); ?></th>
                                    <td colspan="4"><?php echo $product_data->bracelet; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('bracelet length'); ?></th>
                                    <td colspan="4"><?php echo $product_data->bracelet_length; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('bracelet width'); ?></th>
                                    <td colspan="4"><?php echo $product_data->bracelet_width; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('clasp type'); ?></th>
                                    <td colspan="4"><?php echo $product_data->clasp_type; ?></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>

                        <div class="tab-pane panel" id="technical_data">
                           <h4>Technical Data </h4>
                           <table class="table table-striped">
                              <tbody>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('movement'); ?></th>
                                    <td colspan="4"><?php echo $product_data->movement; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('crystal'); ?></th>
                                    <td colspan="4"><?php echo $product_data->crystal; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('case'); ?></th>
                                    <td colspan="4"><?php echo $product_data->case; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('case diameter'); ?></th>
                                    <td colspan="4"><?php echo $product_data->case_diameter; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('case thickness'); ?></th>
                                    <td colspan="4"><?php echo $product_data->case_thickness; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('bezel material'); ?></th>
                                    <td colspan="4"><?php echo $product_data->bezel_material; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('bezel function'); ?></th>
                                    <td colspan="4"><?php echo $product_data->bezel_function; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('water resistant'); ?></th>
                                    <td colspan="4"><?php echo $product_data->water_resistant; ?></td>
                                 </tr>
                                 <tr>
                                    <th scope="row"><?php echo ucwords('calendar'); ?></th>
                                    <td colspan="4"><?php echo $product_data->calendar; ?></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>

                        <!--additional information-->
                        <div class="tab-pane panel" id="review">
                           <h4>Reviews (0) </h4>
                        </div>
                        <!--reviews--> 
                     </div>
                  </div>
                  <!--only sho win desktop and tablet-->
                  <div id="accordion" class=" product-desc show-mobile">
                     <!-- <h3>Product description</h3>
                        <div>
                          <p> Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                            ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                            amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                            odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate. </p>
                        </div>
                        <h3>Additional Information</h3>
                        <div>
                          <p> Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                            purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                            velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                            suscipit faucibus urna. </p>
                        </div>
                        <h3>Reviews(0)</h3>
                        <div>
                          <p> Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                            Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                            ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                            lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui. </p>
                        </div> -->
                     <h3>Information</h3>
                     <div>
                        <table class="table table-striped">
                           <tbody>
                              <tr>
                                 <th scope="row"><?php echo ucwords('movement'); ?></th>
                                 <td colspan="4"><?php echo $product_data->movement; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('crystal'); ?></th>
                                 <td colspan="4"><?php echo $product_data->crystal; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('case'); ?></th>
                                 <td colspan="4"><?php echo $product_data->case; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('case diameter'); ?></th>
                                 <td colspan="4"><?php echo $product_data->case_diameter; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('case thickness'); ?></th>
                                 <td colspan="4"><?php echo $product_data->case_thickness; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('bezel material'); ?></th>
                                 <td colspan="4"><?php echo $product_data->bezel_material; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('bezel function'); ?></th>
                                 <td colspan="4"><?php echo $product_data->bezel_function; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('water resistant'); ?></th>
                                 <td colspan="4"><?php echo $product_data->water_resistant; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('calendar'); ?></th>
                                 <td colspan="4"><?php echo $product_data->calendar; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('bracelet'); ?></th>
                                 <td colspan="4"><?php echo $product_data->bracelet; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('bracelet length'); ?></th>
                                 <td colspan="4"><?php echo $product_data->bracelet_length; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('bracelet width'); ?></th>
                                 <td colspan="4"><?php echo $product_data->bracelet_width; ?></td>
                              <tr>
                              <tr>
                                 <th scope="row"><?php echo ucwords('clasp type'); ?></th>
                                 <td colspan="4"><?php echo $product_data->clasp_type; ?></td>
                              <tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!--show mobile only--> 
               </div>
               <!--product--> 
            </div>
            <!--top-->
         </div>
      </div>
   </section>




 <!--main banner-->
    <main class="main secondary-padding">
        <div class="product-list">
            <div class="container">

<h2><u>Related Products</u></h2>
                <!-- <h2 class="text-center"><?php echo $category_name.$style_name; ?></h2> -->
                <div class="row">                
                    <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                        <!-- <div class="storefront-sorting"> -->
                            <!-- <form class="woocommerce-ordering" method="get">
                                <span class="">Sort:</span>
                                <select name="orderby" class="orderby">
                                    <option value="menu_order" selected="selected">Default sorting</option>
                                    <option value="date">Sort by newness</option>
                                    <option value="price">Sort by price: low to high</option>
                                    <option value="price-desc">Sort by price: high to low</option>
                                </select> 
                                <span class="glyphicon glyphicon-chevron-down select-dropdown"></span>
                            </form> -->
                            <!--form-->
                            <!--short by-->
                           <!--  <div class="woocommerce-result-count">
                               Items 1-9 of 10
                            </div> -->
                            <!--result-->
                            <!-- <nav class="woocommerce-pagination">
                                <ul class="page-numbers">
                                    <li><a class="prev page-numbers" href="#">&lt;</a></li>
                                    <li><span class="page-numbers current">1</span></li>
                                    <li><a class="page-numbers" href="#">2</a></li>
                                    <li><a class="page-numbers" href="#">3</a></li>
                                    <li><a class="next page-numbers" href="#">&gt;</a></li>
                                </ul>
                            </nav> -->
                             <!-- <div class="short-by pull-right">
                                <a href="07-rab-shop-left-sidebar-list.html" class="btn btn-default bdr">
                                    <i class="fa fa-list-ul"></i>
                                </a>
                                <a href="05-rab-shop-left-sidebar-grid.html" class="btn btn-default bdr active">
                                    <i class="fa fa-th-large"></i>
                                </a>
                            </div> -->
                        <!-- </div> -->
                        <!--storefront-sorting-->
                            <?php //d($all_products_options);
                            // exit();
                             ?>
                        <ul class="products clearfix">
                            <?php
                                $i=0;
                                foreach ($all_products_options as $value) {
                                    foreach ($value as $watch_one) {
                                        $i++;
                                        if ($i%4==0) {
                                          $class="last";
                                        }else{
                                           $class=""; 
                                        }
                                        $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_one->watch_id));
                                        $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
                                        $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
                                        $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
                                        if($this->session->userdata("group_level")=="dealer")
                                        {
                                           $price=$watch_one->dealer_price;
                                        }
                                        else if($this->session->userdata("group_level")=="retailer")
                                        {
                                           $price=$watch_one->distributor_price;
                                        }
                                        else
                                        {
                                           $price=$watch_one->retail_price;
                                        }
                                    ?>
                                    <li class="product <?php echo $class; ?>">
                                        <figure>
                                            <div class="icons">
                                                 <?php if (isset($this->session->user_id) && $price>0 && $watch_one->dial_stock>0) { ?>
                                                <a href="#" class="btn add_cart " id="<?php echo $watch_one->watch_option_id; ?>"><i class="pe-7s-cart"></i></a>
                                                <?php } ?>
                                                <!-- <a href="#" class="btn"><i class="pe-7s-like"></i></a> -->
                                            </div>
                                            <!--icons-->
                                            <div class="product-wrap base-align"> <a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$watch_one->watch_option_id) ?>">&nbsp;</a> <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_one->dial_color.'/'.$watch_one->main_image; ?>" alt="Product"  style="height: 260px;"> </div>
                                        </figure>
                                        <!--figure-->
                                        <div class="content">
                                           <h6><a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$watch_one->watch_option_id) ?>"><?php echo $series_data.' '.$watch_one->dial_color ?></a></h6>
                                            <div class="bottom">
                                                <div class="price"> <ins>

                                                    <?php
                                                        if ($price>0) {
                                                            echo "$ ".$price;
                                                        }
                                                    ?>
                                                        
                                                

                                                    </ins> </div>
                                                
                                                <!--price-->
                                                <!-- <span class="star-rating">
                                                   <i class="fa fa-star"></i> 
                                                   <i class="fa fa-star"></i> 
                                                   <i class="fa fa-star"></i> 
                                                   <i class="fa fa-star"></i> 
                                                   <i class="fa fa-star-half-empty "></i> 
                                                </span> -->
                                                <!--star-->
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                    }
                                } ?>
                            <!--single product-->
                        </ul>
                        <!--product list-->
                        <!-- <nav class="woocommerce-pagination text-center">
                            <ul class="page-numbers">
                                <li><a class="prev page-numbers" href="#">&lt;</a></li>
                                <li><span class="page-numbers current">1</span></li>
                                <li><a class="page-numbers" href="#">2</a></li>
                                <li><a class="page-numbers" href="#">3</a></li>
                                <li><a class="next page-numbers" href="#">&gt;</a></li>
                            </ul>
                        </nav> -->
                        <!--pegination-->
                    </div>
                    <!--left-->
                    <!--right-->
                </div>
            </div>
        </div>
    
    <!--main-->
