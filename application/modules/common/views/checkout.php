
<main class="main primary-padding">
    <?php if (!empty($cart_item)) { ?>
    <section class="checkout-steps">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="wrap first active">
                        <span class="circle rounded-crcl"> 01 </span>
                        <h6>Check Out</h6>
                    </div>
                    <!--steps-->
                    <div class="wrap second full">
                        <span class="circle rounded-crcl"> 02 </span>
                        <h6>Payment</h6>
                    </div>
                    <!--steps-->
                    <div class="wrap final full">
                        <span class="circle rounded-crcl"> 03 </span>
                        <h6>Order Complete</h6>
                    </div>
                    <!--steps-->
                </div>
            </div>
        </div>
    </section>
    <!--check out steps-->
    <section class="cart-block p-pb">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="woocommerce">
                        <form name="checkout" id="checkout_form" method="post" class="checkout woocommerce-checkout validate" action="<?php echo base_url($this->session->group_level."/payment"); ?>">
                            <div class="col2-set" id="customer_details">
                                <div class="col-1">
                                    <div class="woocommerce-billing-fields">
                                        <h3>Billing Details</h3>
                                       <?php 
                                       // d($cust_data);
                                        $customer_country_frm=array("name"=>"customer_country","id"=>"customer_country","required"=>"","class"=>"form-control","placeholder"=>"customer country");
                                        if (!is_null($cust_data)) {
                                            echo form_dropdown($customer_country_frm,$countries, array($cust_data->customer_country),"customer country");
                                        }else{
                                            echo form_dropdown($customer_country_frm,$countries, array(230),"customer country");
                                        }
                                        $customer_name_frm=array("name"=>"customer_name","id"=>"customer_name","required"=>"","class"=>"form-control","placeholder"=>"customer name");
                                        echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data->customer_name:"","Customer Name");
                                        $customer_address_frm=array("name"=>"customer_address","id"=>"customer_address","required"=>"","class"=>"form-control","placeholder"=>"customer address");
                                        echo form_textarea($customer_address_frm, (!is_null($cust_data))?$cust_data->customer_address:"","Address");
                                       ?>

                                        <div class="form-row form-row form-row-first">
                                           <!--  <label>Town / City
                                                <abbr class="required">*</abbr>
                                            </label>
                                            <input type="text" class="form-control" placeholder="Town / City"> -->
                                            <?php
                                                $customer_city_frm=array("name"=>"customer_city","id"=>"customer_city","required"=>"","class"=>"form-control","placeholder"=>"Town / City");
                                                echo form_input($customer_city_frm, (!is_null($cust_data))?$cust_data->customer_city:"","Town / City");
                                            ?>
                                        </div>
                                        <!--town-->

                                        <div class="form-row form-row form-row-last">
                                            <!-- <label>State / Proviance
                                                <abbr class="required">*</abbr>
                                            </label>
                                            <input type="text" class="form-control" placeholder="State / Proviance"> -->
                                            <?php 
                                                $customer_state_frm=array("name"=>"customer_state","id"=>"customer_state","required"=>"","class"=>"form-control","placeholder"=>"State / Proviance");
                                                echo form_input($customer_state_frm, (!is_null($cust_data))?$cust_data->customer_state:"","State / Proviance");
                                            ?>
                                        </div>
                                        <!--city-->

                                        <div class="clear"></div>
                                        <div class="form-row form-row form-row-first">
                                            <!-- <label>TPostcode / Zip
                                                <abbr class="required">*</abbr>
                                            </label>
                                            <input type="text" class="form-control" placeholder="Postcode / Zip"> -->
                                            <?php 
                                                $custoemer_zip_code_frm=array("name"=>"custoemer_zip_code","id"=>"custoemer_zip_code","required"=>"","class"=>"form-control","placeholder"=>"Postcode / Zip");
                                                echo form_input($custoemer_zip_code_frm, (!is_null($cust_data))?$cust_data->custoemer_zip_code:"","Postcode / Zip");
                                            ?>
                                        </div>                                            
                                        <!--zip-->

                                        <div class="form-row form-row form-row-last">
                                            <!-- <label>Phone Number
                                                <abbr class="required">*</abbr>
                                            </label>
                                            <input type="text" class="form-control" placeholder="Phone Number"> -->
                                            <?php 
                                                $customer_name_frm=array("name"=>"phone","id"=>"phone","required"=>"","class"=>"form-control","placeholder"=>"Phone Number");
                                                echo form_input($customer_name_frm, $ion_data->phone,"Phone Number");
                                            ?>
                                        </div>
                                        <!--postanl-->

                                        <div class="clear"></div>
                                        <div class="form-row form-row form-row-wide">
                                            <?php 
                                                $customer_name_frm=array("name"=>"email","id"=>"user_email","required"=>"","class"=>"form-control","placeholder"=>"Email Address","readonly"=>"");
                                                echo form_input($customer_name_frm, $ion_data->email,"Email Address");
                                            ?>
                                        </div>
                                        <!--address name-->

                                        <!-- <div class="form-row form-row-wide create-account woocommerce-validated">
                                            <div class="checkbox-wrap">
                                                <input type="checkbox" value="1">
                                                <label class="checkbox">Create an account</label>
                                            </div>                                          
                                        </div> -->
                                    </div>
                                </div>
                                <!--col 1-->

                                <div class="col-2">
                                    <div class="woocommerce-shipping-fields">
                                        <h3 id="ship-to-different-address"> Ship to a Same address? 
        
                                            <span class="checkbox-wrap">
                                                <input type="checkbox" value="1" id="ship-to-different-address-checkbox">
                                                <label>&nbsp;</label>
                                            </span> 
                                        </h3>
                                       <?php 
                                        $customer_country_frm=array("name"=>"customer_ship_country","id"=>"customer_ship_country","required"=>"","class"=>"form-control ship_data","placeholder"=>"Receiver country");
                                        if (!is_null($cust_data)) {
                                            echo form_dropdown($customer_country_frm,$countries, array($cust_data->customer_ship_country),"Receiver country");
                                        }else{
                                            echo form_dropdown($customer_country_frm,$countries, array(230),"Receiver country");
                                        }
                                        $customer_name_frm=array("name"=>"customer_ship_name","id"=>"customer_ship_name","required"=>"","class"=>"form-control ship_data","placeholder"=>"Receiver name");
                                        echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data->customer_ship_name:"","Receiver Name");
                                        $customer_address_frm=array("name"=>"customer_ship_address","id"=>"customer_ship_address","required"=>"","class"=>"form-control ship_data","placeholder"=>"Receiver Address");
                                        echo form_textarea($customer_address_frm, (!is_null($cust_data))?$cust_data->customer_ship_address:"","Receiver Address");
                                       ?>

                                        <div class="form-row form-row form-row-first">
                                           <!--  <label>Town / City
                                                <abbr class="required">*</abbr>
                                            </label>
                                            <input type="text" class="form-control" placeholder="Town / City"> -->
                                            <?php
                                                $customer_city_frm=array("name"=>"customer_ship_city","id"=>"customer_ship_city","required"=>"","class"=>"form-control ship_data","placeholder"=>"Town / City");
                                                echo form_input($customer_city_frm, (!is_null($cust_data))?$cust_data->customer_ship_city:"","Town / City");
                                            ?>
                                        </div>
                                        <!--town-->

                                        <div class="form-row form-row form-row-last">
                                            <!-- <label>State / Proviance
                                                <abbr class="required">*</abbr>
                                            </label>
                                            <input type="text" class="form-control" placeholder="State / Proviance"> -->
                                            <?php 
                                                $customer_state_frm=array("name"=>"customer_ship_state","id"=>"customer_ship_state","required"=>"","class"=>"form-control ship_data","placeholder"=>"State / Proviance");
                                                echo form_input($customer_state_frm, (!is_null($cust_data))?$cust_data->customer_ship_state:"","State / Proviance");
                                            ?>
                                        </div>
                                        <!--city-->

                                        <div class="clear"></div>
                                        <div class="form-row form-row form-row-first">
                                            <!-- <label>TPostcode / Zip
                                                <abbr class="required">*</abbr>
                                            </label>
                                            <input type="text" class="form-control" placeholder="Postcode / Zip"> -->
                                            <?php 
                                                $custoemer_zip_code_frm=array("name"=>"custoemer_ship_zip_code","id"=>"custoemer_ship_zip_code","required"=>"","class"=>"form-control ship_data","placeholder"=>"Postcode / Zip");
                                                echo form_input($custoemer_zip_code_frm, (!is_null($cust_data))?$cust_data->custoemer_ship_zip_code:"","Postcode / Zip");
                                            ?>
                                        </div>                                            
                                        <!--zip-->

                                        <div class="form-row form-row form-row-last">
                                            <!-- <label>Phone Number
                                                <abbr class="required">*</abbr>
                                            </label>
                                            <input type="text" class="form-control" placeholder="Phone Number"> -->
                                            <?php 
                                                $customer_name_frm=array("name"=>"customer_ship_phone","id"=>"customer_ship_phone","required"=>"","class"=>"form-control ship_data","placeholder"=>"Phone Number");
                                                echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data->customer_ship_phone:"","Phone Number");
                                            ?>
                                        </div>
                                        <!--postanl-->

                                        <div class="clear"></div>
                                        <div class="form-row form-row form-row-wide">
                                            <?php 
                                                $customer_name_frm=array("name"=>"customer_ship_email","id"=>"customer_ship_email","required"=>"","class"=>"form-control ship_data","placeholder"=>"Email Address",);
                                                echo form_input($customer_name_frm, (!is_null($cust_data))?$cust_data->customer_ship_email:"","Email Address");
                                            ?>
                                        </div>
                                        <!--address name-->

                                        <!-- <div class="form-row form-row-wide create-account woocommerce-validated">
                                            <div class="checkbox-wrap">
                                                <input type="checkbox" value="1">
                                                <label class="checkbox">Create an account</label>
                                            </div>                                          
                                        </div> -->
                                    </div>
                                        <!--shipping address-->

                                        <!-- <div class="form-row form-row notes" id="order_comments_field">
                                            <label>Order Notes</label>
                                            <textarea placeholder="Order Notes" rows="2" cols="5"></textarea>
                                        </div> -->
                                         <?php 
                                                $customer_name_frm=array("name"=>"order_note","id"=>"order_note","class"=>"form-control","placeholder"=>"Order Notes");
                                                echo form_textarea($customer_name_frm,"","Order Notes");
                                            ?>
                                    </div>
                                </div>
                            </div>
                            <!--col set-->

                            <div id="order_review" class="woocommerce-checkout-review-order">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3 id="order_review_heading">Your order</h3>
                                        <table class="shop_table woocommerce-checkout-review-order-table">
                                            <thead>
                                                <tr>
                                                    <th class="product-name">Product Details</th>
                                                    <th class="product-total">Total</th>
                                                </tr>
                                            </thead>
                                            <!--head-->

                                            <tbody>
                                                <?php 
                                                $total=0;
                                                foreach ($cart_item as $cart) {
                                                    $watch_option=$this->custom->getSingleRow("watch_options_master",array('watch_option_id'=>$cart->watch_option_id));
                                                    $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_option->watch_id));
                                                    $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
                                                    $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
                                                    $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
                                                ?>
                                                <tr class="cart_item">
                                                    <td class="product-name" data-title="Product">
                                                        <?php echo $series_data; ?> &nbsp; <span class="product-quantity">× <?php echo $cart->quantity;  ?> </span> </td>
                                                    <td class="product-total" data-title="Total">
                                                        <?php 
                                                            if($this->session->userdata("group_level")=="dealer")
                                                            {
                                                              $price=$watch_option->dealer_price;
                                                            }
                                                            else if($this->session->userdata("group_level")=="retailer")
                                                            {
                                                              $price=$watch_option->distributor_price;
                                                            }
                                                            else
                                                            {
                                                                $price=$watch_option->retail_price;
                                                            }
                                                        ?>
                                                        $<?php $total+=number_format($price*$cart->quantity,2);
                                                            echo number_format($price*$cart->quantity,2);
                                                          ?> 
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                            <!--t body-->

                                            <tfoot>
                                                <tr class="cart-subtotal">
                                                    <td>Cart Sub-total</td>
                                                    <td>$<?php echo number_format($total,2); ?>
                                                    </td>
                                                </tr>
                                                <tr class="shipping">
                                                    <td>Shipping + Handling</td>
                                                    <td data-title="Shipping">
                                                        $00.00
                                                    </td>
                                                </tr>
                                                <tr class="order-total">
                                                    <th>Order Totals</th>
                                                    <th>
                                                        $<?php echo number_format($total,2); 
                                                    ?></th>
                                                </tr>
                                            </tfoot>
                                            <!--t foot-->
                                        </table>
                                    </div>
                                    <!--order detail-->

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Payment Methods</h3>

                                        <div id="payment" class="woocommerce-checkout-payment">
                                            <ul class="wc_payment_methods payment_methods methods">
                                                <li class="wc_payment_method payment_method_ppec_paypal">
                                                    <input type="radio" name="payment_method" value="stripe">
                                                    <label> Stripe <img src="<?php echo IMG_URL; ?>stripe.png" alt="" height="50px" width="50px">
                                                    </label>
                                                </li>                                               

                                                <li class="wc_payment_method payment_method_ppec_paypal">
                                                    <input type="radio" checked="" name="payment_method" value="paypal">
                                                    <label> PayPal <img src="<?php echo IMG_URL; ?>paypal.jpg" alt="">
                                                    </label>
                                                </li>
                                            </ul>

                                           <!--  <div class="form-row form-row-wide create-account woocommerce-validated">
                                                <div class="checkbox-wrap">
                                                    <input type="checkbox" value="1">
                                                    <label class="checkbox">I’ve read and accept the terms & conditions *</label>
                                                </div>  
                                            </div> -->
                                        </div>
                                    </div>
                                    <!--payment-->
                                </div>
                            </div>
                            <!--review-->
                            <?php 
                                /* some hidden field */
                                echo _form_common("hidden",array("name"=>"payable_amount"),$total);
                                echo _form_common("hidden",array("name"=>"order_id_table"),(!empty($cart_item))?$cart_item[0]->order_id:"");
                            ?>
                            <div class="text-center">
                              <input type="submit" class="btn btn-danger" value="Proceed To Payment">  
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--cart block-->
    <?php }else{ ?>
    <h1>Your Cart Is Empty</h1>
    <?php } ?>
</main>
<!--main-->
<script type="text/javascript">
    $("#ship-to-different-address-checkbox").click(function(event) {
        // console.log($(this).prop('checked', true));
        // console.log($(this).prop('checked', false));
        if(this.checked){
            $(".ship_data").attr('readonly', true);
            $("#customer_ship_country").val($("#customer_country").val());
            $("#customer_ship_name").val($("#customer_name").val());
            $("#customer_ship_address").val($("#customer_address").val());
            $("#customer_ship_city").val($("#customer_city").val());
            $("#customer_ship_state").val($("#customer_state").val());
            $("#custoemer_ship_zip_code").val($("#custoemer_zip_code").val());
            $("#customer_ship_phone").val($("#phone").val());
            $("#customer_ship_email").val($("#user_email").val());
            
            console.log($("#checkout_form").serialize());
        }else{
            $(".ship_data").attr('readonly', false);
        }
    });
</script>