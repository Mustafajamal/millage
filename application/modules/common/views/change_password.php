<div class="breadcrumbs">
    <div class="container">
        <ul class="items">
                <li class="item home">
                    <a href="<?php echo base_url() ?>" title="Go to Home Page">Home</a>
                </li>
                <li class="item category9">
                    <a href="<?php echo current_url(); ?>"><?php echo ucwords(" > MY PROFILE"); ?></a>
                </li>
                <!-- /*******************************/ -->
        </ul>
    </div>
</div>
    <!--main banner-->
    <main class="main primary-padding">
        <div class="product-list">
            <div class="container">
                <!-- <h2 class="text-center"><?php echo $category_name.$style_name; ?></h2> -->
                <div class="row">                
                    <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                        <h2>Manage Account</h2>
                        <?php 
                            echo form_open($this->session->group_level.'/change_password/save',array("class"=>"validate"));
                            $customer_name_frm=array("name"=>"identity","id"=>"identity","required"=>"","class"=>"form-control","placeholder"=>"Identity","disabled"=>"","readonly"=>"");
                            echo form_input($customer_name_frm,$this->session->identity,"Identity");
                            $customer_name_frm=array("name"=>"old_password","id"=>"old_password","required"=>"","class"=>"form-control","placeholder"=>"Old Password");
                            echo form_password($customer_name_frm,"","Old Password");
                            $customer_name_frm=array("name"=>"new_password","id"=>"new_password","required"=>"","class"=>"form-control","placeholder"=>"New Password");
                            echo form_password($customer_name_frm,"","New Password");
                            $customer_name_frm=array("name"=>"c_new_password","id"=>"c_new_password","required"=>"","class"=>"form-control","placeholder"=>"Confirm New Password","data-rule-equalTo"=>"#new_password");
                            echo form_password($customer_name_frm,"","Confirm New Password");
                            
                            ?>
                            <button type="submit" class="btn btn-success pull-right">Submit</button>
                            <?php
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--main-->
      <script>
$(document).ready(function(){
    $(".has-menu").click(function(){
        $(this).toggleClass('has-menu');
    });
});
</script>