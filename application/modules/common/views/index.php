


 <main class="main">   
<div class="content-block" id="back" style="height: 637px;">
    <div class="secondary-slogan"> </div>
    <ul class="cb-slideshow" style="list-style:none;">
        <li><span style="background-image: url(<?php echo UPLOAD_URL; ?>/site/slider-1.jpg">Image 01</span></li>
        <li><span style="background-image: url(<?php echo UPLOAD_URL; ?>/site/slider-2.jpg">Image 02</span></li>
        <li><span style="background-image: url(<?php echo UPLOAD_URL; ?>/site/slider-3.jpg">Image 03</span></li>
        <li><span style="background-image: url(<?php echo UPLOAD_URL; ?>/site/slider-4.jpg">Image 04</span></li>
        <li><span style="background-image: url(<?php echo UPLOAD_URL; ?>/site/slider-6.jpg">Image 05</span></li>
        <li><span style="background-image: url(<?php echo UPLOAD_URL; ?>/site/slider-3.jpg">Image 06</span></li>
        <div class="container">
            <section class="center">
                <div class="slogan" style="    position: relative;    color: #fff;    bottom: 0;    top: 0; margin-top:200px; ">
                    <h1 style="color:#a37e2c">MILLAGE WHATCHES</h1>
                    <div style="width:50%"> Since 1997 MILLAGE has been creating uniquely designed timepieces in the United States. Our prices remain very competitive, as we manufacture our watches ourselves in our own factories in Europe. Throughout our history, shoppers have
                        turned to MILLAGE for value, integrity and one of the world's most finest selection of timepieces. Recognized as an undisputed authority in the watch industry, MILLAGE believes that watches are more than mere instruments of time,
                        but rather reflections of an individual's lifestyle and personality.
                        <div><br> <span class="app"><a  class="btn rect bronze text"> Products </a></span> </div>
                      </div>
                </div>
            </section>
            </div>
    </ul>


    </div>
  
  <!--feature category-->
  
  <section class="popular-product p-pb">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="title-main text-uppercase text-center mb-60">
            <h2>New Products</h2>
          </div>
        </div>		</div>		<div class="">
        <!--title-->
        <?php for ($i=0; $i <3 ; $i++) {      if (isset($banner_watch[$i])) {      $product_number=$this->custom->getSingleValue("watch_master","product_number",array('watch_id'=>$banner_watch[$i]->watch_id));	        ?>             <div class="col-md-4">         <div class="watch">		   <div class="img-center">	        <a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$banner_watch[$i]->watch_option_id) ?>">              <img class="img-circle1 circular-img" src="<?php echo UPLOAD_URL.'products/'.$product_number.'/'.$banner_watch[$i]->dial_color.'/'.$banner_watch[$i]->main_image; ?>" width="100%" height="100%" ><br><br>			<div class="max_width bloc_more_news">                    <div class="line"></div>									                </div>			<div>         		 <?php echo $product_number.'-'.$banner_watch[$i]->dial_color; ?> <br>		  		  <?php echo '$'.$banner_watch[$i]-> distributor_price?>		  </div>	        </a>    </div> 	</div>	</div>    <?php }} ?>		       </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="products product-slide">
            <?php foreach ($new_watch as $new_watch_one) {
              $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$new_watch_one->watch_id));
              $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
              $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
              $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
              if($this->session->userdata("group_level")=="dealer")
               {
                  $price=$new_watch_one->dealer_price;
               }
               else if($this->session->userdata("group_level")=="retailer")
               {
                  $price=$new_watch_one->distributor_price;
               }
               else
               {
                  $price=$new_watch_one->retail_price;
               }

             ?>
              <li class="product">
                <figure>
                  <div class="icons"> 
                     <?php if (isset($this->session->user_id) && $price>0) { ?>
                    <a href="#" class="btn add_cart" id="<?php echo $new_watch_one->watch_option_id;  ?>"><i class="pe-7s-cart"></i></a>
                    <?php } ?>
                     <!-- <a href="#" class="btn"><i class="pe-7s-like"></i></a> --> </div>
                  <!--icons-->
                  
                  <div class="product-wrap base-align"> <a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$new_watch_one->watch_option_id) ?>">&nbsp;</a> <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$new_watch_one->dial_color.'/'.$new_watch_one->main_image; ?>" alt="Product"  style="height: 300px;"> </div>
                </figure>
                <!--figure-->
                
                <div class="content">
                  <h6><a href=""><?php echo $series_data.' '.$new_watch_one->dial_color ?></a></h6>
                  <a href="#"><span class="product-label">new!</span></a>
                  <div class="bottom">

                  <div class="price"> 
                  <ins>  
                     <?php
                       if ($price>0) {
                           echo "$ ".$price;
                       }
                   ?>
                    </ins> 
                  </div>
                    <!--price--> 
                    
                     
                    <!--star--> 
                  </div>
                </div>
              </li>
            <?php } ?>
            <!--single product-->
          </ul>
        </div>
      </div>
    </div>
  </section>     <section class="popular-product p-pb">    <div class="container">      <div class="row">        <div class="col-md-12 col-sm-12 col-xs-12">          <div class="title-main text-uppercase text-center mb-60">            <h2>featured watches</h2>          </div>        </div>		</div>		<div class="">        <!--title-->        <?php for ($i=0; $i <3 ; $i++) {      if (isset($banner_watch[$i])) {      $product_number=$this->custom->getSingleValue("watch_master","product_number",array('watch_id'=>$banner_watch[$i]->watch_id));	        ?>             <div class="col-md-4">         <div class="watch">		   <div class="img-center">	        <a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$banner_watch[$i]->watch_option_id) ?>">              <img class="img-circle1 circular-img" src="<?php echo UPLOAD_URL.'products/'.$product_number.'/'.$banner_watch[$i]->dial_color.'/'.$banner_watch[$i]->main_image; ?>" width="100%" height="100%" ><br><br>			<div class="max_width bloc_more_news">                    <div class="line"></div>									                </div>			<div>         		 <?php echo $product_number.'-'.$banner_watch[$i]->dial_color; ?> <br>		  		  <?php echo '$'.$banner_watch[$i]-> distributor_price?>		  </div>	        </a>    </div> 	</div>	</div>    <?php }} ?>		       </div>        <div class="col-md-12 col-sm-12 col-xs-12">          <ul class="products product-slide">            <?php foreach ($new_watch as $new_watch_one) {              $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$new_watch_one->watch_id));              $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));              $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));              $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));              if($this->session->userdata("group_level")=="dealer")               {                  $price=$new_watch_one->dealer_price;               }               else if($this->session->userdata("group_level")=="retailer")               {                  $price=$new_watch_one->distributor_price;               }               else               {                  $price=$new_watch_one->retail_price;               }             ?>              <li class="product">                <figure>                  <div class="icons">                      <?php if (isset($this->session->user_id) && $price>0) { ?>                    <a href="#" class="btn add_cart" id="<?php echo $new_watch_one->watch_option_id;  ?>"><i class="pe-7s-cart"></i></a>                    <?php } ?>                     <!-- <a href="#" class="btn"><i class="pe-7s-like"></i></a> --> </div>                  <!--icons-->                                    <div class="product-wrap base-align"> <a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$new_watch_one->watch_option_id) ?>">&nbsp;</a> <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$new_watch_one->dial_color.'/'.$new_watch_one->main_image; ?>" alt="Product"  style="height: 300px;"> </div>                </figure>                <!--figure-->                                <div class="content">                  <h6><a href=""><?php echo $series_data.' '.$new_watch_one->dial_color ?></a></h6>                  <a href="#"><span class="product-label">new!</span></a>                  <div class="bottom">                  <div class="price">                   <ins>                       <?php                       if ($price>0) {                           echo "$ ".$price;                       }                   ?>                    </ins>                   </div>                    <!--price-->                                                              <!--star-->                   </div>                </div>              </li>            <?php } ?>            <!--single product-->          </ul>        </div>      </div>    </div>  </section>  <div class="max_width bloc_more_news">                    <div class="line"></div>					<center>					<div class="nn">                    <a class="btn rect bronze text" data-format="news" data-otherpage="company/news" href="/en/company/news" dir="ltr">                        More news                    </a>					</div>					</center>                </div>				
  <!--popular product-->
  
  
  <!--promo banner-->
  
  
