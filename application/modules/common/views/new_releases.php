<section class="popular-product p-pb">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="title-main text-uppercase text-center mb-60">
            <h2>New Products</h2>
          </div>
        </div>
        <!--title-->
        
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="products product-slide">
            <?php foreach ($new_watch as $new_watch_one) {
              $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$new_watch_one->watch_id));
              $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
              $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
              $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
              if($this->session->userdata("group_level")=="dealer")
               {
                  $price=$new_watch_one->dealer_price;
               }
               else if($this->session->userdata("group_level")=="retailer")
               {
                  $price=$new_watch_one->distributor_price;
               }
               else
               {
                  $price=$new_watch_one->retail_price;
               }

             ?>
              <li class="product">
                <figure>
                  <div class="icons"> 
                     <?php if (isset($this->session->user_id) && $price>0) { ?>
                    <a href="#" class="btn add_cart" id="<?php echo $new_watch_one->watch_option_id;  ?>"><i class="pe-7s-cart"></i></a>
                    <?php } ?>
                     <!-- <a href="#" class="btn"><i class="pe-7s-like"></i></a> --> </div>
                  <!--icons-->
                  
                  <div class="product-wrap base-align"> <a href="<?php echo base_url($this->session->group_level.'/product_detail/'.$new_watch_one->watch_option_id) ?>">&nbsp;</a> <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$new_watch_one->dial_color.'/'.$new_watch_one->main_image; ?>" alt="Product"  style="height: 300px;"> </div>
                </figure>
                <!--figure-->
                
                <div class="content">
                  <h6><a href=""><?php echo $series_data.' '.$new_watch_one->dial_color ?></a></h6>
                  <a href="#"><span class="product-label">new!</span></a>
                  <div class="bottom">

                  <div class="price"> 
                  <ins>  
                     <?php
                       if ($price>0) {
                           echo "$ ".$price;
                       }
                   ?>
                    </ins> 
                  </div>
                    <!--price--> 
                    
                     
                    <!--star--> 
                  </div>
                </div>
              </li>
            <?php } ?>
            <!--single product-->
          </ul>
        </div>
      </div>
    </div>
  </section>