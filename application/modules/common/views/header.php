<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<title>Millage</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">

<!-- favicon -->
<link rel="shortcut icon" type="image/png" href="<?php echo IMG_URL; ?>fav.jpg" />
<!-- GOOGLE FONT -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i%7CMr+Dafoe%7CPoppins:300,400,500,600,700" rel="stylesheet">

<!-- CSS LIBRARY -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>bootstrap2.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font-awesome-animation.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>pe-icon-7-stroke.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>menu.css" />

<!-- Main css -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>style1.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>responsive.css" />

<!-- color skin -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>color-skin/default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>toastr.min.css" />

<!-- <link rel="stylesheet" href="<?php echo CSS_URL; ?>AdminLTE.css"> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="<?php echo CSS_URL;?>bootstrap-datepicker.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>jquery.realperson.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>layouts-common1.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>common2.css">
    <link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>inline8.css">
	<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>style3.css">
<script src="<?php echo JS_URL; ?>menu6.js"></script> 


<script src="<?php echo JS_URL; ?>jquery-3.1.1.min.js"></script> 

<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style >
    .swal2-container{
      z-index: 1051 !important;
    }  
    .swal2-modal{
      height: 100% !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
      background-color: #da1048; 
      color: #FFF;
    }
	@media (min-height: 601px) and (min-width: 1025px){
	.rlx-site-nav__openMenu{
	margin-bottom:10px;}	} 
	
  </style>
</head>

<body class="single-product">

<header class="header">               <!-- /.navbar-collapse -->     

    <div id="mySidenav" class="sidenav"><div class="rlx-nav__sub rlx-nav__sub--active"><img  src="<?php echo UPLOAD_URL; ?>/site/logo4.gif" class="" style="" alt="Logo" ><div><button style="background-color:#fff"><a href="javascript:void(0)" onclick="closeNav()">&times; Close the menu</a></button></div>   <div class="max_width bloc_more_news">
                    <div class="line"></div>
					<center>
					<div class="nn">
                    
					</div>
					</center>
                </div>   <ul class=" rlx-nav__sub-list">                    <li class="rlx-nav__section"><a href="<?php echo base_url($this->session->group_level.'/new_releases'); ?>">New Releases</a></li>          <li class="rlx-nav__section"><a href="<?php echo base_url($this->session->group_level.'/product_list'); ?>">The Collection</a></li>		  		  <?php foreach ($categories as  $value) {               // echo $this->session->group_level;            ?>          <li class="dropdown">            <a href="#" class="dropdown-toggle" ><?php echo $value->category_name ?> </a>            <ul class="sub-menu dropdown-menu">              <li><a href="<?php echo base_url($this->session->group_level.'/product_list/'.$value->category_id) ?>">All</a></li>              <?php foreach ($style as  $one_style) { ?>                <li><a href="<?php echo base_url($this->session->group_level.'/product_list/'.$value->category_id.'/'.$one_style->style_id) ?>"><?php echo $one_style->style_name; ?></a></li>              <?php } ?>              <!-- <li><a href="#">Sport</a></li>              <li><a href="#">Luxury</a></li> -->            </ul>          </li>          <?php } ?>		   <?php if (count($page_categories)>3) { ?>            <li class="dropdown"> <a href="#" class="dropdown-toggle">Collection</a>              <ul class="sub-menu dropdown-menu">                 <?php foreach ($page_categories as  $one) { ?>                  <li><a href="<?php echo base_url($this->session->group_level.'/page/'.$one->page_category_id); ?>"><?php echo $one->page_category_name ?></a></li>                <?php } ?>              </ul>            </li>          <?php }else{ ?>          <?php foreach ($page_categories as  $one) { ?>                  <li><a href="<?php echo base_url($this->session->group_level.'/page/'.$one->page_category_id); ?>"><?php echo $one->page_category_name ?></a></li>                <?php } ?>          <?php } ?>          <?php if ($this->session->group_level!="common" && $this->session->group_level!="admin") { ?>                        <!-- <li><a href="#" class="dropdown-toggle">My Account</a></li> -->          <?php } ?>          <li><a href="<?php echo base_url($this->session->group_level.'/contactus'); ?>">Contact Us</a></li>          <!-- <li class="mega-link dropdown"> <a href="#" class="dropdown-toggle">Dealers</a>              <ul class="mega-menu dropdown-menu">                <div class="mega-wrap row">                  <div class="col-sm-3 col-sm-offset-3">                    <li><a href="<?php echo base_url($this->session->group_level.'/apply/distributorship/usa'); ?>">USA Distributorship</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/apply/distributorship'); ?>">Distributorship</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/apply/dealership/usa'); ?>">USA Dealership</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/apply/dealership'); ?>">Dealership</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/search_dealer/dealer'); ?>">Search Dealer</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/search_dealer/distributor'); ?>">Search Distributor</a></li>                  </div>                </div>              </ul>            </li> -->            <li class="mega-link dropdown">                            <a href="#" class="dropdown-toggle">Dealers</a>                            <div class="mega-menu dropdown-menu">                               <div class="mega-wrap row">                                   <div class="col-sm-6">                                       <h6 style="margin-left:75px !important">Distributorship</h6>                                       <ul>                                           <li><a href="<?php echo base_url($this->session->group_level.'/apply/distributorship/usa'); ?>">USA Distributorship</a></li>                                          <li><a href="<?php echo base_url($this->session->group_level.'/apply/distributorship'); ?>">Distributorship</a></li>                                           <li><a href="<?php echo base_url($this->session->group_level.'/search_dealer/distributor'); ?>">Search Distributor</a></li>                                       </ul>                                   </div>                                   <div class="col-sm-6">                                       <h6 style="margin-left:75px !important">Dealership</h6>                                       <ul>                                           <li><a href="<?php echo base_url($this->session->group_level.'/apply/dealership/usa'); ?>">USA Dealership</a></li>                                          <li><a href="<?php echo base_url($this->session->group_level.'/apply/dealership'); ?>">Dealership</a></li>                                           <li><a href="<?php echo base_url($this->session->group_level.'/search_dealer/dealer'); ?>">Search Dealer</a></li>                                       </ul>                                   </div>                                   <!--clothing-->                               </div>                             </div>                        </li>            <li><a  id="search_here">Search</a></li>        </ul>      </div>    <div class="max_width bloc_more_news">
                    <br>
					<div class="line"></div>
					
					<div class="nn">
					<center>
					<br>
                  
				   </center>
					</div>
					
                </div>  
				<center>
<!-- GTranslate: https://gtranslate.io/ -->
<a href="#" onclick="doGTranslate('en|ar');return false;" title="Arabic" class="gflag nturl" style="background-position:-100px -0px;"><img src="//gtranslate.net/flags/blank.png" height="16" width="16" alt="Arabic" /></a><a href="#" onclick="doGTranslate('en|zh-CN');return false;" title="Chinese (Simplified)" class="gflag nturl" style="background-position:-300px -0px;"><img src="//gtranslate.net/flags/blank.png" height="16" width="16" alt="Chinese (Simplified)" /></a><a href="#" onclick="doGTranslate('en|en');return false;" title="English" class="gflag nturl" style="background-position:-0px -0px;"><img src="//gtranslate.net/flags/blank.png" height="16" width="16" alt="English" /></a><a href="#" onclick="doGTranslate('en|fr');return false;" title="French" class="gflag nturl" style="background-position:-200px -100px;"><img src="//gtranslate.net/flags/blank.png" height="16" width="16" alt="French" /></a><a href="#" onclick="doGTranslate('en|ru');return false;" title="Russian" class="gflag nturl" style="background-position:-500px -200px;"><img src="//gtranslate.net/flags/blank.png" height="16" width="16" alt="Russian" /></a><a href="#" onclick="doGTranslate('en|es');return false;" title="Spanish" class="gflag nturl" style="background-position:-600px -200px;"><img src="//gtranslate.net/flags/blank.png" height="16" width="16" alt="Spanish" /></a><a href="#" onclick="doGTranslate('en|ur');return false;" title="Urdu" class="gflag nturl" style="background-position:-100px -700px;"><img src="//gtranslate.net/flags/blank.png" height="16" width="16" alt="Urdu" /></a>

<style type="text/css">
<!--
a.gflag.nturl {
    display: inline-block;    margin: 5px;
}
a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}
-->
</style>

<div id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>

				<!--container--> </center>    </div>     

 
      

  <a href="javascript:void(0)"  onclick="openNav()" class="btnmenu">
  <span class="menu">
					<span></span>
					
					<span></span>
					<span></span>
					
				</span>
				<span class="wmen">MENU</span>
				</a>
				
                                 
                         
  <a style="margin-right: 40px;margin:auto;padding-bottom: 5px;" href="<?php echo base_url() ?>">
    <h1 class="header__title" >
    <img  src="<?php echo UPLOAD_URL; ?>/site/<?php echo $site_logo ?>" class="headerButton" alt="Logo" >
    Millage
     </h1>
  </a>
	
	

   <div class="header-right">
     <!--  <form class="searchbox navbar-form">
        <input type="search" placeholder="Search..." name="search" class="searchbox-input" onkeyup="buttonUp();" required>
        <input type="submit" class="searchbox-submit" value="">
        <span class="searchbox-icon"><i class="pe-7s-search"></i></span>
      </form> -->
      <?php if ($this->session->user_id) { ?>
      <ul class="site-header-cart menu">
        <li class="dropdown"> 
          <a href="#" data-toggle="dropdown" title="">
              <span class="count rounded-crcl"><?php echo count($cart_item); ?></span>
              <i class="pe-7s-shopbag icon"></i>
          </a>
          <div class="dropdown-menu widget woocommerce widget_shopping_cart canvas" style="overflow-x:scroll; ">
            <!-- <button type="button" class="close"> <span>&times;</span> </button> -->
            
            <div class="widget_shopping_cart_content">
              <h6 class="title">Your Shopping Cart (<?php echo count($cart_item); ?> Items)</h6>
              <ul class="cart_list product_list_widget ">
                <?php foreach ($cart_item as $cart) { 
                    $watch_option=$this->custom->getSingleRow("watch_options_master",array('watch_option_id'=>$cart->watch_option_id));
                    $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_option->watch_id));
                    $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
                    $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
                    $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
                  ?>
                  <li class="mini_cart_item">
                    <table>
                      <tr>
                        <td class="figure"><figure> <img src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>" alt=""> </figure></td>
                        <td class="content"><a id="<?php echo $cart->order_item_id; ?>" class="remove" title="Remove this item"> <i class="fa fa-close"></i> </a>
                          <h6 class="product-title"> <?php echo $series_data; ?> </h6>
                          <ul class="product-info m-0">
                            
                             <li>Price : $
                             <?php
                                if($this->session->userdata("group_level")=="dealer")
                                {
                                   echo $watch_option->dealer_price;
                                }
                                else if($this->session->userdata("group_level")=="retailer")
                                {
                                   echo $watch_option->distributor_price;
                                }
                                else
                                {
                                   echo $watch_option->retail_price;
                                }
                             ?> 

                           <?php //echo $watch_option->retail_price; ?></li>




                            <li>Qty : <?php echo $cart->quantity;  ?>
                              <button type="button" onclick="$('.<?php echo $cart->order_item_id ?>_qty_update').toggle();" class="btn-xs btn-danger">Edit</button>
                              <span class="form-inline <?php echo $cart->order_item_id ?>_qty_update" style="display: none;">
                                <button type="button" id="btn_min" class="<?php echo $cart->order_item_id ?> btn-xs btn-primary">-</button>
                                <input type="number" min="1" max="<?php echo $watch_option->dial_stock;?>" name="" style="height:26px;width: 20%" id="input_qty" class="input-text qty <?php echo $cart->order_item_id ?>_qty_new_value input-xs" readonly value="<?php echo $cart->quantity;  ?>">
                                <button type="button" class="<?php echo $cart->order_item_id ?> btn-xs btn-primary" id="btn_max">+</button>
                                <button onclick="update_qry('<?php echo $cart->order_item_id ?>')" class="btn-xs btn-success">Update</button>
                              </span>
                            </li>
                            <li>Style : <?php echo  $this->custom->getSingleValue("style_master","style_name",array('style_id'=>$watch_option->watch_id)); ?></li>
                            <li>Color : <?php echo  ucwords($watch_option->dial_color); ?></li>
                          </ul></td>
                      </tr>
                    </table>
                  </li>
                <?php } ?>
              </ul>
              <?php if(!empty($cart_item)): ?>
              <div class="cart-collaterals">
                <div class="cart_totals">
                  <table class="shop_table shop_table_responsive">
                    <tbody>
                      <tr class="cart-subtotal">
                        <th>Cart subtotal </th>
                        <td data-title="Subtotal">$<?php echo $cart_item[0]->total; ?> </td>
                      </tr>
                      <tr class="shipping">
                        <th> Shipping + Handling </th>
                        <td data-title="Shipping"><span class="shipping text-green">$<?php echo $cart_item[0]->shipping_cost ?></span></td>
                      </tr>
                      <tr class="order-total">
                        <th>grand Total</th>
                        <td data-title="Total"> $<?php echo number_format($cart_item[0]->shipping_cost+$cart_item[0]->total,2) ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="buttons"><!--  <a href="<?php echo base_url('customer/view_cart') ?>" class="button">view cart</a>  --><a href="<?php echo base_url($this->session->group_level.'/checkout') ?>" class="button checkout" style="width: 100%;padding:15px">checkout</a> </div>
            <?php endif; ?>
            </div>
          </div>
        </li>
      </ul> 
      <ul class="navbar-nav pull-right">
       <li class="dropdown"> <a href="#" class="dropdown-toggle">Welcome  , <?php echo $this->session->username; ?> </a>
            <ul class="sub-menu dropdown-menu" style="background: #fff;">

              <?php
                if($this->session->userdata("group_level")=="dealer")
                {
                   $user_nm="dealer";
                }
                else if($this->session->userdata("group_level")=="retailer")
                {
                    $user_nm="retailer";
                }
                else
                {
                  $user_nm="customer";
                }
              ?>
              <li><a href="<?php echo base_url($user_nm.'/my_orders') ?>" class="dropdown-toggle">My Orders</a></li>
              <li><a href="<?php echo base_url($user_nm.'/manage_profile') ?>">My account</a></li>
              <li><a href="<?php echo base_url($user_nm.'/change_password') ?>">Change Password</a></li>
              <li><a href="<?php echo base_url('common/logout') ?>">Logout</a></li>
            </ul>
          </li>
        </ul>
      <?php }else{ ?>
      <!-- <ul class="login">
        <li>Welcome  , <?php echo $this->session->username; ?> </li>
        <li> <a href="<?php echo base_url('common/logout') ?>" class="btn-login" data-show=""><i class="pe-7s-users"></i><span>Logout</span></a> </li>
      </ul> -->
        <!-- <ul class="nav navbar-nav">
        <li> <a href="#" class="trigger-modal" data-show="#login"><i class="pe-7s-users"></i><b>Login</b></a> </li>
        <li>/</li>
        <li class="register"> <a href="#" data-show="#register" class=" btn-signup trigger-modal"><i class="fa fa-edit"></i><b>Register</b></a> </li>
      </ul> -->
      <ul class="login">
          <!-- <li><a style="margin-top: -30px" href="<?php //echo base_url($this->session->group_level.'/new_releases'); ?>">New Releases</a></li> -->
          <li> <a href="javascript:void(0)" onclick="openLogin()" style="font-size: 15px; margin:0px" class="trigger-modal" data-show="#login" style="color:a37e2c" ><!-- <i class="pe-7s-users"></i> --><b>Login</b></a> </li>
          <li><a  style="font-size: 15px;"></a></li>
          <li> <a href="#" style="font-size: 15px;" class="trigger-modal" data-show="#register"><!-- <i class="pe-7s-users"></i> --><b></b></a> </li>
        </ul>
      <?php } ?>
	  
    </div>
	
  
   
    <!-- /.navbar-collapse -->
    
  
</header>
<?php if(isset($this->session->user_id)){?><script src="<?php echo JS_URL; ?>menu.js"></script>  <script type="text/javascript">      var position = 0;      var mainPanel = document.getElementById("mainPanel");      var leftDrawer = document.getElementById("leftDrawer");      var rightDrawer = document.getElementById("rightDrawer");      function toggle(evt) {        position++;                 if (position % 3 == 1) {          leftDrawer.classList.add("open");          rightDrawer.classList.remove("open");        } else {          leftDrawer.classList.remove("open");          rightDrawer.classList.add("open");        }        }      mainPanel.addEventListener("click", toggle);      leftDrawer.addEventListener("click", toggle);      rightDrawer.addEventListener("click", toggle);          </script>	   <script>  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;  e=o.createElement(i);r=o.getElementsByTagName(i)[0];  e.src='//www.google-analytics.com/analytics.js';  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));  ga('create','UA-52746336-1');ga('send','pageview');  var isCompleted = {};  function sampleCompleted(sampleName){    if (ga && !isCompleted.hasOwnProperty(sampleName)) {      ga('send', 'event', 'WebCentralSample', sampleName, 'completed');       isCompleted[sampleName] = true;    }  }</script>    <script type="text/javascript">      function init() {        window.matchMedia("(min-width: 800px)").addListener(hitMQ);        window.matchMedia("(min-width: 500px)").addListener(hitMQ);      }      function hitMQ(evt) {        sampleCompleted("RWDPatterns-OffCanvas");      }      init();    </script>
<script type="text/javascript"> 
  $(".dropdown-menu > .widget_shopping_cart_content > .cart_list > .mini_cart_item > table > tbody > tr > .content > .product-info > li > .btn-xs ").click(function(event) {
    event.preventDefault();
    return false;
  });
  $(".dropdown-menu > .widget_shopping_cart_content > .cart_list > .mini_cart_item > table > tbody > tr > .content > .product-info > li > span > .btn-xs ").click(function(event) {
    $(this).each(function(index, el) {
      id=$(this).attr('id');
      class_name=$(this).attr('class').split(' ')[0];
      if (id=="btn_max") {

        var max=<?php echo $watch_option->dial_stock; ?>;
        var qty=$("."+class_name+"_qty_new_value").val();
        if(max>qty)
        {
            $("."+class_name+"_qty_new_value").val(parseInt($("."+class_name+"_qty_new_value").val())+1);
        }
      }
      if (id=="btn_min") {
        var qty=$("."+class_name+"_qty_new_value").val();
        if(qty>1)
        {
            $("."+class_name+"_qty_new_value").val($("."+class_name+"_qty_new_value").val()-1);
        }
      }
    });
    event.preventDefault();
    return false;
  });
</script>
<?php }?>