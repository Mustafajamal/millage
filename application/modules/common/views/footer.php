<div class="container-fluid">
  <div class="row">
         <div class="span12">
        <div class=" center well well-small text-center">
                 <h2>Newsletter</h2>
                 
                 <p>Subscribe to our weekly Newsletter and stay tuned.</p>
                 
                 
                  <?php 
                      $attributes = array('class' => 'validate', 'id' => 'subscribe_form','method'=>"post");
                      echo form_open('#',$attributes);
                  ?>
                <div class="row">    
                  <div class="col-md-4 col-md-offset-4">
                     <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
                      <?php
                        $email=array("name"=>"subscriber_email","id"=>"subscriber_email","class"=>"form-control",'placeholder'=>"your@email.com","required"=>"required");
                         echo form_email($email);
                         ?>
                     </div>
                  </div>
                </div>

                   
                     <input type="submit" value="Subscribe Now !" id="subscribe_btn" class="btn btn-large btn-success" />
                      <!-- <input type="submit" value="Unsubscribe Now !" id="unsubscribe_btn" class="btn btn-large btn-danger" />-->
               <?php echo form_close(); ?>
             </div>    
         </div>
  </div>
  </div>


<div class="modal fade login" id="modal-login" tabindex="-1" role="dialog" style="margin:0px;">
  <div class="modal-dialog" role="document">
    <h2 class="pull-left text-uppercase">My Account</h2>
    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><a href="javascript:void(0)" onclick="closeLogin()">&times;</a></span>
    </button>
    <div id="login" class="hidden">
      <div class="content text-center">
        <div class="top mb-35">
          <h4 class="contact-title">
            <i class="pe-7s-users primary-color"></i>
            Login Account
          </h4>
          <p>Enter your username and password to login.</p>
        </div>
        <!--top-->
        <form action="#" class="validate" id="login_form" method="post">
          <div class="row">
            <?php
              $frm_username=array("name"=>"username","id"=>"user_username","class"=>"form-control","placeholder"=>"Email","required"=>"");
              echo form_email($frm_username,'',"Email");
              $frm_password=array("name"=>"password","id"=>"password","class"=>"form-control","placeholder"=>"Password","required"=>"");
              echo form_password($frm_password,'',"Password");
              $frm_realperson=array("name"=>"login_captcha","id"=>"login_captcha","class"=>"form-control","placeholder"=>"Please enter the letters displayed","required"=>"");
              echo form_input($frm_realperson,'',"Please enter the letters displayed");
              ?>
              <!-- <div id="recaptcha1"></div> -->
            <!-- <div class="form-group col-sm-6 pull-left">
              <div class="checkbox-wrap">
                <input type="checkbox" value="1">
                <label>Remember Me</label>
              </div>
            </div> -->
            <!--checkbox-->
            <div class="form-group col-sm-6 pull-right">
              <a href="#" data-show="#forgot-pass" class="primary-color pull-right btn-password trigger-modal">Lost Password?  </a>
            </div>
            <!--password-->
            <div class="form-group col-sm-12">
              <button type="submit" class="btn btn-block" id="login_btn">Login</button>
            </div>
            <!--button-->
          </div>
        </form>
        <div class="or mb-15">OR</div>
        <div class="row">
          <ul class="login-social col-sm-12">
            <li>
              <a href="#" class="fb">
              <i class="fa fa-facebook"></i> FACEBOOK
              </a>
            </li>
            <li>
              <a href="#" class="twt">
              <i class="fa fa-twitter"></i> TWITTER
              </a>
            </li>
          </ul>
        </div>
        Don’t have an account yet?
        <a href="#" class="trigger-modal primary-color btn-signup" data-show="#register"> <strong>Signup Now!</strong></a>
      </div>
    </div>
    <!-- login -->
    <div id="register" class="hidden">
      <div class="content text-center sign-up-block">
        <div class="top mb-35">
          <h4 class="contact-title">
            <i class="pe-7s-add-user primary-color"></i>
            Signup Account
          </h4>
          <p>Fill up the form below to signup an account.</p>
        </div>
        <!--top-->
        <form action="#" class="validate" id="reg_form" method="post">
          <div class="row">
            <?php 
              $frm_username=array("name"=>"username","id"=>"username","class"=>"form-control","placeholder"=>"Username","required"=>"");
              echo form_input($frm_username,'',"Username");
              $frm_username=array("name"=>"email","id"=>"email","class"=>"form-control","placeholder"=>"Email","required"=>"");
              echo form_email($frm_username,'',"Email");
              $frm_username=array("name"=>"first_name","id"=>"first_name","class"=>"form-control","placeholder"=>"First Name","required"=>"");
              echo form_input($frm_username,'',"First Name");
              $frm_username=array("name"=>"last_name","id"=>"last_name","class"=>"form-control","placeholder"=>"Last Name","required"=>"");
              echo form_input($frm_username,'',"Last Name");
              $frm_password=array("name"=>"password","id"=>"password_value","class"=>"form-control","placeholder"=>"Password","required"=>"");
              echo form_password($frm_password,'',"Password");
              $frm_c_password=array("name"=>"c_password","id"=>"c_password","class"=>"form-control","placeholder"=>"Verify Password","required"=>"","data-rule-equalTo"=>"#password_value");
              echo form_password($frm_c_password,'',"Verify Password");
              $frm_realperson=array("name"=>"reg_captcha","id"=>"reg_captcha","class"=>"form-control","placeholder"=>"Please enter the letters displayed","required"=>"");
              echo form_input($frm_realperson,'',"Please enter the letters displayed");
            ?>
            <!-- <div id="recaptcha2"></div> -->
            <!-- <div class="g-recaptcha" data-sitekey="6LcFPzMUAAAAAGcrPd6kvNVEQtpTslgZ0bpzAZ9L"></div> -->
            <!--user name-->
            <div class="form-group col-sm-12">
              <button type="submit" id="reg_form_btn" class="btn btn-block">signup</button>
            </div>
            <!--button-->
          </div>
        </form>
        <div class="or mb-15">OR</div>
        <div class="row">
          <ul class="login-social col-sm-12">
            <li>
              <a href="#" class="fb">
              <i class="fa fa-facebook"></i> FACEBOOK
              </a>
            </li>
            <li>
              <a href="#" class="twt">
              <i class="fa fa-twitter"></i> TWITTER
              </a>
            </li>
          </ul>
        </div>
        Have an account?
        <a href="#" class="primary-color btn-login trigger-modal" data-show="#login"> <strong>Login Now!</strong></a>
      </div>
    </div>
    <!-- register -->
    <div id="forgot-pass" class="hidden">
      <div class="content text-center forget-password-block">
        <div class="top mb-35">
          <h4 class="contact-title">
            <i class="pe-7s-door-lock primary-color"></i>
            Lost Password
          </h4>
          <p>Fill up the form below to Retrieve your password.</p>
        </div>
        <!--top-->
        <form action="#" class="validate" id="forgot_form" method="post">
          <div class="row">
            <?php 
              $frm_username=array("name"=>"email","id"=>"forgot_email","class"=>"form-control","placeholder"=>"Email","required"=>"");
              echo form_email($frm_username,'',"Email");
              $frm_realperson=array("name"=>"forgot_captcha","id"=>"forgot_captcha","class"=>"form-control","placeholder"=>"Please enter the letters displayed","required"=>"");
              echo form_input($frm_realperson,'',"Please enter the letters displayed");
            ?>
            <!-- <div id="recaptcha3"></div> -->
            <!--user name-->
            <div class="form-group col-sm-12">
              <button type="submit" id="forgot_form_btn"  class="btn btn-block">Reset</button>
            </div>
            <!--button-->
          </div>
        </form>
      </div>
    </div>
    <!-- forget password -->
  </div>
  <!-- /.modal-dialog -->
</div>
<style type="text/css">
  @import url(https://fonts.googleapis.com/css?family=Josefin+Sans:300,400);
  #section03 {
    position: static;
    width: 100%;
    height: 100%;
  }
  #section03::after {
    position: absolute;
    bottom: 0;
    left: 0;
    content: '';
    width: 100%;
    height: 30%;
    background: -webkit-linear-gradient(top,rgba(0,0,0,0) 0,rgba(0,0,0,.8) 80%,rgba(0,0,0,.8) 100%);
    background: linear-gradient(to bottom,rgba(0,0,0,0) 0,rgba(0,0,0,.8) 80%,rgba(0,0,0,.8) 100%);
  }

  .demo a {
    position: absolute;
    bottom: 20px;
    left: 50%;
    z-index: 2;
    display: inline-block;
    -webkit-transform: translate(0, -50%);
    transform: translate(0, -50%);
    color: #fff;
    font : normal 400 20px/1 'Josefin Sans', sans-serif;
    letter-spacing: .1em;
    text-decoration: none;
    transition: opacity .3s;
  }
  .demo a:hover {
    opacity: .5;
  }

  #section03 a {
    padding-top: 20px;
  }
  #section03 a span {
    position: absolute;
    top: 0;
    left: 50%;
    width: 46px;
    height: 46px;
    margin-left: -23px;
    border: 1px solid #fff;
    border-radius: 100%;
    box-sizing: border-box;
  }
  #section03 a span::after {
    position: absolute;
    top: 50%;
    left: 50%;
    content: '';
    width: 16px;
    height: 16px;
    margin: -10px 0 0 -8px;
    border-left: 1px solid #fff;
    border-bottom: 1px solid #fff;
    -webkit-transform: rotate(135deg);
    transform: rotate(135deg);
    box-sizing: border-box;
  }
  #section03 a span::before {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    content: '';
    width: 44px;
    height: 44px;
    box-shadow: 0 0 0 0 rgba(255,255,255,.1);
    border-radius: 100%;
    opacity: 0;
    -webkit-animation: sdb03 3s infinite;
    animation: sdb03 3s infinite;
    box-sizing: border-box;
  }
  @-webkit-keyframes sdb03 {
    0% {
      opacity: 0;
    }
    30% {
      opacity: 1;
    }
    60% {
      box-shadow: 0 0 0 60px rgba(255,255,255,.1);
      opacity: 0;
    }
    100% {
      opacity: 0;
    }
  }
  @keyframes sdb03 {
    0% {
      opacity: 0;
    }
    30% {
      opacity: 1;
    }
    60% {
      box-shadow: 0 0 0 60px rgba(255,255,255,.1);
      opacity: 0;
    }
    100% {
      opacity: 0;
    }
  }
</style>
<footer>
  <div class="footer">
    <div class="container">
	<div class="row">
      <section id="section03" class="demo">
        <a  href="#top"><span></span></a>
      </section>
	   <div class="col-md-4 col-sm-4 col-xs-12 copy-right">
	   <center>
	     <div ><a href="<?php echo  base_url($this->session->group_level."/page/2/Company Profile");?>" class="primary-color"> <strong> About</strong></a></div>       
		<div ><a href="<?php echo  base_url($this->session->group_level."/page/1/Warranty and Repair");?>" class="primary-color"> <strong> Customer Service</strong></a></div>
           <div ><a href="<?php echo  base_url($this->session->group_level."/page/1/Warranty and Repair");?>" class="primary-color"> <strong> Terms & Conditions</strong></a></div>
        </center>
	  </div>
	  
	   <div class="col-md-4 col-sm-4 col-xs-12 copy-right">
	   <center>
	     <div ><a href="<?php echo  base_url($this->session->group_level."/contactus");?>" class="primary-color"> <strong> Contact US</strong></a></div>
        <div ><a href="<?php echo  base_url($this->session->group_level."/page/3/A Trademark Design");?>" class="primary-color"> <strong> Advertising</strong></a></div>
		<div ><a href="<?php echo  base_url($this->session->group_level."/site_map");?>" class="primary-color"> <strong> SITE MAP</strong></a></div>
        </center>
	  </div>
	 
	   <div class="col-md-4 col-sm-4 col-xs-12 copy-right">
	    <center>
	     <div ><a href="<?php echo  base_url($this->session->group_level."/customer/manage_profile");?>" class="primary-color"> <strong>My Account</strong></a></div>
        <div ><a href="<?php echo  base_url($this->session->group_level."/customer/my_orders");?>" class="primary-color"> <strong> Order History</strong></a></div>       
        <div ><a href="<?php echo  base_url($this->session->group_level."/apply/dealership");?>" class="primary-color"> <strong> Newsletter</strong></a></div>
        </center>
	  </div>
	  
        <!--right--> 
      
	  </div>
	  
    <hr>
    <div class="row logo-header">
	
	
        <div class="col-md-5 col-sm-5 col-lg-5 col-md-offset-5 col-sm-offset-5 col-lg-offset-5">
            <center><a class="navbar-brand" href="<?php echo base_url() ?>"> <img height="70px" width="70px" src="<?php echo UPLOAD_URL; ?>/site/<?php echo $site_logo ?>" class="hidden-xs" style="margin-left:30px" alt="Logo"></a></center>   
            <h6 style="color: #946F29;margin-left: 40px" class="hidden-xs">Millage</h6>
        </div> 
    </div>
<div class="row">
                     <div class="col-md-3 col-sm-3 col-xs-12 copy-right"> ©  <?php echo date('Y');?> <a href="<?php echo  base_url();?>" class="primary-color"> <strong> millage.com</strong></a>. </div>

	     <div class="col-md-2 col-sm-2 col-xs-12 copy-right" ><a href="<?php echo  base_url($this->session->group_level."/new_releases");?>" class="primary-color"> <strong> NEW RELEASES</strong></a></div>
        <div class="col-md-2 col-sm-2 col-xs-12 copy-right"><a href="<?php echo  base_url($this->session->group_level."/product_list");?>" class="primary-color"> <strong> THE COLLECTION</strong></a></div>
       
        <div class="col-md-2 col-sm-2 col-xs-12 copy-right" ><a href="<?php echo  base_url($this->session->group_level."/apply/dealership");?>" class="primary-color"> <strong> BECOME DEALER</strong></a></div>
       
	  <div class="col-md-3 col-sm-3 col-xs-12 copy-right"> 
          <ul class="social-icons">
            <li> <a href="https://www.facebook.com/MillageWatchCompany/"><i class="fa fa-facebook"></i></a> </li>
            <li> <a href="https://twitter.com/millagewatches?lang=en"><i class="fa fa-twitter"></i></a> </li>
            <li> <a href="https://in.linkedin.com/company/millage-watch-company"><i class="fa fa-linkedin"></i></a> </li>
            <li> <a href="https://www.instagram.com/p/BKhvsjDj68T/"><i class="fa fa-instagram"></i></a> </li>
          </ul>
		  </div>
</div>	  
	
	
	  <br><br>
	  <div class="max_width bloc_more_news">
                    <div class="line"></div>
				
					
                </div>
	 
		</div>
    </div>
  </div>
</footer>
</main>
<!--footer--> 
<!-- back to top--> 
<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a> 
<!-- Js libery --> 
<script src="<?php echo JS_URL; ?>bootstrap.min.js"></script> 
<script src="<?php echo JS_URL; ?>modernizr.js"></script> 
<!--jquery ui--> 
<script src="<?php echo JS_URL; ?>jquery-ui.js"></script> 
<script src="<?php echo JS_URL; ?>jquery-ui-touch-punch.js"></script> 
<!-- masonry --> 
<script src="<?php echo JS_URL; ?>masonry.pkgd.min.js"></script> 
<!-- slider js --> 
<script src="<?php echo JS_URL; ?>jquery.bxslider.min.js"></script> 
<!--color box--> 
<script src="<?php echo JS_URL; ?>jquery.colorbox-min.js"></script> 
<!-- wow --> 
<script src="<?php echo JS_URL; ?>wow.min.js"></script> 
<!-- parallax --> 
<script src="<?php echo JS_URL; ?>parallax.js"></script> 
<!-- jquery.dlmenu --> 
<script src="<?php echo JS_URL; ?>jquery.dlmenu.js"></script> 
<!-- count down --> 
<script type="text/javascript" src="<?php echo JS_URL; ?>jquery.countdown.min.js"></script> 
<!--Costom js--> 
<script src="<?php echo JS_URL; ?>main.js"></script>

<script type="text/javascript" src="<?php echo JS_URL.'jquery.form.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'jquery.validate.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'custom.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'toastr.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JS_URL.'ajax.js'; ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="<?php echo JS_URL.'jquery.plugin.js'; ?>"></script>
<script src="<?php echo JS_URL.'jquery.realperson.js'; ?>"></script>
<script type="text/javascript">

$(function(){
  $(window).scroll(function(){
    var aTop = $('.header').height();
    if($(this).scrollTop()>=aTop){
         document.getElementById("p1").innerHTML = "";
    }
  });
});
if (window.matchMedia("(max-width:800px)").matches)  {
				
				
                   document.getElementById("p1").innerHTML = "";
                } else {
  // window width is less than 500px
                
                }

		jQuery(document).ready(function($){
		
		

		  	$(window).scroll(function() {
		  		
		  		console.log("asdf");

				if ($(window).scrollTop() > 50 ){
		    
		 	  //	$('.top-header').addClass('shows');
				
				
			     document.getElementById("p1").innerHTML = "";
				
				
				
		    
		  		} else {
				
				
				
		    
		   	 	//$('.top-header').removeClass('shows');
				
				 document.getElementById("p1").innerHTML = "Millage";
				 
				
				 
				
		    
		 		};   	
			});

		  });

		jQuery('.scroll').on('click', function(e){	

		
				e.preventDefault()
				
		    
		  jQuery('html, body').animate({
		      scrollTop : jQuery(this.hash).offset().top
		    }, 1500);
		});


		</script>
<script type="text/javascript">
         
		
		  
		  
          $('#login_captcha').realperson({chars: $.realperson.alphanumeric}); 
          $('#forgot_captcha').realperson({chars: $.realperson.alphanumeric}); 
          $('#reg_captcha').realperson({chars: $.realperson.alphanumeric}); 
          // var message="<?php //echo $this->session->flashdata('message'); ?>";
          // if (message!="") {
          //   toastr["info"](message,"Message");
          // }
          var options = { 
            url : "<?php echo base_url('common/ajax_controller/subscriber_process') ?>",
            beforeSubmit: function() {
              $("#subscribe_btn").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>").attr('disabled', true);
            },
            clearForm:true,
            resetForm:true,
            success:function (data) {
                $("#subscribe_btn").val("Subscribe Now!").attr('disabled', false);
                data=$.trim(data);
                
                if (data=="success") {
                    toastr["success"]("Subscribe Successfully","Message");
                }
                 else if(data=="Already Subscribed")
                {
                  toastr["error"](data,"Message");
                }
                else{
                  toastr["error"]("Try Again","Message");
                }
            },
          }
           var un_options = { 
            url : "<?php echo base_url('common/ajax_controller/unsubscriber_process') ?>",
            beforeSubmit: function() {
              $("#unsubscribe_btn").val("<i class='fa fa-spin fa-refresh fa-pulse'></i>").attr('disabled', true);
            },
            clearForm:true,
            resetForm:true,
            success:function (data) {
                 $("#unsubscribe_btn").val("Unsubscribe Now!").attr('disabled', false);
                data=$.trim(data);
                
                if (data=="success") {
                    toastr["success"]("Unsubscribe Successfully","Message");
                }
                 else if(data=="You have not Subscribed")
                {
                  toastr["error"](data,"Message");
                }
                else{
                  toastr["error"]("You have already Unubscribed","Message");
                }
            },
          }
          $("#subscribe_btn").click(function(event) {
            if ($('#subscribe_form').valid()) {
              $('#subscribe_form').ajaxForm(options); 
            }
          });
          $("#unsubscribe_btn").click(function(event) {
            if ($('#subscribe_form').valid()) {
              $('#subscribe_form').ajaxForm(un_options); 
            }
          });

        //$("#contactus_form").ajaxForm();
</script>
<script type="text/javascript">
  
  $('select').select2();
  var login_options = { 
    url : "<?php echo base_url('common/ajax_controller/login_check') ?>",
    beforeSubmit: function() {
      $("#login_btn").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>");
    },
    success:function (data) {
      $("#login_btn").html("Submit");  
      data=$.trim(data);
      $("#login_btn").html("Sign In");
      data=$.trim(data);
      if (data=="customer" || data=="dealer" || data=="retailer" || data=="admin") {
          toastr["success"]("Successfully Logged In <br> Redirecting.....","");
          window.location.href = "<?php echo base_url() ?>";
      }
      else{
          toastr["error"](data,"Login Failed");
      }
    },
  };
  var forgot_options = { 
    url : "<?php echo base_url('common/ajax_controller/forgot_password_recover') ?>",
    beforeSubmit: function() {
      $("#forgot_form_btn").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>").attr("disabled",true);
    },
    success:function (data) {
      $("#forgot_form_btn").html("Reset").attr("disabled",false);  
      data=$.trim(data);
     try {
            data=$.parseJSON(data);
        } catch (e) {
            toastr["error"](data,"Message");
            return false;
        }
        if (data.message=="success") {
            // $('#signup_form').reset();
            toastr["success"]("Successfully Registred <br> Redirecting.....","");
            window.location.href = "<?php echo base_url() ?>";
          // toastr["success"](data,"Account Registred Successfully ! Account Activation Mail Sent To Your Account");
        }
        else{
          toastr["error"](data.message,"Message");
        }
    },
  };
  var reg_options = { 
    url : "<?php echo base_url('common/ajax_controller/register') ?>",
    beforeSubmit: function() {
      $("#reg_form_btn").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>").attr("disabled",true);
    },
    success:function (data) {
        $("#reg_form_btn").html("signup").attr("disabled",false);
        data=$.trim(data);
        try {
            data=$.parseJSON(data);
        } catch (e) {
            toastr["error"](data,"Message");
            return false;
        }
        if (data.message=="success") {
            // $('#signup_form').reset();
            toastr["success"]("Successfully Registred <br> Redirecting.....","");
            window.location.href = "<?php echo base_url() ?>";
          // toastr["success"](data,"Account Registred Successfully ! Account Activation Mail Sent To Your Account");
        }
        else{
          toastr["error"](data.message,"Message");
        }
    },
  }; 
  $("#login_btn").click(function(event) {
      // var form_id=$("form").attr('id');
      // alert(form_id);
      // if (form_id=="login_form") {
        $('#login_form').ajaxForm(login_options); 
      // }
      // if (form_id=="forgot_password_form") {
      //   $('#forgot_password_form').ajaxForm(forgot_options); 
      // }
    });
  $("#reg_form_btn").click(function(event) {
    $('#reg_form').ajaxForm(reg_options); 
  });
  $("#forgot_form_btn").click(function(event) {
    $('#forgot_form').ajaxForm(forgot_options); 
  });
  
  $(".remove").click(function() {
  item_id=$(this).attr('id');
  url='<?php echo base_url('common/order_ajax_controller/remove_item') ?>';
  jqxhr.ajaxRequest(url,{order_item_id:item_id});
  ajaxCall.done(function(data){
    data=$.trim(data);
    if (data=="success") {
        location.reload();
      }
      else{
        toastr["warning"](data,"Message");
      }
  })
  });
  
  $(".add_cart").click(function() {
  item_id=$(this).attr('id');
  url='<?php echo base_url('common/order_ajax_controller/add_item') ?>';
  jqxhr.ajaxRequest(url,{watch_option_id:item_id});
  ajaxCall.done(function(data){
      data=$.trim(data);
      if (data=="success") {
        location.reload();
      }
      else{
        toastr["warning"](data,"Message");
      }
  })
  });
  
  $(".btn_cart").click(function() {
  post_data=$(this).parent().parent().parent().parent().parent().parent().serialize();
  console.log(post_data);
  url='<?php echo base_url('common/order_ajax_controller/add_item') ?>';
  jqxhr.ajaxRequest(url,post_data);
  ajaxCall.done(function(data){
      data=$.trim(data);
      if (data=="success") {
        location.reload();
      }
      else{
        toastr["warning"](data,"Message");
      }
  })
  });
  
  function update_qry(id) {
  // alert(new_qty);
  new_qty=$('.'+id+'_qty_new_value').val();
  url='<?php echo base_url('common/order_ajax_controller/update_qty') ?>';
  jqxhr.ajaxRequest(url,{new_qty:new_qty,order_item_id:id});
  ajaxCall.done(function(data){
      data=$.trim(data);
      if (data=="success") {
        location.reload();
      }
      else{
        toastr["warning"](data,"Message");
      }
  })
  }
</script>
<script type="text/javascript">
  toastr.options = {
  // "positionClass": "toast-top-center",
  "progressBar": true,
  "showDuration": "300",
  "hideDuration": "10000",
  "timeOut": "10000",
  "extendedTimeOut": "1000",
  "closeButton": true,
  "showMethod": "slideDown",
  "hideMethod": "slideUp",
  "preventDuplicates": true,
  
  }
  // toastr["error"]("message_error","Message");
  var message_error="<?php echo $this->session->flashdata('message-error'); ?>";
    if (message_error!="") {
      toastr["error"](message_error,"Message");
    }
    var message_success="<?php echo $this->session->flashdata('message-success'); ?>";
    if (message_success!="") {
      toastr["success"](message_success,"Message");
    }
    var message_info="<?php echo $this->session->flashdata('message-info'); ?>";
    if (message_info!="") {
      toastr["info"](message_info,"Message");
    }
    var message_warning="<?php echo $this->session->flashdata('message-warning'); ?>";
    if (message_warning!="") {
      toastr["warning"](message_warning,"Message");
    }
</script>
<script type="text/javascript" src="https://unpkg.com/sweetalert2@7.1.0/dist/sweetalert2.all.js"></script>
<script type="text/javascript">
  $(function(){
    $("a[href='#top']").click(function() {
      document.location.href = "#back";
    });
    $('#search_here').click(function(event) {
      var group_level='<?php echo $this->session->group_level; ?>';
        swal({
          title: '<font class="text-danger">Custom Product Search</font>',
          html:"<?php echo $custom_search; ?>",
          width: 1000,
          confirmButtonColor:'#43a047',
          cancelButtonColor:'#ec407a',
          showCancelButton: true,
          confirmButtonText: 'Search',
          onOpen: function () {
                $('.select2').select2({
                    width: '100%',
                 });
                $("#slider-range1").slider({
                  range: true,
                  min: 0,
                  max: <?php echo $max_price; ?>,
                  values: [0, <?php echo $max_price; ?>],
                  slide: function(event, ui) {
                      $("#amount5").html("$" + ui.values[0] + " - $" + ui.values[1]);
                      $("#amount3").val(ui.values[0]);
                      $("#amount4").val(ui.values[1]);
                  }
              });

              $("#amount5").html("$" + $('#slider-range1').slider("values", 0) +
                  " - $" + $('#slider-range1').slider("values", 1));
            },
            preConfirm: () => {
               var cust_url='<?php echo base_url();?>'+group_level+'/product_list/'+$('#gender').val()+'?style='+$('#style').val()+'&series='+$('#cust_series').val()+'&color='+$('#cust_color').val()+'&amount1='+$('#amount3').val()+'&amount2='+$('#amount4').val();
               
               window.location.href=cust_url;
          },

        }).then(function(result){
           // function when confirm button clicked
           if(result.dismiss == 'cancel' || result.dismiss == 'overlay' || result.dismiss == 'close' || result.dismiss == 'esc'){
              location.reload();
           }
        });
    });
  });
</script>
</body>
</html>