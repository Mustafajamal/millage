<!-- <link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>base.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL.'myOrders.min.css' ?>"> -->
<div class="breadcrumbs">

    <!--main banner-->
    <main class="main ">
        <div class="">
            <div class="container">
                <!-- <h2 class="text-center"><?php echo $category_name.$style_name; ?></h2> -->
                <div class="row">                
                  <div class="col-md-24 col-sm-12 col-xs-12">
                    <?php
                            $orders=array();
                            $class="btn-primary";
                            $i=0;
                            // d($order_data);
                            foreach ($order_data as $order) {
                                $watch_option=$this->custom->getSingleRow("watch_options_master",array('watch_option_id'=>$order->watch_option_id));
                                $product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_option->watch_id));
                                $series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
                                $style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
                                $category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
                                if (!in_array($order->order_id,$orders)) { 
                                    $orders[]=$order->order_id;
                                    $i++;
                                    if ($order->order_status=="SHIPPED") {
                                        $class="btn-warning";
                                    }
                                    if ($order->order_status=="SUCCESS") {
                                        $class="btn-success";
                                    }
                                    if ($order->order_status=="CANCELED") {
                                        $class="btn-danger";
                                    }
                                    if ($i!=1) {
                                        echo "
                                        </div>
                                         </div>
                                      </div>
                                        ";
                                    }
                                     ?>
                                      <div class="outerOrderDetails">
                                        <div class="pull-right">
                                          <button class="btn <?php echo $class ?>"><?php echo $order->order_status ?></button>
                                          <?php if ($order->order_status=="CANCELED" || $order->order_status=="PENDING") { ?>
                                          <form action="<?php echo base_url($this->session->group_level."/payment") ?>" class="form-inline" method="post">
                                          <input type="hidden" name="order_id_table" value="<?php echo $order->order_id ?>">
                                          <input type="hidden" name="payable_amount" value="<?php echo $order->total ?>">
                                          <button class="btn btn-primary">PAY NOW</button>
                                          </form>
                                          <?php } ?>
                                        </div>
                                         <h5>Order ID: <?php echo strtotime($order->order_date); ?>
                                         </h5>
                                         <div class="ordDate">Placed on <?php echo $order->order_date ?></div>
                                         <div class="subOrderDetails borderSubOrder">
                                            <div class="ordContent">
                                              
                                              <div class="order_item" style="padding-top: 25px;margin-bottom:  -85px;">
                                                <span class="subOrdImage">
                                                <a href="#" v="p" ispartialsearch="" categoryid="12" class="subOrderImage" pogid="" pos=";" hidomntrack="">
                                                <img alt="" title="" class="gridViewImage" anim="true" border="0" src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>">
                                                </a>
                                                </span>
                                                <span class="softBundleParent">
                                                    <a class="subOrdName" href="<?php echo base_url($this->session->group_level.'/product_detail/'.$watch_option->watch_id); ?>" pogid="634407752835" orderid="7990058275">
                                                       <div><span style="font-size: 28px !important"><?php echo $product_data->product_name ?> x <?php echo $order->quantity ?></span> <u class="pull-right">$<?php echo number_format($watch_option->retail_price*$order->quantity,2) ?> ($<?php echo $watch_option->retail_price ?> X  <?php echo $order->quantity ?>)</u></div>
                                                    </a>
                                                    <!-- For the O2O pickup flow -->
                                                    <div class="pinnacle-product-attrs">
                                                       <span class="pinnacle-product-attr">
                                                       <span class="strong pinn-attr-key">Color:</span>
                                                       <span class="pinn-attr-value"><?php echo $watch_option->dial_color ?></span>
                                                       </span>
                                                    </div>
                                                    <div class="pinnacle-product-attrs">
                                                       <span class="pinnacle-product-attr">
                                                       <span class="strong pinn-attr-key">Series:</span>
                                                       <span class="pinn-attr-value"><?php echo $series_data ?></span>
                                                       </span>
                                                    </div>
                                                    <div class="pinnacle-product-attrs">
                                                       <span class="pinnacle-product-attr">
                                                       <span class="strong pinn-attr-key">Category:</span>
                                                       <span class="pinn-attr-value"><?php echo $category_data ?></span>
                                                       </span>
                                                    </div>
                                                    <!-- For the O2O pickup flow -->
                                                    <span class="subOrdContent">
                                                    </span>
                                                </span>
                                              </div>
                                <?php }else{ ?>
                                      <div class="order_item" style="border-top: 2px dotted #dfdfdf;padding-top: 25px;margin-bottom:  -85px;">
                                                <span class="subOrdImage" pogid="634407752835" orderid="7990058275">
                                                <a href="#" v="p" ispartialsearch="" categoryid="12" class="subOrderImage" pogid="" pos=";" hidomntrack="">
                                                <img alt="" title="" class="gridViewImage" anim="true" border="0" src="<?php echo UPLOAD_URL.'products/'.$product_data->product_number.'/'.$watch_option->dial_color.'/'.$watch_option->main_image; ?>">
                                                </a>
                                                </span>
                                                <span class="softBundleParent">
                                                    <a class="subOrdName" href="https://www.snapdeal.com/product/micromax-canvas-spark/634407752835" pogid="634407752835" orderid="7990058275">
                                                       <div><?php echo $product_data->product_name ?> x <?php echo $order->quantity ?> <u class="pull-right">$<?php echo number_format($watch_option->retail_price*$order->quantity,2) ?> ($<?php echo $watch_option->retail_price ?> x <?php echo $order->quantity ?>)</u></div>
                                                    </a>
                                                    <!-- For the O2O pickup flow -->
                                                    <div class="pinnacle-product-attrs">
                                                       <span class="pinnacle-product-attr">
                                                       <span class="pinn-attr-key">Color:</span>
                                                       <span class="pinn-attr-value"><?php echo $watch_option->dial_color ?></span>
                                                       </span>
                                                    </div>
                                                    <div class="pinnacle-product-attrs">
                                                       <span class="pinnacle-product-attr">
                                                       <span class="pinn-attr-key">Series:</span>
                                                       <span class="pinn-attr-value"><?php echo $series_data ?></span>
                                                       </span>
                                                    </div>
                                                    <div class="pinnacle-product-attrs">
                                                       <span class="pinnacle-product-attr">
                                                       <span class="pinn-attr-key">Category:</span>
                                                       <span class="pinn-attr-value"><?php echo $category_data ?></span>
                                                       </span>
                                                    </div>
                                                    <!-- For the O2O pickup flow -->
                                                    <span class="subOrdContent">
                                                    </span>
                                                </span>
                                      </div>
                                <?php } ?>
                            <?php } ?>
                </div>
            </div>
        </div>
    </main>
</div>
    <!--main-->
      <script>
$(document).ready(function(){
    $(".has-menu").click(function(){
        $(this).toggleClass('has-menu');
    });
});
</script>