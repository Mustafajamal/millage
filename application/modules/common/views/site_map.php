<style type="text/css">
.sec-title{color:#b13434}
li.site_map a{color:black !important}  
</style>
  <div class="container primary-padding">      

<div class="row">
<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
	<h5 class="sec-title">Timepieces:</h5>		
      <ul class="shop-sidebar clearfix">
      	<?php
      	foreach ($series_data as $sd) 
      	{
      	?>
      		<li class="site_map"><a href="<?php echo base_url($this->session->group_level."/product_list?series=".$sd->series_id);?>"><?php echo $sd->series_name;?></a></li>
      	<?php
      	}
      	?>
  	  </ul>
</div>
<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
<?php foreach ($page_categories as $value) { ?>
    <h5 class="sec-title"><?php echo $value->page_category_name; ?></h5>
    <?php 
        $sections=$this->custom->getRows("cms_page_category_section",array("page_category_id"=>$value->page_category_id,"status"=>1));
              foreach ($sections as $one_section) { 
          ?>
          <ul class="shop-sidebar clearfix">
              <li class="site_map">
              <a href="<?php echo base_url($this->session->group_level.'/page/'.$value->page_category_id.'/'.$one_section->section_name); ?>"><?php echo $one_section->section_name ?> <span class="no"></span></a>
          </li>
      </ul>
  <?php }
      }
   ?>   
</div>
<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
  <h5 class="sec-title">Contacting Us:</h5>
      <ul class="shop-sidebar clearfix">
          <li class="site_map"><a href="<?php echo base_url($this->session->group_level."/contactus");?>">General Inquiries</a></li>
          <li class="site_map"><a href="<?php echo base_url($this->session->group_level."/contactus");?>">Feedback / Suggestions</a></li>
          <li class="site_map"><a href="<?php echo base_url($this->session->group_level."/contactus");?>">Request a Catalog</a></li>
      </ul>

<h5 class="sec-title">Dealer Information:</h5>
      <ul class="shop-sidebar clearfix">
          <li class="site_map"><a href="<?php echo base_url($this->session->group_level."/apply/distributorship");?>">Distributorship Application</a></li>
          <li class="site_map"><a href="<?php echo base_url($this->session->group_level."/apply/distributorship/usa");?>">USA Distributorship Application</a></li>
          <li class="site_map"><a href="<?php echo base_url($this->session->group_level."/apply/dealership");?>">Dealer Application</a></li>
          <li class="site_map"><a href="<?php echo base_url($this->session->group_level."/apply/dealership/usa");?>">USA Dealer Application</a></li>
      </ul>
</div>
</div>

</div>
