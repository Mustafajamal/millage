<main class="main primary-padding">
  <div class="container" id="back">
    <div class="col-md-10 col-md-offset-1">
      <?php 
        $attributes = array('class' => 'form-horizontal validate', 'id' => 'contactus_form','method'=>"post");
        echo form_open('#',$attributes);
        ?>
      <!-- Form Name -->
      <legend><u>Contact Us</u></legend>
      <?php
          $data=array("name"=>"subject","id"=>"subject","class"=>"form-control");
          $options=array("General Inquiry"=>"General Inquiry","Feedback / Suggestions"=>"Feedback / Suggestions","Request a Catalog"=>"Request a Catalog");
          $selected="General Inquiry";
          echo form_dropdown($data,$options,$selected,"Subject");
          /**/
          $YourName=array("name"=>"name","id"=>"YourName","class"=>"form-control","required"=>"required");
          echo form_input($YourName,"","Your Name :");
          $YourEmail=array("name"=>"email","id"=>"YourEmail","class"=>"form-control","required"=>"required");
          echo form_email($YourEmail,"","Your Email :");
          $Telephone=array("name"=>"telephone","id"=>"Telephone","class"=>"form-control","required"=>"required");
          echo form_input($Telephone,"","Telephone :");
          $Address=array("name"=>"address","id"=>"Address","class"=>"form-control","required"=>"required");
          echo form_input($Address,"","Address :");
          $City=array("name"=>"city","id"=>"City","class"=>"form-control","required"=>"required");
          echo form_input($City,"","City :");
          $State=array("name"=>"state","id"=>"State","class"=>"form-control","required"=>"required");
          echo form_input($State,"","State :");
          $PostalCode=array("name"=>"postal_code","id"=>"PostalCode","class"=>"form-control","required"=>"required");
          echo form_input($PostalCode,"","Postal Code :");
          $Country=array("name"=>"country","id"=>"Country","class"=>"form-control","required"=>"required");
          echo form_input($Country,"","Country :");
          $Message=array("name"=>"message","id"=>"Message","class"=>"form-control","required"=>"required","rows"=>"8","cols"=>"20");
          echo form_textarea($Message,"","Message :");
        ?>
      <input type="submit" value="Submit" id="contactus_btn" class="btn btn-primary">
      <?php echo form_close(); ?>
    </div>
    <div class="col-md-2">
    </div>
  </div>


<script src="<?php echo JS_URL;?>jquery.form.min.js"></script>
<script type="text/javascript">
      $(function() {
          // var message="<?php //echo $this->session->flashdata('message'); ?>";
          // if (message!="") {
          //   toastr["info"](message,"Message");
          // }
          var options = { 
            url : "<?php echo base_url('common/ajax_controller/contactus_process') ?>",
            beforeSubmit: function() {
              $("#contactus_btn").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>").attr('disabled', true);
            },
            clearForm:true,
            resetForm:true,
            success:function (data) {
                $("#contactus_btn").html('<span>Update Settings</span>').attr('disabled', false);
                data=$.trim(data);
                
                if (data=="success") {
                    toastr["success"]("Contact Request Sent Successfully","Message");
                }
                else{
                  toastr["error"]("Try Again","Message");
                }
            },
          }
          $("#contactus_btn").click(function(event) {
            if ($('#contactus_form').valid()) {
              $('#contactus_form').ajaxForm(options); 
            }
          });
        });

        //$("#contactus_form").ajaxForm();
</script>