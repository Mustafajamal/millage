<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<title>Millage</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">

<!-- favicon -->
<link rel="shortcut icon" type="image/png" href="<?php echo IMG_URL; ?>fav.jpg" />
<!-- GOOGLE FONT -->
<link href="https://fonts.googleapis.com/css?family=Arimo:400,400i,700,700i%7CMr+Dafoe%7CPoppins:300,400,500,600,700" rel="stylesheet">

<!-- CSS LIBRARY -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font-awesome-animation.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>pe-icon-7-stroke.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>menu.css" />

<!-- Main css -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>responsive.css" />

<!-- color skin -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>color-skin/default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>toastr.min.css" />

<script src="<?php echo JS_URL; ?>jquery-3.1.1.min.js"></script> 


<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="single-product row">
	<main class="main primary-padding col-md-8 col-md-offset-2">
	<div id="forgot-pass" class="">
      <div class="content forget-password-block">
        <div class="top mb-35 text-center">
          <h4 class="contact-title">
            <i class="pe-7s-key primary-color"></i>
            Reset Password
          </h4>
          <p>Fill up the form below to Change your password.</p>
        </div>
        <!--top-->
        <?php 
            echo form_open('common/ajax_controller/reset_password/' . $code);
            echo form_input($new_password,'',"Password");
            echo form_input($new_password_confirm,'',"Confirm Password");
            echo form_input($user_id);
            echo form_hidden($csrf);
          ?>
<!--user name-->
            <div class="form-group">
              <button type="submit" id="forgot_form_btn"  class="btn btn-primary btn-block">Change</button>
            </div>
            <!--button-->
          </div>
        </form>
      </div>
    </div>


</body>
</html>