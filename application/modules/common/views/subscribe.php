<main class="main primary-padding">
   <div class="container">
<div class="col-md-3">
</div>
<div class="col-md-6">

<?php 
$attributes = array('class' => 'form-horizontal validate', 'id' => 'setting_form','method'=>"post");
echo form_open('#',$attributes);
?>
<fieldset>
<!-- Form Name -->
<legend>Form Name</legend>
<div class="alert alert-info">
  <strong>Subscribe to our list to receive special offers and notifications of new product releases from Millage. You can easily Un-subscribe from this list at any time.
	Use this form to add your address to our e-mail list.</strong>
</div>
<!-- Select Basic -->
<div class="form-group">
  <label class="col-sm-4 control-label" for="EmailAddress">Your Email Address :</label>
  <div class="col-sm-6">
  	<?php
  	$EmailAddress=array("name"=>"EmailAddress","id"=>"EmailAddress","class"=>"form-control","required"=>"required");
    echo form_email($EmailAddress);
  	?>
  </div>
</div>
<!-- Button -->
<div class="form-group">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Button</button>
  </div>
</div>
</fieldset>
</div>
<div class="col-md-3">
	</div>
</div>
