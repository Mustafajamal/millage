  <main class="main primary-padding">
   <div class="container">
<div class="col-md-1">
</div>
<div class="col-md-10">
  <?php 
$attributes = array('class' => 'form-horizontal validate', 'id' => 'setting_form','method'=>"post");
echo form_open('#',$attributes);
?>

<!-- Form Name -->
<legend><u>United States Dealer Application</u></legend>

   <?php
          $millage_account_no=array("name"=>"millage_account_no","id"=>"millage_account_no","class"=>"form-control","placeholder"=>"Millage Account No");
          echo form_number($millage_account_no,'','Millage Account No');

    ?>
      <span>Please leave blank if you do not already have an account. </span>
    <?php
     
          $corporation=array("name"=>"corporation","id"=>"textinput","class"=>"form-control input-md","required"=>"required","placeholder"=>"Corporation or Owner");
          echo form_input($corporation,"","Corporation or Owner");

          $dba_name=array("name"=>"dba_name","id"=>"dba_name","class"=>"form-control input-md","required"=>"required","placeholder"=>"DBA Name");
          echo form_input($dba_name,"","DBA Name");
  
          $federal_tax_id=array("name"=>"federal_tax_id","id"=>"federal_tax_id","class"=>"form-control input-md","placeholder"=>"Federal Tax ID");
          echo form_input($federal_tax_id,"","Federal Tax ID:");

          $contact_person=array("name"=>"contact_person","id"=>"contact_person","class"=>"form-control input-md","required"=>"required","placeholder"=>"Contact Person : ");
          echo form_input($contact_person,"","Contact Person");

          $contact_person_position=array("name"=>"contact_person_position","id"=>"contact_person_position","class"=>"form-control input-md","required"=>"required","placeholder"=>"Contact Person's Position:");
          echo form_input($contact_person_position,"","Contact Person's Position");

          $business_start_date=array("name"=>"business_start_date","id"=>"business_start_date","class"=>"form-control input-md","required"=>"required","placeholder"=>"Business start date");
          echo form_input($business_start_date,"","Business start date");
     ?>

<!-- Form Name -->
<legend><u>Business Address</u></legend>

<div class="row">

<div class="col-md-2"></div>

<div class="col-md-7">
<?php
        $address=array("name"=>"address","id"=>"address","class"=>"form-control","required"=>"required");      
        echo form_textarea($address,'',"Address : ");

        $state=array("name"=>"state","id"=>"state","class"=>"form-control input-md","required"=>"required");
        echo form_input($state,"","State");
       
        $state=array("name"=>"country","id"=>"country","class"=>"form-control input-md","required"=>"required");
        // echo $country_array; 
?>

<div class="row">
  <div class="col-md-5">
<?php
    $city=array("name"=>"city","id"=>"city","class"=>"form-control input-md","required"=>"required","placeholder"=>"City : ");
          echo form_input($city,"","City");
?>
</div>
   <div class="col-md-1"></div>
  <div class="col-md-5">
<?php
          $zipcode=array("name"=>"zipcode","id"=>"zipcode","class"=>"form-control input-md","required"=>"required","placeholder"=>"ZipCode : ");
          echo form_input($zipcode,"","ZipCode : ");
          ?>
    </div>
  </div>

  <?php
  $at_this_location_since=array("name"=>"at_this_location_since","id"=>"at_this_location_since","class"=>"form-control input-md","required"=>"required","placeholder"=>"At This Location Since : ");
          echo form_input($at_this_location_since,"","At This Location Since : ");
          ?>


</div>

<div class="col-md-3"></div>
</div>

<legend><u>Communication Data</u></legend>

<div class="row">
  <div class="col-md-5">
    <?php
      $telephone=array("name"=>"telephone","id"=>"telephone","class"=>"form-control","required"=>"required");      
        echo form_number($telephone,'',"Telephone ");
    ?>
  </div>
    <div class="col-md-1"></div>
  <div class="col-md-5">
    <?php
        $fax=array("name"=>"fax","id"=>"fax","class"=>"form-control");      
        echo form_input($fax,'',"Fax");
    ?>
  </div>
</div>

<div class="row">
  <div class="col-md-5">
    <?php
        $email=array("name"=>"email","class"=>"form-control","required"=>"required");      
        echo form_email($email,'',"Email");
    ?>
  </div>
    <div class="col-md-1"></div>
  <div class="col-md-5">
    <?php
            $website=array("name"=>"website","id"=>"website","class"=>"form-control");      
        echo form_input($website,'',"Website");
    ?>
  </div>
</div>

<legend><u>Business Profile Data</u></legend>

<u>Business Location Type:</u><br>

<input name="dealer_location_list[]" value="destination_store" type="checkbox"> Destination Store<br>
<input name="dealer_location_list[]" value="website" type="checkbox"> Web Site <br>
<input name="dealer_location_list[]" value="kiosk" type="checkbox"> Kiosk <br>
<input name="dealer_location_list[]" value="mall_location" type="checkbox"> Mall Location <br>
<input name="dealer_location_list[]" value="shopping_district" type="checkbox"> Shopping District <br>
<input name="dealer_location_list[]" value="swap_outdoor_market" type="checkbox"> Swap / Outdoor Market <br>

<u>Type of retail:</u><br>

<input name="type_of_retail[]" value="fashion_boutique" type="checkbox"> Fashion Boutique<br>
<input name="type_of_retail[]" value="accessories" type="checkbox"> Accessories <br>
<input name="type_of_retail[]" value="sporting_goods" type="checkbox">  Sporting Goods <br>
<input name="type_of_retail[]" value="jewelry_store" type="checkbox"> Jewelry Store <br>
<input name="type_of_retail[]" value="general_gifts" type="checkbox"> General Gifts <br>
<input name="type_of_retail[]" value="discount_value_store" type="checkbox"> Discount Value Store <br>
<input name="type_of_retail[]" value="watch_store" type="checkbox"> Watch Store <br>

<u>Target customers:</u><br>
      
<input name="target_customers[]" value="male" type="checkbox"> Male <br>
<input name="target_customers[]" value="female" type="checkbox"> Female <br>
<input name="target_customers[]" value="unisex" type="checkbox"> Unisex <br>

<u>Customer age range:</u><br>

<input name="customer_age_range[]" value="0_17" type="checkbox">  0 - 17 <br>
<input name="customer_age_range[]" value="18_23" type="checkbox"> 18 - 23 <br>
<input name="customer_age_range[]" value="24_30" type="checkbox"> 24 - 30 <br>
<input name="customer_age_range[]" value="31_40" type="checkbox"> 31 - 40 <br>
<input name="customer_age_range[]" value="41_55" type="checkbox"> 41 - 55 <br>
<input name="customer_age_range[]" value="55" type="checkbox"> 55+ <br><br>

<input type="submit" value="Submit" id="save_dealership" class="btn btn-primary">

<?php echo form_close(); ?>
</div>
<div class="col-md-2">
  </div>
</div>


<script type="text/javascript" src="<?php echo JS_URL.'bootstrap-datepicker.min.js'; ?>"></script>

<script type="text/javascript">
  $('#business_start_date').datepicker({
    endDate:'-1d',
    format:'yyyy-mm-dd',
  });
  $('#at_this_location_since').datepicker({
    endDate: '-1d',
    format:'yyyy-mm-dd',
  });
</script> 
<script type="text/javascript">
      $(function() {
        var options = { 
            url : "<?php echo base_url('common/dealership_process') ?>",
            beforeSubmit: function() {
             // $("#save_dealership").html("<i class='fa fa-spin fa-refresh fa-pulse'></i>").attr('disabled', true);

            },
            success:function (data) {
                //$("#contactus_btn").html('<span>Update Settings</span>').attr('disabled', false);
                
                data=$.trim(data);
                
                if (data=="success") {
                    toastr["success"]("Add Successfully","Message");
                    window.location="<?php //echo base_url();?>"
                }
                else{
                  toastr["error"](data,"Message");

                }
            },
          }
          $("#save_dealership").click(function(event) {
            if ($('#setting_form').valid()) {
              $('#setting_form').ajaxForm(options); 
            }
          });
        
      });
    </script>