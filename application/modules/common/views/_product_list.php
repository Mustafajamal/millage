<div class="breadcrumbs">
    <div class="container">
        <ul class="items">
                <li class="item home">
                    <a href="<?php echo base_url() ?>" title="Go to Home Page">Home</a>
                </li>
                <?php if(!is_null($this->uri->segment(3))){ ?>
                <li class="item category6">
                    <a href="<?php echo base_url('common/product_list/'.$this->uri->segment(3)) ?>" title=""><?php echo $category_name=$this->custom->getSingleValue("categories_master","category_name",array("category_id"=>$this->uri->segment(3))) ?></a>
                </li>
                <?php }else{$category_name="";} ?>
                <?php if(!is_null($this->uri->segment(4))){ ?>
                <li class="item category9">
                    <strong><?php echo $style_name=$this->custom->getSingleValue("style_master","style_name",array("style_id"=>$this->uri->segment(4))) ?></strong>
                </li>
                <?php }else{$style_name="";} ?>
        </ul>
    </div>
</div>
    <!--main banner-->
    <main class="main primary-padding">
        <div class="product-list">
            <div class="container">
                <h2 class="text-center"><?php echo $category_name." > ".$style_name; ?></h2>
                <div class="row">                
                    <div class="col-md-9 col-sm-9 col-xs-12 pull-right">
                        <div class="storefront-sorting">
                            <form class="woocommerce-ordering" method="get">
                                <span class="">Sort:</span>
                                <select name="orderby" class="orderby">
                                    <option value="menu_order" selected="selected">Default sorting</option>
                                    <!-- <option value="popularity">Sort by popularity</option>
                                    <option value="rating">Sort by average rating</option> -->
                                    <option value="date">Sort by newness</option>
                                    <option value="price">Sort by price: low to high</option>
                                    <option value="price-desc">Sort by price: high to low</option>
                                </select> 
                                <span class="glyphicon glyphicon-chevron-down select-dropdown"></span>
                            </form>
                            <!--form-->
                            <!--short by-->
                           <!--  <div class="woocommerce-result-count">
                               Items 1-9 of 10
                            </div> -->
                            <!--result-->
                            <!-- <nav class="woocommerce-pagination">
                                <ul class="page-numbers">
                                    <li><a class="prev page-numbers" href="#">&lt;</a></li>
                                    <li><span class="page-numbers current">1</span></li>
                                    <li><a class="page-numbers" href="#">2</a></li>
                                    <li><a class="page-numbers" href="#">3</a></li>
                                    <li><a class="next page-numbers" href="#">&gt;</a></li>
                                </ul>
                            </nav> -->
                             <!-- <div class="short-by pull-right">
                                <a href="07-rab-shop-left-sidebar-list.html" class="btn btn-default bdr">
                                    <i class="fa fa-list-ul"></i>
                                </a>
                                <a href="05-rab-shop-left-sidebar-grid.html" class="btn btn-default bdr active">
                                    <i class="fa fa-th-large"></i>
                                </a>
                            </div> -->
                        </div>
                        <!--storefront-sorting-->
                        <ul class="products clearfix">
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp1.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies black formal jacket</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp2.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies white Tee’s</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product last">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp3.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Gents stripe griped Tee’s</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp4.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies full body gown</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp5.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies black formal jacket</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product last">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp6.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies white Tee’s</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp7.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Gents stripe griped Tee’s</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp8.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies full body gown</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product last">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp9.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies black formal jacket</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp10.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies white Tee’s</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp11.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Gents stripe griped Tee’s</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product last">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp12.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies full body gown</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp1.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies black formal jacket</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp2.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Ladies white Tee’s</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product-->
                            <li class="product last">
                                <figure>
                                    <div class="icons">
                                        <a href="#" class="btn"><i class="pe-7s-cart"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-like"></i></a>
                                        <a href="#" class="btn"><i class="pe-7s-edit"></i></a>
                                        <a href="#" class="btn" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-look"></i></a>
                                    </div>
                                    <!--icons-->
                                    <div class="product-wrap base-align">
                                        <a href="09-rab-shop-product-single.html">&nbsp;</a>
                                        <img src="<?php echo IMG_URL; ?>fashion/fp3.png" alt="Product">
                                    </div>
                                </figure>
                                <!--figure-->
                                <div class="content">
                                    <h6><a href="#">Gents stripe griped Tee’s</a></h6>
                                    <div class="bottom">
                                        <div class="price">
                                            <del>$189</del>
                                            <ins>$170</ins>
                                        </div>
                                        <!--price-->
                                        <span class="star-rating">
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star"></i> 
                                           <i class="fa fa-star-half-empty "></i> 
                                        </span>
                                        <!--star-->
                                    </div>
                                </div>
                            </li>
                            <!--single product--> 
                        </ul>
                        <!--product list-->
                        <nav class="woocommerce-pagination text-center">
                            <ul class="page-numbers">
                                <li><a class="prev page-numbers" href="#">&lt;</a></li>
                                <li><span class="page-numbers current">1</span></li>
                                <li><a class="page-numbers" href="#">2</a></li>
                                <li><a class="page-numbers" href="#">3</a></li>
                                <li><a class="next page-numbers" href="#">&gt;</a></li>
                            </ul>
                        </nav>
                        <!--pegination-->
                    </div>
                    <!--left-->
                    <div class="col-md-3 col-sm-3 col-xs-12 sidebar shop-sidebar left-block">
                        <h2 class="text-center">Shop By</h2>
                        <h5 class="text-center">Shopping Options</h5>
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Brand  <span class="glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                                <li>
                                    <a href="#">Clothes <span class="no">[10]</span></a>
                                </li>
                                <li>
                                    <a href="#">Shoes <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Accessories <span class="no">[09]</span></a>
                                </li>
                                <li>
                                    <a href="#">Clothes <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Shoes <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Accessories <span class="no">[23]</span></a>
                                </li>
                                <li>
                                    <a href="#">Nasty Gal <span class="no">[87]</span></a>
                                </li>
                                <li>
                                    <a href="#">Vintage <span class="no">[93]</span></a>
                                </li>
                                <li>
                                    <a href="#">Sale <span class="no">[10]</span></a>
                                </li>
                                <li>
                                    <a href="#">Lookbooks <span class="no">[03]</span></a>
                                </li>
                                <li>
                                    <a href="#">Features <span class="no">[65]</span></a>
                                </li>
                            </ul>
                        </div>
                        <!--category-->
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Case material <span class="glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                                <li>
                                    <a href="#">Xtra-Small <span class="no">[10]</span></a>
                                </li>
                                <li>
                                    <a href="#">Medium <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Large <span class="no">[09]</span></a>
                                </li>
                                <li>
                                    <a href="#">Xtra-Large <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">XXL <span class="no">[19]</span></a>
                                </li>
                            </ul>
                        </div>
                        <!--size-->
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Display <span class="glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                                <li>
                                    <a href="#">Black <span class="no">[10]</span></a>
                                </li>
                                <li>
                                    <a href="#">Green <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Sky-Blue <span class="no">[09]</span></a>
                                </li>
                                <li>
                                    <a href="#">Purple <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Navy Blue <span class="no">[19]</span></a>
                                </li>
                            </ul>
                        </div>
                        <!--color-->
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Gender <span class="glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                                <li>
                                    <a href="#">Xtra-Small <span class="no">[10]</span></a>
                                </li>
                                <li>
                                    <a href="#">Medium <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Large <span class="no">[09]</span></a>
                                </li>
                                <li>
                                    <a href="#">Xtra-Large <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">XXL <span class="no">[19]</span></a>
                                </li>
                            </ul>
                        </div>
                        <!--size-->
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Manufacturer <span class="glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                                <li>
                                    <a href="#">Black <span class="no">[10]</span></a>
                                </li>
                                <li>
                                    <a href="#">Green <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Sky-Blue <span class="no">[09]</span></a>
                                </li>
                                <li>
                                    <a href="#">Purple <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Navy Blue <span class="no">[19]</span></a>
                                </li>
                            </ul>
                        </div>
                        <!--color-->
                        <div class="box mb-30 has-menu">
                            <h5 class="sec-title">Movement  <span class="pull-right drop-d glyphicon glyphicon-chevron-down"></span></h5>
                            <ul class="shop-sidebar">
                                <li>
                                    <a href="#">Black <span class="no">[10]</span></a>
                                </li>
                                <li>
                                    <a href="#">Green <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Sky-Blue <span class="no">[09]</span></a>
                                </li>
                                <li>
                                    <a href="#">Purple <span class="no">[19]</span></a>
                                </li>
                                <li>
                                    <a href="#">Navy Blue <span class="no">[19]</span></a>
                                </li>
                            </ul>
                        </div>
                        <!--color-->
                        <div class="box mb-30">
                            <h5 class="sec-title">Price</h5>
                             <form action="#">
                                <div id="slider-range"></div>
                                <input type="hidden" id="amount1">
                                <input type="hidden" id="amount2">
                                <div id="amount" class="mb-45"></div>
                                <button type="submit" class=" btn btn-default filter-btn faa-parent animated-hover">filter now <i class="fa fa-long-arrow-right faa-passing"></i></button>
                             </form>
                        </div>
                        <div class="box mb-30 text-center">
                            <h4 class="sec-title">Compare Products</h4>
                            <hr/>
                            <span class="compare">You have no items to compare. </span>
                        </div>
                        <div class="box mb-30 text-center">
                            <h4 class="sec-title">My Wish List</h4>
                            <hr/>
                            <span class="compare">You have no items to wish list. </span>
                        </div>
                        <!--color-->
                        <!--side bar banner-->
                    </div>
                    <!--right-->
                </div>
            </div>
        </div>
    </main>
    <!--main-->
      <script>
$(document).ready(function(){
    $(".has-menu").click(function(){
        $(this).toggleClass('has-menu');
    });
});
</script>