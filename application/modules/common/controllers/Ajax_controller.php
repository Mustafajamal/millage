<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/***********************************************************************************************************************************************************************************************************************************************
 | | | | | | | | | | | | | | | | | ONLY FOR ION AUTH MANAGMENT | | | | | | | | | | | | | | | | |
************************************************************************************************
************************************************************************************************/
class Ajax_controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function login_check(){
		$post=$this->input->post();
		if (isset($post['login_captcha'])) {
			if ($this->rpHash($post['login_captcha']) == $post['login_captchaHash']) {
				unset($post['login_captcha']);
				unset($post['login_captchaHash']);
				$result=$this->ion_auth->login($post['username'],$post['password']);
				if($result){
					$user_data=$this->ion_auth->get_users_groups()->result()[0];
					$user=$this->ion_auth->user()->result()[0];
					$this->session->set_userdata('group_id',$user_data->id);
					$this->session->set_userdata('group_level',trim($user_data->name));
					$this->session->set_userdata('username',$user->username);
					$this->session->set_userdata('login_from',"google");
					echo $user_data->name;
				}
				else{
					echo $this->ion_auth->messages().' '.$this->ion_auth->errors();
				}
			}else{
				echo "Verify Recaptcha";
			}
		}else{
			echo "Verify Recaptcha";
		}
	}

	public function register(){
		$post=$this->input->post();
		if (isset($post['reg_captcha'])) {
			if ($this->rpHash($post['reg_captcha']) == $post['reg_captchaHash']) {
				unset($post['reg_captcha']);
				unset($post['reg_captchaHash']);
				$groups=$this->ion_auth->groups()->result();
				foreach ($groups as $value) {
					if ("customer"==trim($value->name)) {
						$group_id=$value->id;
					}
				}
				$password=$post['password'];
				$username=$post['username'];
				$email=$post['email'];
				unset($post['username']);
				unset($post['email']);
				unset($post['password']);
				unset($post['c-password']);
				unset($post['terms']);
				unset($post['g-recaptcha-response']);
				if (!$this->ion_auth->username_check($username) && !$this->ion_auth->email_check($email)) {
					if($this->ion_auth->register($username,$password,$email,$post,array($group_id))){
						$this->session->set_flashdata('message',$this->ion_auth->messages().' '.$this->ion_auth->errors());
						$this->session->set_flashdata('message-success', $this->ion_auth->messages().' '.$this->ion_auth->errors());
						echo json_encode(array("status"=>"success","message"=>"success"));
					}
					else{
						$this->session->set_flashdata('message-error', $this->ion_auth->messages().' '.$this->ion_auth->errors());
						echo json_encode(array("status"=>"error","message"=>$this->ion_auth->messages().' '.$this->ion_auth->errors()));
					}
				}
				else{
					$this->session->set_flashdata('message-error', "Username/Email Already Registered");
					echo json_encode(array("status"=>"error","message"=>"Username/Email Already Registered"));
				}
			}else{
				echo "Verify Recaptcha";
			}
		}else{
			echo "Verify Recaptcha";
		}
	}

	// activate the user
	public function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message-success', $this->ion_auth->messages());
			redirect("", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message-error', $this->ion_auth->errors());
			redirect("", 'refresh');
		}
	}

	public function forgot_password_recover(){
		$post=$this->input->post();
		if (isset($post['forgot_captcha'])) {
			if ($this->rpHash($post['forgot_captcha']) == $post['forgot_captchaHash']) {
				unset($post['forgot_captcha']);
				unset($post['forgot_captchaHash']);
				$email=$this->input->post('email');
				if ($email!="") {
					$identity=$email;
					//$identity=$this->ion_auth->get_user_by_email($email)->result()[0]->username;
					if (!$this->ion_auth->email_check($email)){
						echo json_encode(array("status"=>"error","message"=>"Email Not Registered"));
					}
					else{
						$forgotten = $this->ion_auth->forgotten_password($identity);
						if ($forgotten) { //if there were no errors
							$this->session->set_flashdata('message-success',$this->ion_auth->messages());
							echo json_encode(array("status"=>"success","message"=>"success"));
						}
						else {
							$this->session->set_flashdata('message-error',$this->ion_auth->messages());
							echo json_encode(array("status"=>"error","message"=>$this->ion_auth->messages().' '.$this->ion_auth->errors()));
						}
					}
				}
				else{
					$this->session->set_flashdata('message-error',"Email is required");
					echo json_encode(array("status"=>"error","message"=>"Email is required"));
				}
			}else{
				echo "Verify Recaptcha";
			}
		}else{
			echo "Verify Recaptcha";
		}
	}

	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'class' => 'form-control input-lg',
					'placeholder'=>"New Password (Minimum ".$this->data['min_password_length']." Character Length  )",
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name'    => 'new_confirm',
					'id'      => 'new_confirm',
					'class' => 'form-control input-lg',
					'placeholder'=>"Confirm New Password",
					'type'    => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				// $this->load->view('common/header');
				$this->load->view('common/reset_password', $this->data);
				// $this->load->view('common/footer');
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message-success', $this->ion_auth->messages());
						redirect("", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message-error', $this->ion_auth->errors());
						redirect('common/ajax_controller/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message-error', $this->ion_auth->errors());
			redirect("", 'refresh');
		}
	}

	public function rpHash($value) { 
	    $hash = 5381; 
	    $value = strtoupper($value); 
	    for($i = 0; $i < strlen($value); $i++) { 
	        $hash = ($this->leftShift32($hash, 5) + $hash) + ord(substr($value, $i)); 
	    } 
	    return $hash; 
	} 
	 
	// Perform a 32bit left shift 
	public function leftShift32($number, $steps) { 
	    // convert to binary (string) 
	    $binary = decbin($number); 
	    // left-pad with 0's if necessary 
	    $binary = str_pad($binary, 32, "0", STR_PAD_LEFT); 
	    // left shift manually 
	    $binary = $binary.str_repeat("0", $steps); 
	    // get the last 32 bits 
	    $binary = substr($binary, strlen($binary) - 32); 
	    // if it's a positive number return it 
	    // otherwise return the 2's complement 
	    return ($binary{0} == "0" ? bindec($binary) : 
	        -(pow(2, 31) - bindec(substr($binary, 1)))); 
	}


	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	public function _valid_csrf_nonce()
	{
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function subscriber_process()
	{
		$post=$this->input->post();

		$already_subscribed=$this->custom->getSingleRow("subscriber",array("subscriber_email"=>$post['subscriber_email']));
		if(is_null($already_subscribed))
		{
			$post['date']=date("Y-m-d h:i:s");
			$post['status']=1;
			$id=$this->custom->insertRow('subscriber',$post);
			if($id!=0)
			{
				echo "success";
			}
			else
			{
				echo "fail";
			}
		}
		else
		{
			if($already_subscribed->status==0)
			{
				$id=$this->custom->updateRow('subscriber',array('status'=>1),array('subscriber_email'=>$post['subscriber_email']));
				$result=$this->db->affected_rows();
				if($result>0)
				{
					echo "success";
				}
				else
				{
					echo "fail";
					
				}
			}
			else
			{
				echo "Already Subscribed";
			}
		}
	}

	public function unsubscriber_process()
	{
		$post=$this->input->post();

		$subscriber=$this->custom->getSingleValue("subscriber","subscriber_email",array("subscriber_email"=>$post['subscriber_email']));
		if($subscriber==$post['subscriber_email'])
		{
			$res=$this->custom->updateRow('subscriber',array('status'=>0),array('subscriber_email'=>$post['subscriber_email']));
			//echo $this->db->last_query();
			$result=$this->db->affected_rows();
			if($result>0)
			{
				echo "success";	
			}
			else
			{
				echo "fail";
			}
		}
		else
		{
			echo "You have not Subscribed";
			
		}
	}

	public function contactus_process()
	{
		$post=$this->input->post();
		$post['ip_address']=$this->input->ip_address();
		$id=$this->custom->insertRow('contact_us',$post);
		if($id!=0)
		{
			echo "success";
			exit();
		}
		else
		{
			echo "fail";
			exit();
		}

	}

	public function dealership_process()
	{
		$post=$this->input->post();
		 $post['dealer_location_list']=implode(",",$post['dealer_location_list']);
		 $post['type_of_retail']=implode(",",$post['type_of_retail']);
		 $post['target_customers']=implode(",",$post['target_customers']);
		 $post['customer_age_range']=implode(",",$post['customer_age_range']);
		

		 $id=$this->custom->insertRow('dealership',$post);
		 
		if($id!=0)
		{
			echo "success";
			$this->session->set_flashdata('message-success', 'Your Data Submitted Successfully');
			exit();
		}
		else
		{
			echo "fail";
			exit();
		}
	}

	public function united_states_dealership_process()
	{
		$post=$this->input->post();
		 $post['dealer_location_list']=implode(",",$post['dealer_location_list']);
		 $post['type_of_retail']=implode(",",$post['type_of_retail']);
		 $post['target_customers']=implode(",",$post['target_customers']);
		 $post['customer_age_range']=implode(",",$post['customer_age_range']);
		

		 $id=$this->custom->insertRow('dealership',$post);
		 
		if($id!=0)
		{
			echo "success";
			$this->session->set_flashdata('message-success', 'Your Data Submitted Successfully');
			exit();
		}
		else
		{
			echo "fail";
			exit();
		}
	}
}

/* End of file Ajax_controller.php */
/* Location: ./application/controllers/Ajax_controller.php */