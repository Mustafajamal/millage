<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_customer extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			// $this->table="categories_master";
			$this->logged_id = $this->session->user_id;
			$this->view_path = 'common/ajax/Manage_customer/';
		}
		public function add()
		{
			is_ajax();
			$countries=$this->custom->getRows("countries",array(),array(),NULL,"id,country_name","array");
			foreach ($countries as $value) {
				$countries_array[$value['id']]=$value['country_name'];
			}
			
			$this->data['countries']=$countries_array;
			$this->data['mode'] = "add";
			$this->parser->parse($this->view_path.'add',$this->data);
		}
		public function edit()
		{
			$this->fetchData();
			$this->data['mode'] = "Edit";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}

		public function save()
		{
			$post=$this->input->post();
			
			

			if($post)
			{
				$userdata['username']=$post['username'];
				$userdata['password']=$post['password'];
				$userdata['first_name']=$post['first_name'];
				$userdata['last_name']=$post['last_name'];
				$userdata['email']=$post['email'];
				$userdata['phone']=$post['phone'];
				unset($post['c_password']);
				unset($post['username']);
				unset($post['password']);
				unset($post['first_name']);
				unset($post['last_name']);
				unset($post['email']);
				unset($post['phone']);
				$res=$this->ion_auth->register($userdata['username'],$userdata['password'],$userdata['email'],$userdata,array(4));
				if (is_integer($res)) {
					$post['user_id']=$res;
				
					$re=$this->custom->insertRow("customer_data",$post);
					if(is_integer($re))
					{
						$this->session->set_flashdata('message-success',"Customer Added Successfully");
					}
					else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
					}
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
				

			}
			else
			{
				show_404();
			}
			redirect('admin/manage_customer','refresh');
		}
		public function update()
		{
			$post=$this->input->post();
			if($post)
			{
				$id = $post['id'];
				unset($post['id']);
				$userdata['username']=$post['username'];
				
				$userdata['first_name']=$post['first_name'];
				$userdata['last_name']=$post['last_name'];
				$userdata['email']=$post['email'];
				$userdata['phone']=$post['phone'];
				unset($post['c_password']);
				unset($post['username']);
				
				unset($post['first_name']);
				unset($post['last_name']);
				unset($post['email']);
				unset($post['phone']);
				$result = $this->ion_auth->update($id,$userdata);
				$checkdata=$this->custom->getSingleRow("customer_data",array("user_id"=>$id),'array');
				
				if($checkdata != null)
				{
					$res=$this->custom->updateRow("customer_data",$post,array("user_id"=>$id));
				}
				else
				{
					$post['user_id']=$id;
					$res=$this->custom->insertRow("customer_data",$post);
				}
				
				if($res){
					$this->session->set_flashdata('message-success',"Customer Updated Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
				redirect('admin/manage_customer','refresh');
			} 
			else
			{
				show_404();
			}
		}
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$result = $this->ion_auth->delete_user($id);
			echo $result;
		}

		public function enable()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$result = $this->ion_auth->activate($id);

			$count=$this->db->affected_rows($result);
			if($count>=1)
			{
				echo "Enable";
			}
			else
			{
				echo "Already Enable";
			}
		}

		public function disable()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$result = $this->ion_auth->deactivate($id);

			$count=$this->db->affected_rows($result);
			if($count>=1)
			{
				echo "Disable";
			}
			else
			{
				echo "Already Disable";
			}
		}

		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->ion_auth->user($id)->result_array()[0];
			if($row)
			{
				$this->data['cust_data']=$this->custom->getSingleRow("customer_data",array('user_id'=>$row['id']),'array');
				$countries=$this->custom->getRows("countries",array(),array(),NULL,"id,country_name","array");
				foreach ($countries as $value) {
					$countries_array[$value['id']]=$value['country_name'];
				}
				
				$this->data['countries']=$countries_array;
				$this->data['user_data'] = $row;
			}	
		}

}

/* End of file Manage_admin.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_admin.php */