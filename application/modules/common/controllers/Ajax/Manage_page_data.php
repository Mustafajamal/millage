<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_page_data extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			$this->table="cms_page_category_section";
			$this->logged_id = $this->session->user_id;
			$this->view_path = 'common/ajax/Manage_page_data/';
		}
		public function add()
		{
			is_ajax();
			$this->data['mode'] = "add";
			$this->data['category_data']=$this->custom->createDropdownSelect("cms_page_category",array('page_category_id','page_category_name'));
			$this->parser->parse($this->view_path.'add',$this->data);
		}
		public function edit()
		{
			$this->fetchData();
			$this->data['mode'] = "Edit";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}

		public function save()
		{
			$post=$post=$this->input->post();
			unset($post['file_contents']);
			
			if(!empty($_FILES['file_content']['name'][0])):
	          $upload_data=file_upload(date('YmdHis'),"file_content","cms_files","gif|jpg|png|jpeg|pdf|docx");
	          if ($upload_data['status']) {
	          	$post['file_content']=$upload_data['upload_data']['file_name'];
	          }else{
	          	$this->session->set_flashdata('message-error',$upload_data['error']);
	          	goto REDIRECT;
	          }
	        endif;
			if($post)
			{
				$res=$this->custom->insertRow($this->table,$post);
				if (is_integer($res)) {
					$this->session->set_flashdata('message-success',"Category Added Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
			}
			else
			{
				show_404();
			}
			REDIRECT:
			redirect('admin/manage_page_data','refresh');
		}
		public function update()
		{
			$post=$this->input->post();
			if($post['file_contents']=="content"){
				unset($_FILES);
				$post['file_content']="";
			}
			if($post['file_contents']=="file"){
				$post['page_data']="";
			}
			unset($post['file_contents']);
			if(!empty($_FILES['file_content']['name'][0])):
	          $upload_data=file_upload(date('YmdHis'),"file_content","cms_files","gif|jpg|png|jpeg|pdf|docx");
	          if ($upload_data['status']) {
	          	$post['file_content']=$upload_data['upload_data']['file_name'];
	          }else{
	          	$this->session->set_flashdata('message-error',$upload_data['error']);
	          	goto REDIRECT;
	          }
	        endif;
			if($post)
			{
				$id = $post['section_id'];
				unset($post['section_id']);
				$where = array('section_id'=>$id);
				$result = $this->custom->updateRow($this->table,$post,$where);
				if($result){
					$this->session->set_flashdata('message-success',"Category Updated Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
				REDIRECT:
				redirect('admin/manage_page_data','refresh');
			}
			else
			{
				show_404();
			}
		}
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('section_id' => $id);
			$result = $this->custom->deleteRow($this->table,$where);
			echo $result;
		}
		public function enable()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('section_id' => $id);
			$result = $this->custom->updateRow($this->table,array("status"=>1),$where);

			$count=$this->db->affected_rows($result);
			if($count>=1)
			{
				echo "Enable";
			}
			else
			{
				echo "Already Enable";
			}
		}

		public function disable()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('section_id' => $id);
			$result = $this->custom->updateRow($this->table,array("status"=>0),$where);
			
			$count=$this->db->affected_rows($result);
			if($count>=1)
			{
				echo "Disable";
			}
			else
			{
				echo "Already Disable";
			}
		}
		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->custom->getSingleRow($this->table,array('section_id' => $id),"array");
			if($row)
			{
				$this->data['cat_data'] = $row;
				$this->data['category_data']=$this->custom->createDropdownSelect("cms_page_category",array('page_category_id','page_category_name'),"",array(" "),null,array($row['page_category_id']));
			}	
		}

}

/* End of file Manage_department.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_department.php */