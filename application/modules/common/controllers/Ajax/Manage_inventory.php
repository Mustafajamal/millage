<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_inventory extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			// $this->table="categories_master";
			$this->logged_id = $this->session->user_id;
			$table="watch_options_master";
			$this->view_path = 'common/ajax/Manage_inventory/';
		}
		
		public function edit()
		{
			$this->fetchData();
			$this->data['mode'] = "Edit";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}
		
		public function update()
		{
			$post=$this->input->post();
			
			if($post)
			{
				$id = $post['watch_option_id'];
				$stock=$this->custom->getSingleValue('watch_options_master',"dial_stock",array('watch_option_id'=>$id));
				if($post['process'])
				{
					$update_data['dial_stock']=$stock-$post['quantity'];
				}
				else
				{
					$update_data['dial_stock']=$stock+$post['quantity'];
				}
				$res=$this->custom->updateRow("watch_options_master",$update_data,array('watch_option_id'=>$id));
				
				
				if($res){
					$this->session->set_flashdata('message-success',"Record Updated Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
				redirect('admin/inventory','refresh');
			} 
			else
			{
				show_404();
			}
		}
		
		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->custom->getSingleRow("watch_options_master",array("watch_option_id"=>$id),'array');

			if($row)
			{
				$this->data['watch']=$watch=$this->custom->getSingleRow("watch_master",array("watch_id"=>$row['watch_id']),'array');
				$this->data['series_name']=$this->custom->getSingleValue("series_master","series_name",array("series_id"=>$watch['series_id']));
				$this->data['watch_option_data'] = $row;
			}	
		}

}

/* End of file Manage_admin.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_admin.php */