<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_series extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			$this->table="series_master";
			$this->logged_id = $this->session->user_id;
			$this->view_path = 'common/ajax/Manage_series/';
		}
		public function add()
		{
			is_ajax();
			$this->data['mode'] = "add";
			$this->parser->parse($this->view_path.'add',$this->data);
		}
		public function edit()
		{
			$this->fetchData();
			$this->data['mode'] = "Edit";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}

		public function save()
		{
			$post=$post=$this->input->post();
			if($post)
			{
				$res=$this->custom->insertRow($this->table,$post);
				if (is_integer($res)) {
					$this->session->set_flashdata('message-success',"Category Added Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
			}
			else
			{
				show_404();
			}
			redirect('admin/manage_series','refresh');
		}
		public function update()
		{
			$post=$this->input->post();
			if($post)
			{
				$id = $post['series_id'];
				unset($post['series_id']);
				$where = array('series_id'=>$id);
				$result = $this->custom->updateRow($this->table,$post,$where);
				if($result){
					$this->session->set_flashdata('message-success',"Category Updated Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
				redirect('admin/manage_series','refresh');
			}
			else
			{
				show_404();
			}
		}
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('series_id' => $id);
			$result = $this->custom->deleteRow($this->table,$where);
			echo $result;
		}

		public function active()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('series_id' => $id);
			$result = $this->custom->updateRow($this->table,array("status"=>1),$where);
			$count=$this->db->affected_rows($result);
			
			if($count>=1)
			{
				echo "Active";
			}
		}

		public function inactive()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('series_id' => $id);
			$result = $this->custom->updateRow($this->table,array("status"=>0),$where);
			$count=$this->db->affected_rows($result);
			if($count>=1)
			{
				echo "Inactive";
			}
		}

		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->custom->getSingleRow($this->table,array('series_id' => $id),"array");
			if($row)
			{
				$this->data['series_data'] = $row;
			}	
		}

}

/* End of file Manage_department.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_department.php */