<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_products extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			$this->table="watch_master";
			$this->logged_id = $this->session->user_id;
			$this->view_path = 'common/ajax/Manage_products/';
			$this->data["features"]=array("movement","crystal","case","case_diameter","case_thickness","bezel_material","bezel_function","water_resistant","calendar","bracelet","bracelet_length","bracelet_width","clasp_type");
		}
		public function add()
		{
			is_ajax();
			$this->data['mode'] = "add";
			$this->data['series_data']=$this->custom->createDropdownSelect("series_master",array('series_id','series_name'));
			$this->data['style_data']=$this->custom->createDropdownSelect("style_master",array('style_id','style_name'));
			$this->data['category_data']=$this->custom->createDropdownSelect("categories_master",array('category_id','category_name'));



			$this->parser->parse($this->view_path.'add',$this->data);
		}
		public function edit()
		{
			$this->fetchData();
			$this->data['mode'] = "Edit";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}

		public function save()
		{
			$post=$post=$this->input->post();
			/*$features=$this->data["features"];
			for ($i=0; $i <count($features) ; $i++) { 
				if (!isset($post[$features[$i]])) {
					$post[$features[$i]]="NO";
				}
				else{
					$post[$features[$i]]="YES";	
				}
			}*/
			if($post)
			{

				$res=$this->custom->insertRow($this->table,$post);
				if (is_integer($res)) {
					if (!is_dir(FCPATH.'uploads/products/'.$post['product_number'])) {
						mkdir(FCPATH.'uploads/products/'.$post['product_number']);
					}

					if(!empty($_FILES['file_content']['name'][0]))
					{
						$upload_data=file_upload($post['product_name'],"file_content",'products/'.$post['product_number'],'gif|jpg|png|jpeg|pdf|doc|docx');
						if ($upload_data['status']) {
		        	  		$file['product_catalogue']=$upload_data['upload_data']['file_name'];
				    		$this->custom->updateRow($this->table,$file,array("watch_id"=>$res));
						}else{
							$this->session->set_flashdata('message-error',$upload_data['error']);
						}
				    }

					$this->session->set_flashdata('message-success',"Product Added Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
			}
			else
			{
				show_404();
			}
			redirect('admin/manage_products','refresh');
		}
		public function update()
		{
			$post=$this->input->post();
			
			/*$features=$this->data["features"];
			for ($i=0; $i <count($features) ; $i++) { 
				if (!isset($post[$features[$i]])) {
					$post[$features[$i]]="NO";
				}
				else{
					$post[$features[$i]]="YES";	
				}
			}*/
			if($post)
			{
				$id = $post['watch_id'];
				unset($post['watch_id']);
				$where = array('watch_id'=>$id);
				$folder_name=$this->custom->getSingleValue($this->table,"product_number",$where);

				$check_data=rename(FCPATH.'uploads/products/'.$folder_name,FCPATH.'uploads/products/'.$post['product_number']);
				if ($check_data) {
					$result = $this->custom->updateRow($this->table,$post,$where);
					if($result){
						$this->session->set_flashdata('message-success',"Product Updated Successfully");
					}
					else{
						$this->session->set_flashdata('message-error',"Something Went Wrong");
					}
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
				
				redirect('admin/manage_products','refresh');
			}
			else
			{
				show_404();
			}
		}
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('watch_id' => $id);
			$result = $this->custom->deleteRow($this->table,$where);
			echo $result;
		}

		public function manage_color_or_options()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			?>
			<script>
				var id="<?php echo $id; ?>";
				window.location = "<?php echo base_url(); ?>"+"admin/manage_product_option/"+id;
			</script>
			<?php 
		}
		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->custom->getSingleRow($this->table,array('watch_id' => $id),"array");
			if($row)
			{
				$this->data['product_data'] = $row;
				$this->data['series_data']=$this->custom->createDropdownSelect("series_master",array('series_id','series_name'),"Series",array(' '),array(),array($row['series_id']));
				$this->data['style_data']=$this->custom->createDropdownSelect("style_master",array('style_id','style_name'),"Style",array(' '),array(),array($row['style_id']));
				$this->data['category_data']=$this->custom->createDropdownSelect("categories_master",array('category_id','category_name'),"Category",array(' '),array(),array($row['category_id']));
			}	
		}

}

/* End of file Manage_department.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_department.php */