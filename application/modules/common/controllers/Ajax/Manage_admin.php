<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_admin extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			// $this->table="categories_master";
			$this->logged_id = $this->session->user_id;
			$this->view_path = 'common/ajax/Manage_admin/';
		}
		public function add()
		{
			is_ajax();
			$this->data['mode'] = "add";
			$this->parser->parse($this->view_path.'add',$this->data);
		}
		public function edit()
		{
			$this->fetchData();
			$this->data['mode'] = "Edit";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}

		public function save()
		{
			$post=$this->input->post();
			if($post)
			{
				unset($post['c_password']);
				$res=$this->ion_auth->register($post['username'],$post['password'],$post['email'],$post,array(1));
				if (is_integer($res)) {
					$this->session->set_flashdata('message-success',"Admin Added Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
			}
			else
			{
				show_404();
			}
			redirect('admin/manage_admin','refresh');
		}
		public function update()
		{
			$post=$this->input->post();
			if($post)
			{
				$id = $post['id'];
				unset($post['id']);
				$result = $this->ion_auth->update($id,$post);
				if($result){
					$this->session->set_flashdata('message-success',"Admin Updated Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
				redirect('admin/manage_admin','refresh');
			}
			else
			{
				show_404();
			}
		}
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$result = $this->ion_auth->delete_user($id);
			echo $result;
		}

		public function enable()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$result = $this->ion_auth->activate($id);

			$count=$this->db->affected_rows($result);
			if($count>=1)
			{
				echo "Enable";
			}
		}

		public function disable()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$result = $this->ion_auth->deactivate($id);
			
			$count=$this->db->affected_rows($result);
			if($count>=1)
			{
				echo "Disable";
			}
		}

		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->ion_auth->user($id)->result_array()[0];
			if($row)
			{
				$this->data['user_data'] = $row;
			}	
		}

}

/* End of file Manage_admin.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_admin.php */