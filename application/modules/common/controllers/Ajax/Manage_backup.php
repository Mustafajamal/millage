<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_backup extends CI_Controller {

	
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			 $this->table="backup_master";
			$this->logged_id = $this->session->user_id;
			$this->load->helper('file');
			
		}
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$path=$this->custom->getSingleValue($this->table,"backup_file",array("backup_id"=>$id));
			$result = $this->custom->deleteRow($this->table,array("backup_id"=>$id));
			unlink(FCPATH.'uploads/database_backups/'.$path);
			echo $result;
		}
		public function restore()
		{
			$id=$this->input->post('rowID');
			$path=$this->custom->getSingleValue($this->table,"backup_file",array("backup_id"=>$id));
			$sql_file=FCPATH . 'uploads/database_backups/'.$path;
			
			$query_list = explode(";", read_file($sql_file));
			
			try { 
				$this->db->query("SET FOREIGN_KEY_CHECKS=0");
				// d($query_list);
				foreach($query_list as $query):
					$query=trim($query);
					if($query!=""){
						// d($query);
		    			$this->db->query($query);
		    		}
	    		endforeach;
				$this->db->query("SET FOREIGN_KEY_CHECKS=1");
	     			echo "System Restored Successfully";
     		}
     		catch (Exception $e) {
			  //alert the user.
			  echo $e->getMessage();
			}	
     			
     			 // redirect("admin/restore_db");
		}

}

/* End of file Manage_admin.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_admin.php */