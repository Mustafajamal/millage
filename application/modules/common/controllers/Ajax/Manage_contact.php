<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_contact extends CI_Controller 
{

		public function __construct()
		{
			parent::__construct();
			
			$this->table="contact_us";
			$this->view_path = 'common/ajax/Manage_contact/';
		}
		
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'view',$this->data);
		}
		
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('id' => $id);
			$result = $this->custom->deleteRow($this->table,$where);
			echo $result;
		}
		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->custom->getSingleRow($this->table,array('id' => $id),"array");
			if($row)
			{
				$this->data['contact_data'] = $row;
			}	
		}

}

/* End of file Manage_department.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_department.php */