<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_dealers_application extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			$this->table="dealership";
			$this->logged_id = $this->session->user_id;
			$this->view_path = 'common/ajax/Manage_dealer_application/';
		}

				
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}

		
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('dealership_id' => $id);
			$result = $this->custom->deleteRow($this->table,$where);
			echo $result;
		}
		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->custom->getSingleRow($this->table,array('dealership_id' => $id),"array");
			if($row)
			{
				$this->data['country']=$this->custom->getSingleValue("countries","country_name",array("country_code"=>$row['country']));
				$this->data['dealership'] = $row;
			}	
		}

}
