<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_subscriber extends CI_Controller 
{

		public function __construct()
		{
			parent::__construct();
			
			$this->table="subscriber";
		}
	
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('id' => $id);
			$result = $this->custom->deleteRow($this->table,$where);
			echo $result;
		}
}

/* End of file Manage_department.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_department.php */