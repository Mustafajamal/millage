<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_product_option extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			$this->table="watch_options_master";
			$this->logged_id = $this->session->user_id;
			$this->view_path = 'common/ajax/Manage_product_option/';
			$this->watch_id=$this->data['watch_id']=$this->session->watch_id;
			$this->watch_data=$this->data['watch_data']=$this->custom->getSingleRow("watch_master",array("watch_id"=>$this->watch_id),"array");
			
		}
		public function add()
		{
			is_ajax();
			$this->data['mode'] = "add";
			$this->parser->parse($this->view_path.'add',$this->data);
		}
		public function edit()
		{
			$this->fetchData();
			$this->data['mode'] = "Edit";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}

		public function save()
		{
			$post=$post=$this->input->post();
	        /***************check price display or not********************/
	        	$retailer=$post['retail_price_radio'];
		        $dealer=$post['dealer_price_radio'];
		        $distributor=$post['distributor_price_radio'];
		        unset($post['retail_price_radio']);
		        unset($post['dealer_price_radio']);
		        unset($post['distributor_price_radio']);
		        if($retailer)
		        {
		        	$post['retail_price']=0.0;
		        }
		        if($dealer)
		        {
		        	$post['dealer_price']=0.0;
		        }
		        if($distributor)
		        {
		        	$post['distributor_price']=0.0;
		        }
			/***************************************************************/
			if($post)
			{
				$post['watch_id']=$this->watch_id;
				if (!is_dir(FCPATH.'uploads/products/'.$this->watch_data['product_number'].'/'.$post['dial_color'])) {
					if(!mkdir(FCPATH.'uploads/products/'.$this->watch_data['product_number'].'/'.$post['dial_color'])){
						$result['message']="Can not Create Folder";
					}
				}
				if(!empty($_FILES['main_image']['name'][0])):
		          $upload_data=file_upload("logo","main_image","products/".$this->watch_data['product_number'].'/'.$post['dial_color']);
		          if($upload_data['status']){
		          	$post['main_image']=$upload_data['upload_data']['file_name'];
		          }
		          else{
		          	$result['message']=$upload_data['error'];
		          }
		        endif;
		        if(!empty($_FILES['other_image']['name'][0])):
	    			for ($i = 0; $i <count($_FILES['other_image']['name']) ; $i++) 
	    			{
	    				$_FILES['one_file']['name']=$_FILES['other_image']['name'][$i];
	    				$_FILES['one_file']['type']=$_FILES['other_image']['type'][$i];
	    				$_FILES['one_file']['tmp_name']=$_FILES['other_image']['tmp_name'][$i];
	    				$_FILES['one_file']['error']=$_FILES['other_image']['error'][$i];
	    				$_FILES['one_file']['size']=$_FILES['other_image']['size'][$i];
	    				$multiple_upload_data=file_upload("other-".($i+1),"one_file","products/".$this->watch_data['product_number'].'/'.$post['dial_color']);
	    				// d($multiple_upload_data);
	    				if($multiple_upload_data['status']){
	    					$other[]=$multiple_upload_data['upload_data']['file_name'];	
				        }
				        else{
				        	$result['message']=$multiple_upload_data['error'];
				        }
	    			}
	    			$post['other_image']=implode(',',$other);
		        endif;
		       
				$res=$this->custom->insertRow($this->table,$post);
				if (!is_integer($res)) {
					$result['message']="Recoed Can not Insert";
				}
				if (!empty($result)) {
					$this->session->set_flashdata('message-error',implode(" , ",$result['message']));
				}
				else{
					$this->session->set_flashdata('message-success',"Option Added Successfully");
				}
			}
			else
			{
				show_404();
			}
			redirect('admin/manage_product_option/'.$this->watch_id,'refresh');
		}
		public function update()
		{
			$post=$this->input->post();
			if($post)
			{
				$other=array();
				$result['message']=array();
				$id = $post['watch_option_id'];
				unset($post['watch_option_id']);
				$where = array('watch_option_id'=>$id);
				$dial_color=$this->custom->getSingleValue($this->table,"dial_color",$where);
				rename(FCPATH.'uploads/products/'.$this->watch_data['product_number'].'/'.$dial_color,FCPATH.'uploads/products/'.$this->watch_data['product_number'].'/'.$post['dial_color']);
				if(!empty($_FILES['main_image']['name'][0])):
		          $upload_data=file_upload("main","main_image","products/".$this->watch_data['product_number'].'/'.$post['dial_color']);
		          // d($upload_data);
		          if($upload_data['status']){
		          	$post['main_image']=$upload_data['upload_data']['file_name'];
		          }
		          else{
		          	$result['message'][]=$upload_data['error'];
		          }
		        endif;
		        if(!empty($_FILES['other_image']['name'][0])):
	    			for ($i = 0; $i <count($_FILES['other_image']['name']) ; $i++) 
	    			{
	    				$_FILES['one_file']['name']=$_FILES['other_image']['name'][$i];
	    				$_FILES['one_file']['type']=$_FILES['other_image']['type'][$i];
	    				$_FILES['one_file']['tmp_name']=$_FILES['other_image']['tmp_name'][$i];
	    				$_FILES['one_file']['error']=$_FILES['other_image']['error'][$i];
	    				$_FILES['one_file']['size']=$_FILES['other_image']['size'][$i];
	    				$multiple_upload_data=file_upload("other-".($i+1),"one_file","products/".$this->watch_data['product_number'].'/'.$post['dial_color']);
	    				// d($multiple_upload_data);
	    				if($multiple_upload_data['status']){
	    					$other[]=$multiple_upload_data['upload_data']['file_name'];	
				        }
				        else{
				        	$result['message'][]=$multiple_upload_data['error'];
				        }
	    			}
	    			$post['other_image']=implode(',',$other);
		        endif;
		        /***************check price display or not********************/
		            $retailer=$post['retail_price_radio'];
			        $dealer=$post['dealer_price_radio'];
			        $distributor=$post['distributor_price_radio'];
			        unset($post['retail_price_radio']);
			        unset($post['dealer_price_radio']);
			        unset($post['distributor_price_radio']);
			        if($retailer)
			        {
			        	$post['retail_price']=0.0;
			        }
			        if($dealer)
			        {
			        	$post['dealer_price']=0.0;
			        }
			        if($distributor)
			        {
			        	$post['distributor_price']=0.0;
			        }
				/***************************************************************/
				// d($post);
				// exit();
				$res=$this->custom->updateRow($this->table,$post,$where);
				if ($res!="updated") {
					$result['message'][]="Recoed Can not Insert";
				}
				// d($result['message']);
				if (!empty($result)) {
					$this->session->set_flashdata('message-error',implode(" , ",$result['message']));
				}
				else{
					$this->session->set_flashdata('message-success',"Option Updated Successfully");
				}
				redirect('admin/manage_product_option/'.$this->watch_id,'refresh');
			}
			else
			{
				show_404();
			}
		}
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('watch_option_id' => $id);
			$result = $this->custom->deleteRow($this->table,$where);
			echo $result;
		}

		public function mark_as_on_sale()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('watch_option_id' => $id);
			$on_sale=$this->custom->getSingleValue($this->table,"on_sale",$where);
			if ($on_sale) {
				$data['on_sale']=0;
			}else{
				$data['on_sale']=1;
			}
			$res=$this->custom->updateRow($this->table,$data,$where);

			if (!$on_sale)
			{
				echo "Product Mark As On Sale";
			}
			else
			{
				echo "Product Remove From On Sale";
			}
		}

		public function mark_as_new()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('watch_option_id' => $id);
			$is_new=$this->custom->getSingleValue($this->table,"is_new",$where);
			if ($is_new) {
				$data['is_new']=0;
			}else{
				$data['is_new']=1;
			}
			$res=$this->custom->updateRow($this->table,$data,$where);
		
			if (!$is_new)
			{
				echo "Product Mark As New";
			}
			else
			{
				echo "Product Remove From New";
			}
		}

		public function mark_as_banner_product()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('watch_option_id' => $id);
			$is_banner_product=$this->custom->getSingleValue($this->table,"is_banner_product",$where);
			if ($is_banner_product) {
				$data['is_banner_product']=0;
			}else{
				$data['is_banner_product']=1;
			}
			$res=$this->custom->updateRow($this->table,$data,$where);
		
			if (!$is_banner_product)
			{
				echo "Product Mark As Banner Product";
			}
			else
			{
				echo "Product Remove From Banner Product";
			}
		}

		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row = $this->custom->getSingleRow($this->table,array('watch_option_id' => $id),"array");
			if($row)
			{
				$this->data['product_option_data'] = $row;
			}	
		}

}

/* End of file Manage_department.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_department.php */