<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_retailers_orders extends CI_Controller {

	public $view_path;
		public $data;
		public $table;
		public $logged_id;
		public function __construct()
		{
			parent::__construct();
			
			$this->table="order_master";
			$this->logged_id = $this->session->user_id;
			$this->view_path = 'common/ajax/Manage_retailers_orders/';
		}
		public function edit()
		{
			$this->fetchData();
			$this->data['mode'] = "Edit";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}
		public function view()
		{
			$this->fetchData();
			$this->data['mode'] = "View";
			$this->parser->parse($this->view_path.'edit',$this->data);
		}

		public function save()
		{
			$post=$post=$this->input->post();
			if($post)
			{
				$res=$this->custom->insertRow($this->table,$post);
				if (is_integer($res)) {
					$this->session->set_flashdata('message-success',"Category Added Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
			}
			else
			{
				show_404();
			}
			redirect('admin/manage_retailers_orders','refresh');
		}
		public function update()
		{
			$post=$this->input->post();
			d($post);
			/*if($post)
			{
				$id = $post['order_id'];
				unset($post['order_id']);
				$where = array('order_id'=>$id);
				$result = $this->custom->updateRow($this->table,$post,$where);
				if($result){
					$this->session->set_flashdata('message-success',"Category Updated Successfully");
				}
				else{
					$this->session->set_flashdata('message-error',"Something Went Wrong");
				}
				redirect('admin/manage_retailers_orders','refresh');
			}
			else
			{
				show_404();
			}*/
		}
		public function delete()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('order_id' => $id);
			$result = $this->custom->deleteRow($this->table,$where);
			echo $result;
		}
		
		public function canceled()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$is_shipped=$this->custom->getRowsWhereInLike($this->table,array('order_id'=>$id),"order_status",["SHIPPED","DELIVERED"],'','',null,'',"array");
			$where = array('order_id' => $id);
			if (empty($is_shipped)) {
				 
				 $result = $this->custom->updateRow($this->table,array("order_status"=>"CANCELED"),$where);
				
				if ($result=="updated") {
					echo "Canceled";
				}else{
					echo "Not Canceled";
				}
			}
		}
		public function success()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$is_shipped=$this->custom->getRowsWhereInLike($this->table,array('order_id'=>$id),"order_status",["SHIPPED","DELIVERED"],'','',null,'',"array");
			$where = array('order_id' => $id);
			if (empty($is_shipped)) {
			 	$result = $this->custom->updateRow($this->table,array("order_status"=>"SUCCESS"),$where);
			 	if ($result=="updated") {
					echo "Success";
				}else{
					echo "Not Success";
				}
			 }
			 		}
		public function shipped()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$where = array('order_id' => $id);
			$is_shipped=$this->custom->getRows($this->table,array("order_status !="=>"SHIPPED",'order_id'=>$id,"order_status !="=>"DELIVERED"),"array");
			if (!empty($is_shipped)) {
				$quantity=$this->custom->getRows("order_item_master",$where,"array");
				
				foreach ($quantity as $key=>$value) {
					$sold_stock=$this->custom->getSingleValue("watch_options_master","sold_stock",array("watch_option_id"=>$value->watch_option_id));
					$this->custom->updateRow("watch_options_master",array("sold_stock"=>$sold_stock+$value->quantity),array("watch_option_id"=>$value->watch_option_id));
				}
				
				$result = $this->custom->updateRow($this->table,array("order_status"=>"SHIPPED"),$where);
				if ($result=="updated") {
					echo "Shipped";
				}else{
					echo "Not Shipped";
				}
			}
		}
		public function delivered()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			$is_shipped=$this->custom->getRows($this->table,array("order_status"=>"SHIPPED",'order_id'=>$id),"array");
			// print_r($is_shipped);
			$where = array('order_id' => $id);
			if (!empty($is_shipped)) {
			 	$result = $this->custom->updateRow($this->table,array("order_status"=>"DELIVERED"),$where);
			 	if ($result=="updated") {
					echo "Delivered";
				}else{
					echo "Not Delivered";
				}
			 }
			 
		}
		public function invoice()
		{
			is_ajax();
			$id=$this->input->post('rowID');
			?>
			<script>
				var id="<?php echo $id; ?>";
				window.location = "<?php echo base_url(); ?>"+"admin/invoices_view/"+id;
			</script>
			<?php 
		}



		function fetchData(){
			is_ajax();
			$id=$this->input->post('rowID');
			$row=$this->custom->getRowsWhereJoin("order_master",array("order_master.order_status !="=>"CHOSEN","order_master.order_id"=>$id),array("order_item_master"),array("order_master.order_id=order_item_master.order_id"));
			if($row)
			{
				$this->data['order_data'] = $row;
			}	
		}

}

/* End of file Manage_department.php */
/* Location: ./application/modules/common/controllers/Ajax/Manage_department.php */