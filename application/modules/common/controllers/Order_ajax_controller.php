<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_ajax_controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function remove_item()
	{
		$post=$this->input->post();
		$order_data=$this->custom->getSingleRow("order_item_master",$post);
		$watch_option_data=$this->custom->getSingleRow("watch_options_master",array("watch_option_id"=>$order_data->watch_option_id));
		$current_total=$this->custom->getSingleValue("order_master","total",array("order_id"=>$order_data->order_id));
		
           if($this->session->userdata("group_level")=="dealer")
           {
              //echo $watch_option->dealer_price;
              echo $total=$current_total-($watch_option_data->dealer_price*$order_data->quantity);
           }
           else if($this->session->userdata("group_level")=="retailer")
           {
              //echo $watch_option->distributor_price;
              $total=$current_total-($watch_option_data->distributor_price*$order_data->quantity);
           }
           else
           {
              //echo $watch_option->retail_price;
              $total=$current_total-($watch_option_data->retail_price*$order_data->quantity);
           }

		// $total=$current_total-($watch_option_data->retail_price*$order_data->quantity);

		$this->custom->updateRow("order_master",array("total"=>$total),array("order_id"=>$order_data->order_id));
		$this->custom->deleteRow("order_item_master",$post);
		$this->session->set_flashdata('message-error', 'Item Removed From Cart');
			echo "success";
	}

	public function add_item(){
		$post=$this->input->post();
		/* set quantity */
		$qty=1;
		if(isset($post['quantity'])){
			$qty=$post['quantity'];
			unset($post['quantity']);
			if ($qty==0) {
				$qty=1;
			}
		}
		/* check order is added if not than insert */
		$order_data=$this->custom->getSingleRow("order_master",array("customer_user_id"=>$this->session->user_id,"order_status"=>"CHOSEN"));
		if(is_null($order_data)){
			$this->custom->insertRow("order_master",array("customer_user_id"=>$this->session->user_id,"user_group_id"=>$this->session->group_id));
			$order_data=$this->custom->getSingleRow("order_master",array("customer_user_id"=>$this->session->user_id,"order_status"=>"CHOSEN"));
		}
		// $order_data;
		/* check order is added if not than insert */
		$watch_option_data=$this->custom->getSingleRow("watch_options_master",$post);
		$current_total=$order_data->total;
		
		  if($this->session->userdata("group_level")=="dealer")
           {
              $total=$current_total+($watch_option_data->dealer_price*$qty);
           }
           else if($this->session->userdata("group_level")=="retailer")
           {
              $total=$current_total+($watch_option_data->distributor_price*$qty);
           }
           else
           {
           		$total=$current_total+($watch_option_data->retail_price*$qty);
           }


		$cart_data=$this->custom->getSingleRow("order_item_master",array("order_id"=>$order_data->order_id,"watch_option_id"=>$post['watch_option_id']));
		// var_dump($cart_data);
		if (is_null($cart_data)) {
			$this->custom->insertRow("order_item_master",array("order_id"=>$order_data->order_id,"watch_option_id"=>$post['watch_option_id'],"quantity"=>$qty));
			$this->custom->updateRow("order_master",array('total'=>$total),array("order_id"=>$order_data->order_id));
			$this->session->set_flashdata('message-success', 'Item Added To Cart');
			echo "success";
		}
		else{
			echo "Already In Cart";
		}
	}

	public function update_qty()
	{
		$post=$this->input->post();
		$new_qty=$post['new_qty'];
		unset($post['new_qty']);
		$order_data=$this->custom->getSingleRow("order_item_master",$post);
		$watch_option_data=$this->custom->getSingleRow("watch_options_master",array("watch_option_id"=>$order_data->watch_option_id));
		$current_total=$this->custom->getSingleValue("order_master","total",array("order_id"=>$order_data->order_id));
		
		if($this->session->userdata("group_level")=="dealer")
           {
              $total=$current_total-($watch_option_data->dealer_price*$order_data->quantity);

           }
           else if($this->session->userdata("group_level")=="retailer")
           {
           	  $total=$current_total-($watch_option_data->distributor_price*$order_data->quantity);
           }
           else
           {
           		$total=$current_total-($watch_option_data->retail_price*$order_data->quantity);
           }

           	if($this->session->userdata("group_level")=="dealer")
           {
              $total=$total+($watch_option_data->dealer_price*$new_qty);
           }
           else if($this->session->userdata("group_level")=="retailer")
           {
			  $total=$total+($watch_option_data->distributor_price*$new_qty);

           }
           else
           {
           		$total=$total+($watch_option_data->retail_price*$new_qty);	
           }

		
		$this->custom->updateRow("order_master",array("total"=>$total),array("order_id"=>$order_data->order_id));
		$this->custom->updateRow("order_item_master",array("quantity"=>$new_qty),$post);
		$this->session->set_flashdata('message-success', 'Cart Updated Successfully');
			echo "success";
	}

}

/* End of file Order_ajax_controller.php */
/* Location: ./application/controllers/Order_ajax_controller.php */