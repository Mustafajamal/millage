<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->header_vars['categories']=$this->custom->getRows("categories_master");
		$this->header_vars['style']=$this->custom->getRows("style_master");
		$this->header_vars['page_categories']=$this->custom->getRows("cms_page_category");
		$this->header_vars['site_logo']=get_site_info("site_logo");
		$this->header_vars['site_name']=get_site_info("site_title");

		$this->header_vars['cart_item']=$this->custom->getRowsWhereJoin("order_master",array("order_master.order_status"=>"CHOSEN","order_master.customer_user_id"=>$this->session->user_id),array("order_item_master"),array("order_master.order_id=order_item_master.order_id"));



		$custom_search_series['series']=$this->custom->createDropdownSelect("series_master",array("series_id","series_name"),"Series");
		$custom_search_series['color']=$this->custom->getDistinctRows("watch_options_master",array(),array(),"dial_color");
		$this->footer_vars['max_price']=$custom_search_series['max_price']=$this->custom->getAggrigateValue("watch_options_master","retail_price");

		$this->footer_vars['min_price']=$custom_search_series['min_price']=$this->custom->getAggrigateValue("watch_options_master","retail_price",'min');

				
		$this->footer_vars['custom_search']=$this->load->view('custom_search',$custom_search_series, TRUE);



		if ($this->ion_auth->logged_in()) {
			if ($this->uri->segment(2)!="logout") {
				if ($this->uri->segment(1) !=$this->session->group_level) {
					redirect($this->session->group_level,'refresh');
			  	}
			}
		}
		if ($this->ion_auth->logged_in() && $this->session->group_level=="admin") {
			if ($this->uri->segment(2)!="logout") {
				redirect($this->session->group_level.'/dashboard','refresh');
			}
        }
        if (!$this->ion_auth->logged_in()) {
			$this->session->set_userdata('group_level',"common");
			if ($this->uri->segment(1) !=$this->session->group_level) {
				redirect($this->session->group_level,'refresh');
		  	}
        }
	}

	public function index()
	{
		$slider_img=get_site_info("slider_images");
		$slider_images=explode(',', $slider_img);
		$this->body_vars['slider_images']=$slider_images;
		$this->body_vars['new_watch']=$this->custom->getRowsSorted("watch_options_master",array("is_new"=>1),array(),"date","DESC");
		$this->body_vars['sell_watch']=$this->custom->getRowsSorted("watch_options_master",array("on_sale"=>1),array(),'watch_option_id','');
		$this->body_vars['banner_watch']=$this->custom->getRowsSorted("watch_options_master",array("is_banner_product"=>1),array(),'watch_option_id','',4);
		$this->body_vars['footer_img']=get_site_info("footer_image");
		// d($this->body_vars['cart_item']);
		// exit();
	}

	public function product_detail($id="")
	{
		if ($id!="") {
			$this->body_vars['watch_detail']=$watch_detail=$this->custom->getSingleRow("watch_options_master",array('watch_option_id'=>$id));
			if (!is_null($watch_detail)) {
				$this->body_vars['product_data']=$product_data=$this->custom->getSingleRow("watch_master",array('watch_id'=>$watch_detail->watch_id));
				$this->body_vars['series_data']=$series_data=$this->custom->getSingleValue("series_master","series_name",array('series_id'=>$product_data->series_id));
				$this->body_vars['style_data']=$style_data=$this->custom->getSingleValue("style_master","style_name",array('style_id'=>$product_data->style_id));
				$this->body_vars['category_data']=$category_data=$this->custom->getSingleValue("categories_master","category_name",array('category_id'=>$product_data->category_id));
			}
			else{
				redirect('','refresh');
			}
		}
		else{
			redirect('','refresh');	
		}
	}

	public function product_list($category_id="",$style_id="")
	{
		$series_id=$this->input->get('series');
		if (isset($series_id)) {
			$where['series_id']=$series_id;	
		}
		$color=$this->input->get('color');
		if (isset($color)) {
			$where_option['dial_color']=$color;	
		}

		// $where=array();
		// $get=$this->input->get();
		// if($get)
		// {
		// 	if(isset($get['series']))
		// 	{
		// 		$where['series_id']=$get['series'];
		// 	}			
		// }
		if ($category_id!="") {
			$where['category_id']=$category_id;
		}
		if ($style_id!="") {
			$where['style_id']=$style_id;
		}
		$where=array_diff($where,array(""));
		$products=$this->custom->getRows("watch_master",$where);
		$this->body_vars['products']=$products;
		$all_products_options=array();
		foreach ($products as $one_product) {
			$where_option['watch_id']=$one_product->watch_id;
			

			// if($get)
			// {
			// 	if(isset($get['color']))
			// 	{
			// 		$where_option['dial_color']=$get['color'];	
			// 	}			
			// }	

			$result1=$this->custom->getAggrigateValue("watch_options_master","retail_price");
			$result2=$this->custom->getAggrigateValue("watch_options_master","retail_price",'min');
			
			// if($get)
			// {
			// 	if(isset($get['amount1']) && isset($get['amount2']))
			// 	{
			// 		if($this->session->userdata("group_level")=="dealer")
			// 	   {
			// 	   	  	$where_option['dealer_price>=']=$get['amount1'];
			// 			$where_option['dealer_price<=']=$get['amount2'];

			// 			$this->body_vars['max_price']=$result1[0]->dealer_price;
			// 			$this->body_vars['min_price']=$result2[0]->dealer_price;
			// 	   }
			// 	   else if($this->session->userdata("group_level")=="retailer")
			// 	   {
			// 	   	   	$where_option['distributor_price>=']=$get['amount1'];
			// 			$where_option['distributor_price<=']=$get['amount2'];

			// 			$this->body_vars['max_price']=$result1[0]->distributor_price;
			// 			$this->body_vars['min_price']=$result2[0]->distributor_price;
			// 	   }
			// 	   else
			// 	   {
			// 	   	   	$where_option['retail_price>=']=$get['amount1'];
			// 			$where_option['retail_price<=']=$get['amount2'];
			// 	   }	
			// 	}

			// }
				$this->body_vars['max_price']=$result1;
				$this->body_vars['min_price']=$result2;

			$all_products_options[/*$one_product->category_id][$one_product->style_id][$one_product->watch_id][*/]=$this->custom->getRows("watch_options_master",$where_option);
		}
		$this->body_vars['all_products_options']=$all_products_options;
		$this->body_vars['series']=$this->custom->getRows("series_master");
		$this->body_vars['color']=$this->custom->getDistinctRows("watch_options_master",array(),array(),"dial_color");
	}


	public function logout($redirect="")
	{
		$this->ion_auth->logout();
		redirect($redirect,'refresh');
	}

	public function login_check()
	{
		$post=$this->input->post();
		$result=$this->ion_auth->login($post['username'],$post['password']);
		
		if($result)
		{
			$user_data=$this->ion_auth->get_users_groups()->result()[0];
			$this->session->set_userdata('group_id',$user_data->id);
			$this->session->set_userdata('group_level',trim($user_data->name));
			$this->session->set_userdata('ip_address',$this->input->ip_address());
			$this->session->set_userdata('username',$this->ion_auth->user()->result()[0]->username);
			$this->session->set_userdata('login_from',"system");
			// print_r($this->session);
			echo json_encode(array("status"=>"success","message"=>$this->session->group_level));
			exit();
		}
		else{
			echo json_encode(array("status"=>"error","message"=>$this->ion_auth->messages().''.$this->ion_auth->errors()));
		}
	}

	public function view_cart()
	{
		if ($this->session->group_level!="customer") {
			redirect('','refresh');
		}
		$slider_img=get_site_info("slider_images");
		$slider_images=explode(',', $slider_img);
		$this->body_vars['slider_images']=$slider_images;
	}

	public function checkout()
	{
		if ($this->session->group_level=="admin") {
			redirect('','refresh');
		}
		$this->body_vars['ion_data']=$ion_data=$this->ion_auth->user()->row();

		 if($this->session->userdata("group_level")=="dealer")
         {
         	$table_nm="dealer_data";
         }
         else if($this->session->userdata("group_level")=="retailer")
         {
            $table_nm="retailer_data";
         }
         else
         {
         	$table_nm="customer_data";
         }

		$this->body_vars['cust_data']=$this->custom->getSingleRow($table_nm,array('user_id'=>$ion_data->user_id));
		$countries=$this->custom->getRows("countries",array(),array(),NULL,"id,country_name","array");
		foreach ($countries as $value) {
			$countries_array[$value['id']]=$value['country_name'];
		}
		$this->body_vars['countries']=$countries_array;
		$this->header_vars['cart_item']=$this->custom->getRowsWhereJoin("order_master",array("order_master.order_status"=>"CHOSEN","order_master.customer_user_id"=>$this->session->user_id),array("order_item_master"),array("order_master.order_id=order_item_master.order_id"));
	}

	public function manage_profile($action="form")
	{
		if ($this->session->group_level=="admin") {
			redirect('','refresh');
		}
		$this->body_vars['ion_data']=$ion_data=$this->ion_auth->user()->row();

		 if($this->session->userdata("group_level")=="dealer")
         {
         	$table_nm="dealer_data";
         }
         else if($this->session->userdata("group_level")=="retailer")
         {
            $table_nm="retailer_data";
         }
         else
         {
         	$table_nm="customer_data";
         }

		$this->body_vars['cust_data']=$this->custom->getSingleRow($table_nm,array('user_id'=>$ion_data->user_id));
		
		$countries=$this->custom->getRows("countries",array(),array(),NULL,"id,country_name","array");
		foreach ($countries as $value) {
			$countries_array[$value['id']]=$value['country_name'];
		}
		$this->body_vars['countries']=$countries_array;
		if ($action=="save") {
			$post=$this->input->post();
			$ion_auth_data['phone']=$post['phone'];
			unset($post['phone']);
			$ion_auth_data['username']=$post['username'];
			unset($post['username']);
			unset($post['email']);
			/*d($post,$ion_auth_data);
			exit();*/
			$this->custom->updateRow($table_nm,$post,array('user_id'=>$this->session->user_id));
			$this->ion_auth->update($this->session->user_id,$ion_auth_data);
			$this->session->set_flashdata('message-success', 'Profile Updated Successfully');
			redirect('customer/manage_profile','refresh');
		}
	}

	public function change_password($action='form'){
		if ($this->session->group_level=="admin") {
			redirect('','refresh');
		}
		if ($action=="save") {
			$data=$this->input->post();
			unset($data['identity']);
			if($this->ion_auth->change_password($this->session->identity, $data['old_password'], $data['new_password'])){
				$this->ion_auth->logout();
				$this->session->set_flashdata("message-success"," Password Changed Successfully");
				redirect('','refresh');
			}
			else{
				$this->session->set_flashdata("message-error","Your Old Password is Not match with your Current Password");
				redirect("customer/change_password");
			}
		}	
	}

	public function payment()
	{
		$cust_data=$post=$this->input->post();
		// d($post);
		if(isset($post['stripeToken'])){
			$amount_cents = str_replace(".","","10.52");  // Chargeble amount
			require APPPATH.'third_party/Stripe/Stripe.php';
			$params = array(
				"testmode"   => "on",
				"private_live_key" => "sk_test_oyz1Q7jpobOz0yhnPtSIfx1w",
				"public_live_key"  => "pk_test_KXCLYceBgnJKQysFVHHjGmr0",
				"private_test_key" => "sk_test_gYo684GbBN0GNdI5EkicTmb7",
				"public_test_key"  => "pk_test_j1nIalKisYAoMURkBQA6MbhC"
			);

			if ($params['testmode'] == "on") {
				Stripe::setApiKey($params['private_test_key']);
				$pubkey = $params['public_test_key'];
			} else {
				Stripe::setApiKey($params['private_live_key']);
				$pubkey = $params['public_live_key'];
			}
			$charge = Stripe_Charge::create(array(
				  "amount" => $amount_cents,
				  "currency" => "usd",
				  "source" => $post['stripeToken'],
			));
			// if ($charge->source->address_zip_check == "fail") {
			// 	throw new Exception("zip_check_invalid");
			// } else if ($charge->source->address_line1_check == "fail") {
			// 	throw new Exception("address_check_invalid");
			// } else if ($charge->source->cvc_check == "fail") {
			// 	throw new Exception("cvc_check_invalid");
			// }
			$payment_info=$charge->__toArray(true);
			$data['txn_id']	= $payment_info["balance_transaction"];
			$data['payment_gross'] = $payment_info["amount"];
			$data['currency_code'] = $payment_info["currency"];
			$data['payer_email'] = $payment_info['source']["name"];
			$data['payment_status']	= $payment_info["status"];
			$data['user_id'] = $this->session->user_id;
			$data['product_id']	= $this->session->order_id;
			$data['gatway']	= "stripe";
			$data['all_data']	= json_encode($payment_info);
			// $data['order_date']=date('Y-m-d H:i:s');
			$this->custom->insertRow("payments",$data);
			// if($payment_info["status"]=
			$this->custom->updateRow("order_master",array("order_status"=>"SUCCESS","order_date"=>date('Y-m-d H:i:s')),array("order_id"=>$this->session->order_id));
			$this->session->set_flashdata("message-success","Your Order Has Been Placed Successfully");
			$this->session->set_userdata("order_id","done");
			redirect($this->session->group_level.'/success','refresh');
		}
		if (empty($post)) {
			redirect('customer/checkout','refresh');
		}
		if (!isset($post['order_note'])) {
			$post['order_note']="";
		}
		$this->custom->updateRow("order_master",array("order_status"=>"PENDING","order_note"=>$post['order_note'],'order_date'=>date('Y-m-d H:i:s')),array("order_id"=>$post['order_id_table']));
		$payment_method=$cust_data['payment_method'];
		unset($cust_data['order_note']);
		unset($cust_data['payment_method']);
		unset($cust_data['payable_amount']);
		unset($cust_data['order_id_table']);
		unset($cust_data['phone']);
		unset($cust_data['email']);
		if (isset($post['phone'])) {
			$this->ion_auth->update($this->session->user_id,array("phone"=>$post['phone']));
		}
		if (!empty($cust_data)) {
			$this->custom->updateRow("customer_data",$cust_data,array('user_id'=>$this->session->user_id));
		}
		$this->session->set_userdata('order_id',$post['order_id_table']);
		if ($payment_method=="stripe") {
			require APPPATH.'third_party/Stripe/Stripe.php';
			$data['custom']=$post['order_id_table'];
			$data['amount']=$post['payable_amount'];
			$this->load->view('stripe_view',$data);
		}
		if ($payment_method=="paypal") {
			$this->load->library('paypal_lib');
			$returnURL = base_url().$this->session->group_level.'/success'; //payment success url
			$cancelURL = base_url().$this->session->group_level.'/cancel'; //payment cancel url
			$notifyURL = base_url().$this->session->group_level.'/ipn'; //ipn url
			//get particular product data
			// $product = $this->product->getRows($id);
			$userID = $this->session->user_id; //current user id
			$logo =UPLOAD_URL."/site/".get_site_info("site_logo");
			
			$this->paypal_lib->add_field('return', $returnURL);
			$this->paypal_lib->add_field('cancel_return', $cancelURL);
			$this->paypal_lib->add_field('notify_url', $notifyURL);
			$this->paypal_lib->add_field('item_name', get_site_info('site_title'));
			$this->paypal_lib->add_field('custom', $post['order_id_table']);
			// $this->paypal_lib->add_field('item_number',  "50");
			$this->paypal_lib->add_field('amount',  $post['payable_amount']);		
			$this->paypal_lib->image($logo);
			$this->paypal_lib->paypal_auto_form();
		}
		$this->header_file="blank.php";
		$this->body_file="blank.php";
		$this->footer_file="blank.php";
	}

	public function success()
	{
		if(is_null($this->session->order_id)){
			redirect('customer/my_orders','refresh');
		}
		$paypalInfo=$this->input->post();
		if(!is_null($this->session->order_id) && $this->session->order_id!="done"){
			$data['txn_id']	= $paypalInfo["txn_id"];
			$data['payment_gross'] = $paypalInfo["payment_gross"];
			$data['currency_code'] = $paypalInfo["mc_currency"];
			$data['payer_email'] = $paypalInfo["payer_email"];
			$data['payment_status']	= $paypalInfo["payment_status"];
			$data['user_id'] = $this->session->user_id;
			$data['product_id']	= $paypalInfo["custom"];
			$data['gatway']	= "paypal";
			$data['all_data']	= json_encode($paypalInfo);
			// $data['order_date']=date('Y-m-d H:i:s');
			$this->custom->insertRow("payments",$data);
			$this->custom->updateRow("order_master",array("order_status"=>"SUCCESS","order_date"=>date('Y-m-d H:i:s')),array("order_id"=>$this->session->order_id));
			$this->session->unset_userdata("order_id");
			$this->session->set_flashdata("message-success","Your Order Has Been Placed Successfully");
		}else{
			$this->session->unset_userdata("order_id");
		}
		// redirect('common/my_orders','refresh');
	}

	public function cancel()
	{
		if(is_null($this->session->order_id)){
			redirect($this->session->group_level.'/my_orders','refresh');
		}
			$this->custom->updateRow("order_master",array("order_status"=>"CANCELED",'order_date'=>date('Y-m-d H:i:s')),array("order_id"=>$this->session->order_id));
			$this->session->unset_userdata("order_id");
			$this->session->set_flashdata("message-error","Your Canceled Your Transaction");
		// redirect('common/my_orders','refresh');
	}

	public function ipn()
	{
		d($this->session->order_id);
		d($this->input->post());
		exit();
	}

	public function my_orders($status)
	{
		if ($this->session->group_level=="common" || $this->session->group_level=="admin") {
			redirect('','refresh');
		}
		if ($status!="") {
			$this->body_vars['order_data']=$this->custom->getRowsWhereJoin("order_master",array("order_master.order_status !="=>"CHOSEN","order_master.order_status"=>$status,"order_master.customer_user_id"=>$this->session->user_id),array("order_item_master"),array("order_master.order_id=order_item_master.order_id"));	
		}else{
			$this->body_vars['order_data']=$this->custom->getRowsWhereJoin("order_master",array("order_master.order_status !="=>"CHOSEN","order_master.customer_user_id"=>$this->session->user_id),array("order_item_master"),array("order_master.order_id=order_item_master.order_id"));
		}
	}

	public function page($page_category='',$section_name='')
	{
		$this->body_vars['page_details']=array();
		if ($page_category!='') {
			$this->body_vars['sections']=$sections=$this->custom->getRows("cms_page_category_section",array("page_category_id"=>$page_category,"status"=>1));
			if ($section_name!="") {
				$this->body_vars['page_details']=$this->custom->getSingleRow("cms_page_category_section",array("page_category_id"=>$page_category,"status"=>1,"section_name"=>urldecode($section_name)));
			}else{
				if (!empty($sections)) {
					$this->body_vars['page_details']=$sections[0];
				}
			}
		}
	}

	public function subscribe()
	{
	
	}
	public function dealership()
	{
		$this->body_file="common/_dealership";
		$this->body_vars['country_array']=$this->custom->createDropdownSelect("countries",array("country_code","country_name"),"Country ",array(' '),null,array("US"));
	}

	public function apply($for="")
	{
		$this->body_vars['country_array']=$this->custom->createDropdownSelect("countries",array("country_code","country_name"),"Country ",array(' '),null,array("US"));
		if ($for=="dealership") {
			$this->body_file="common/dealership";
		}
		if ($for=="distributorship") {
			$this->body_file="common/distributorship";
		}
	}
	public function contactus()
	{
	}

}

/* End of file Common.php */
/* Location: ./application/controllers/Common.php */