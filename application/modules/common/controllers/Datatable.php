<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datatable extends CI_Controller {

	public $logged_id;
	public function __construct()
	{
		parent::__construct();
		// is_ajax();
		$this->logged_id = $this->session->user_id;
		$this->load->model('datatable_model','DT_model');
	}

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('person_view');
	}

	public function ajax_list($data_check)
	{
		$true_icon = "<center><label class='label label-success'><i class='fa fa-fw fa-lg fa-check'></i></label></center>"; // true icon
		$false_icon = "<center><label class='label label-danger'><i class='fa fa-fw fa-lg fa-times'></i></label></center>"; // false icon
		$logged_id = $this->session->user_id;
		$user = $this->session->level;
		/*
			List of group 
		*/

		

		if ($data_check=="manage_category") {
			$table1 = "categories_master";
			
			$col_prefix = "category_";
			$table_id = $col_prefix.'id';
			$table = $table1;
			$columns=array($table_id,$col_prefix."name");
		}

		if ($data_check=="manage_style") {
			$table1 = "style_master";
			
			$col_prefix = "style_";
			$table_id = $col_prefix.'id';
			$table = $table1;
			$columns=array($table_id,$col_prefix."name");
		}

		if ($data_check=="manage_series") {
			$table1 = "series_master";
			
			$col_prefix = "series_";
			$table_id = $col_prefix.'id';
			$table = $table1;
			$columns=array($table_id,$col_prefix."name","status");
		}

		if ($data_check=="manage_products") {
			$table1 = "watch_master";
			$table2 = "series_master";
			$table3 = "style_master";
			$table4 = "categories_master";
			
			$join_table=array($table2,$table3,$table4); 
			$join_condition=array("$table2.series_id=$table1.series_id","$table3.style_id=$table1.style_id","$table4.category_id=$table1.category_id");
			$table = $table1;
			$table_id="watch_id";
			$columns=array("$table1.$table_id","$table2.series_name","$table3.style_name","$table4.category_name","$table1.product_number","$table1.product_name","$table1.movement","$table1.crystal","$table1.case","$table1.case_diameter","$table1.case_thickness","$table1.bezel_material","$table1.bezel_function","$table1.water_resistant","$table1.calendar","$table1.bracelet","$table1.bracelet_length","$table1.bracelet_width","$table1.clasp_type");
		}

		if ($data_check=="manage_product_option") {
			$table1 = "watch_options_master";
			
			$col_prefix = "";
			$table_id = $col_prefix.'watch_option_id';
			$table = $table1;
			$where['watch_id']=$this->input->post('watch_id');
			$columns=array($table_id,$col_prefix."dial_color","dial_stock","retail_price","dealer_price","distributor_price","on_sale","is_new","is_banner_product");
		}

		if ($data_check=="manage_admin") {
			$table1 = "users";
			$table2 = "users_groups";
			$col_prefix = "";

			$table_id = $col_prefix.'id';

			$where[$table2.'.group_id']=1;
			$where[$table1.'.id != ']=1;

			$join_table=array($table2); 
			$join_condition=array("$table1.id=$table2.user_id");

			$table = $table1;
			$columns=array($table1.'.'.$table_id,$table1.'.'."username",$table1.'.'."email",$table1.'.'."first_name",$table1.'.'."last_name",$table1.'.'."phone",$table1.'.'."active");
		}

		if ($data_check=="manage_customer") {
			$table1 = "users";
			$table2 = "users_groups";
			$col_prefix = "";

			$table_id = $col_prefix.'id';

			$where[$table2.'.group_id']=4;

			$join_table=array($table2); 
			$join_condition=array("$table1.id=$table2.user_id");

			$table = $table1;
			$columns=array($table1.'.'.$table_id,$table1.'.'."username",$table1.'.'."email",$table1.'.'."first_name",$table1.'.'."last_name",$table1.'.'."phone",$table1.'.'."active");
		}

		if ($data_check=="manage_dealer") {
			$table1 = "users";
			$table2 = "users_groups";
			$col_prefix = "";

			$table_id = $col_prefix.'id';

			$where[$table2.'.group_id']=3;

			$join_table=array($table2); 
			$join_condition=array("$table1.id=$table2.user_id");

			$table = $table1;
			$columns=array($table1.'.'.$table_id,$table1.'.'."username",$table1.'.'."email",$table1.'.'."first_name",$table1.'.'."last_name",$table1.'.'."phone",$table1.'.'."active",$table1.'.'."address");
		}

		if ($data_check=="manage_retailer") {
			$table1 = "users";
			$table2 = "users_groups";
			$col_prefix = "";

			$table_id = $col_prefix.'id';

			$where[$table2.'.group_id']=2;

			$join_table=array($table2); 
			$join_condition=array("$table1.id=$table2.user_id");

			$table = $table1;
			$columns=array($table1.'.'.$table_id,$table1.'.'."username",$table1.'.'."email",$table1.'.'."first_name",$table1.'.'."last_name",$table1.'.'."phone",$table1.'.'."active");
		}

		if($data_check=="manage_customers_orders" || $data_check=="manage_retailers_orders" || $data_check=="manage_wholesale_orders")
		{
			$table1="order_master";
			$table2="users";

			$table_id="order_id";

			if($data_check=="manage_customers_orders")
				$where[$table1.'.user_group_id']=4;

			if($data_check=="manage_retailers_orders")
				$where[$table1.'.user_group_id']=3;

			if($data_check=="manage_wholesale_orders")
				$where[$table1.'.user_group_id']=2;

			$where[$table1.'.order_status != ']="CHOSEN";

			$join_table=array($table2);
			$join_condition=array("$table1.customer_user_id=$table2.id");

			$table=$table1;
			$columns=array($table1.".order_id",$table2.".username",$table1.".total",$table1.".order_status",$table1.".order_date");
		}

		if($data_check=="manage_page_category")
		{
			$table1="cms_page_category";
			$table_id="page_category_id";

			$table=$table1;
			$columns=array($table1.".page_category_id",$table1.".page_category_name");
		}
		if($data_check=="manage_page_data")
		{
			$table1="cms_page_category_section";
			$table2="cms_page_category";

			$table_id="section_id";

			$join_table=array($table2);
			$join_condition=array("$table1.page_category_id=$table2.page_category_id");

			$table=$table1;
			$columns=array($table1.".section_id",$table2.".page_category_name",$table1.".section_name",$table1.".status");
		}

		if ($data_check=="manage_subscriber") {
			$table1 = "subscriber";
			
			$table_id = 'id';
			$table = $table1;
			$columns=array($table_id,"subscriber_email","status","date");
		}

		if ($data_check=="contact_us") {
			$table1 = "contact_us";
			
			$table_id = 'id';
			$table = $table1;
			$columns=array($table_id,'name','email','telephone','address','city','state','postal_code','country','message','subject');
		}
		if ($data_check=="manage_dealer_application" || $data_check=="manage_distributor_application") {
			$table1 = "dealership";
			$table2="countries";
			$table_id = 'dealership_id';
			
			if($data_check=="manage_dealer_application")
			{
				$where[$table1.'.apply_for = ']="dealer";
			}

			if($data_check=="manage_distributor_application")
			{
				$where[$table1.'.apply_for = ']="distributor";
			}

			$join_table=array($table2); 
			$join_condition=array("$table1.country=$table2.country_code");


			$table = $table1;
			$columns=array($table1.".".$table_id,"corporation","contact_person","city",$table2.".country_name","telephone","email","website");
		}
		if($data_check=="manage_backup")
		{
			$table="backup_master";
			$table_id="backup_id";

			$columns=array($table_id,"backup_file","backup_time","backup_ip");
		}

		if($data_check=='manage_inventory')
		{
			$table1="watch_options_master";
			$table2 = "series_master";
			$table3="watch_master";
			$table_id = 'watch_option_id';
			
			
			$join_table=array($table3,$table2); 
			$join_condition=array("$table1.watch_id=$table3.watch_id","$table3.series_id=$table2.series_id");


			$table = $table1;
			$columns=array($table1.".".$table_id,$table2.".series_name",$table3.".product_number",$table1.".dial_color",$table1.".dial_stock",$table1.".sold_stock");
		}
		
		
		/*--------------------------------------------------------------------------------------------------------*/

		
		
		if (isset($join_table) && isset($join_condition) && isset($where)) {
			$list= $this->DT_model->get_datatables($table,$columns,$join_table,$join_condition,$where,$table_id);
		}
		elseif (isset($join_table) && isset($join_condition)) {
			$list = $this->DT_model->get_datatables($table,$columns,$join_table,$join_condition,null,$table_id);
		}
		elseif (isset($where)){
		 	$list = $this->DT_model->get_datatables($table,$columns,null,null,$where,$table_id);
		}
		else {
			$list = $this->DT_model->get_datatables($table,$columns,null,null,null,$table_id);		
		}	
		$data = array();

		$no = $this->input->post('start');
		
		foreach ($list as $person) {
			
			$no++;
			$row = array();
			/*
				get data of fired query as row as per requirement
			*/
			

			
			if($data_check=="manage_category")
			{
				$row[] = $person->$table_id;
				$row[] = $person->category_name;
			}

			if($data_check=="manage_style")
			{
				$row[] = $person->$table_id;
				$row[] = $person->style_name;
			}

			if($data_check=="manage_series")
			{
				$row[] = $person->$table_id;
				$row[] = $person->series_name;
				if ($person->status) {
					$row[] = "<i class='fa fa-check text-success'> Active";	
				}else{
					$row[] = "<i class='fa fa-ban text-danger'> Inactive";	
				}
			}

			if ($data_check=="manage_products") {
				
				$row[] = $person->$table_id;
				$row[] = $person->series_name;
				$row[] = $person->style_name;
				$row[] = $person->category_name;
				$row[] = $person->product_number;
				$row[] = $person->product_name;
				// /*********************for movement***********************/
				// if($person->movement=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for crystal***********************/
				// if($person->crystal=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for case***********************/
				// if($person->case=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for case_diameter***********************/
				// if($person->case_diameter=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for case_thickness***********************/
				// if($person->case_thickness=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for bezel_material***********************/
				// if($person->bezel_material=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for bezel_function***********************/
				// if($person->bezel_function=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for water_resistant***********************/
				// if($person->water_resistant=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for calendar***********************/
				// if($person->calendar=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for bracelet***********************/
				// if($person->bracelet=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for bracelet_length***********************/
				// if($person->bracelet_length=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for bracelet_width***********************/
				// if($person->bracelet_width=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
				// /*********************for clasp_type***********************/
				// if($person->clasp_type=="YES"){
				// 	$row[] = "<i class='fa fa-check text-success'>";
				// }else{
				// 	$row[] = "<i class='fa fa-times text-danger'>";
				// }
			}

			if($data_check=="manage_product_option")
			{
				$row[] = $person->$table_id;
				$row[] = ucwords($person->dial_color);
				$row[] = $person->dial_stock;
				$row[] = $person->retail_price;
				$row[] = $person->dealer_price;
				$row[] = $person->distributor_price;
				if ($person->on_sale) {
					$row[] = "<i class='fa fa-check text-success'>";	
				}else{
					$row[] = "<i class='fa fa-times text-danger'>";	
				}
				if ($person->is_new) {
					$row[] = "<i class='fa fa-check text-success'>";	
				}else{
					$row[] = "<i class='fa fa-times text-danger'>";	
				}
				if ($person->is_banner_product) {
					$row[] = "<i class='fa fa-check text-success'>";	
				}else{
					$row[] = "<i class='fa fa-times text-danger'>";	
				}
			}

			if ($data_check=="manage_admin" || $data_check=="manage_customer" || $data_check=="manage_retailer" || $data_check=="manage_dealer") {
				$row[] = $person->table_id;
				$row[] = $person->username;
				$row[] = $person->email;
				$row[] = $person->first_name;
				$row[] = $person->last_name;
				$row[] = $person->phone;
				if($person->active){
					$row[] = "<span class='text-success'>Enabled</span>";
				}else{
					$row[] = "<span class='text-danger'>Disabled</span>";
				}
			}

			if($data_check=="manage_customers_orders" || $data_check=="manage_retailers_orders" || $data_check=="manage_wholesale_orders")
			{
				$row[]=$person->table_id;
				$row[]=$person->username;
				$row[]=$person->total;
				if($person->order_status=="PENDING")
				{
					$row[]="<span class='label label-warning'>PENDING</span>";
				}
				if($person->order_status=="CANCELED")
				{
					$row[]="<span class='label label-danger'>CANCELED</span>";
				}
				if($person->order_status=="SUCCESS")
				{
					$row[]="<span class='label label-success'>SUCCESS</span>";
				}
				if($person->order_status=="SHIPPED")
				{
					$row[]="<span class='label label-info'>SHIPPED</span>";
				}
				if($person->order_status=="DELIVERED")
				{
					$row[]="<span class='label label-primary'>DELIVERED</span>";
				}
				$row[]=$person->order_date;
			}

			if($data_check=="manage_page_category")
			{
				$row[]=$person->table_id;
				$row[]=$person->page_category_name;
			}

			if($data_check=="manage_page_data")
			{
				$row[]=$person->table_id;
				$row[]=$person->page_category_name;
				$row[]=$person->section_name;
				if($person->status==1){
					$row[] = "<i class='fa fa-check text-success'>";
				}else{
					$row[] = "<i class='fa fa-times text-danger'>";
				}
				$row[]=base_url('common/page/'.$person->page_category_id.'/'.$person->section_name);
			}

			if ($data_check=="manage_subscriber") 
			{
				$row[]=$person->$table_id;
				$row[]=$person->subscriber_email;
				if($person->status==1)
				{
					$status_value="Subscriber"; 
				}
				else
				{
					$status_value="Unsubscriber";
				}
				$row[]=$status_value;
				$row[]=$person->date;
			}

			if ($data_check=="contact_us") 
			{
				$row[]=$person->$table_id;
				$row[]=$person->name;
				$row[]=$person->email;
				$row[]=$person->telephone;
				$row[]=$person->city;
				$row[]=$person->country;
				$row[]=$person->message;
				$row[]=$person->subject;
			}

			if ($data_check=="manage_dealer_application" || $data_check=="manage_distributor_application")
			 {
			 	$row[] = $person->table_id;
				$row[] = $person->corporation;
				$row[] = $person->contact_person;
				$row[] = $person->city;
				$row[] = $person->country_name;
				$row[] = $person->telephone;
				$row[] = $person->email;
				$row[] = $person->website;
			}
			if($data_check=="manage_backup")
			{
				$row[]=$person->backup_id;
				$row[]=$person->backup_file;
				$row[]=$person->backup_time;
				$row[]=$person->backup_ip;
			}
			if($data_check=="manage_inventory")
			{
			
				$row[]=$person->table_id;
				$row[]=$person->series_name;
				$row[]=$person->product_number;
				$row[]=$person->dial_color;
				$row[]=$person->dial_stock;
				$row[]=$person->sold_stock;
				$row[]=$person->dial_stock-$person->sold_stock;

			}

			/*if ($data_check=="manage_customer") {
				$row[] = $person->table_id;
				$row[] = $person->username;
				$row[] = $person->email;
				$row[] = $person->first_name;
				$row[] = $person->last_name;
				$row[] = $person->phone;
				if($person->active){
					$row[] = "<span class='text-success'>Enabled</span>";
				}else{
					$row[] = "<span class='text-danger'>Disabled</span>";
				}
			}*/

			
			
			//add html for action
			
			$data[] = $row;
		}
		/*----------------------------------------------------------------------------------------------------*/

		if (isset($join_table) && isset($join_condition) && isset($where)) {
		$output = array(
						"draw" => $this->input->post('draw'),
						"recordsTotal" => $this->DT_model->count_all($table),
						"recordsFiltered" => $this->DT_model->count_filtered($table,$columns,$join_table,$join_condition,$where,$table_id),
						"data" => $data,
				);
		}
		elseif (isset($join_table) && isset($join_condition)) {
			$output = array(
						"draw" => $this->input->post('draw'),
						"recordsTotal" => $this->DT_model->count_all($table),
						"recordsFiltered" => $this->DT_model->count_filtered($table,$columns,$join_table,$join_condition,null,$table_id),
						"data" => $data,
				);	
		}
		elseif (isset($where)){
			$output = array(
						"draw" => $this->input->post('draw'),
						"recordsTotal" => $this->DT_model->count_all($table),
						"recordsFiltered" => $this->DT_model->count_filtered($table,$columns,null,null,$where,$table_id),
						"data" => $data,
				);	
		}
		else
		{
			$output = array(
						"draw" => $this->input->post('draw'),
						"recordsTotal" => $this->DT_model->count_all($table),
						"recordsFiltered" => $this->DT_model->count_filtered($table,$columns,null,null,null,$table_id),
						"data" => $data,
				);	
		}
		//output to json format
		echo json_encode($output);
	}
}
