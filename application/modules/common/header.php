<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<title>Millage</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">

<!-- favicon -->
<link rel="shortcut icon" type="image/png" href="<?php echo IMG_URL; ?>fav.jpg" />
<!-- GOOGLE FONT -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i%7CMr+Dafoe%7CPoppins:300,400,500,600,700" rel="stylesheet">

<!-- CSS LIBRARY -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font-awesome-animation.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>pe-icon-7-stroke.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>font.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>menu.css" />

<!-- Main css -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>responsive.css" />

<!-- color skin -->
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>color-skin/default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>toastr.min.css" />

<!-- <link rel="stylesheet" href="<?php echo CSS_URL; ?>AdminLTE.css"> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="<?php echo CSS_URL;?>bootstrap-datepicker.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>jquery.realperson.css"><link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>layouts-common.css"><link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>common.min.1.9.6.1.08fc6598aa074d503c93d2ae5936b2b186f77876.css"><script src="<?php echo JS_URL; ?>menu.js"></script> 


<script src="<?php echo JS_URL; ?>jquery-3.1.1.min.js"></script> 

<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style >
    .swal2-container{
      z-index: 1051 !important;
    }  
    .swal2-modal{
      height: 100% !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
      background-color: #da1048; 
      color: #FFF;
    }                   
  </style>
</head>

<body class="single-product">
<header class="home-header">      <div id="mySidenav" class="sidenav"><div class="rlx-nav__sub rlx-nav__sub--active"><div><button class=" rlx-nav__close"><a href="javascript:void(0)" onclick="closeNav()">Close the menu</a></button></div>     <ul class=" rlx-nav__sub-list">                    <li class="rlx-nav__section"><a href="<?php echo base_url($this->session->group_level.'/new_releases'); ?>">New Releases</a></li>          <li class="rlx-nav__section"><a href="<?php echo base_url($this->session->group_level.'/product_list'); ?>">The Collection</a></li>		  		  <?php foreach ($categories as  $value) {               // echo $this->session->group_level;            ?>          <li class="dropdown">            <a href="#" class="dropdown-toggle" ><?php echo $value->category_name ?> </a>            <ul class="sub-menu dropdown-menu">              <li><a href="<?php echo base_url($this->session->group_level.'/product_list/'.$value->category_id) ?>">All</a></li>              <?php foreach ($style as  $one_style) { ?>                <li><a href="<?php echo base_url($this->session->group_level.'/product_list/'.$value->category_id.'/'.$one_style->style_id) ?>"><?php echo $one_style->style_name; ?></a></li>              <?php } ?>              <!-- <li><a href="#">Sport</a></li>              <li><a href="#">Luxury</a></li> -->            </ul>          </li>          <?php } ?>		   <?php if (count($page_categories)>3) { ?>            <li class="dropdown"> <a href="#" class="dropdown-toggle">Collection</a>              <ul class="sub-menu dropdown-menu">                 <?php foreach ($page_categories as  $one) { ?>                  <li><a href="<?php echo base_url($this->session->group_level.'/page/'.$one->page_category_id); ?>"><?php echo $one->page_category_name ?></a></li>                <?php } ?>              </ul>            </li>          <?php }else{ ?>          <?php foreach ($page_categories as  $one) { ?>                  <li><a href="<?php echo base_url($this->session->group_level.'/page/'.$one->page_category_id); ?>"><?php echo $one->page_category_name ?></a></li>                <?php } ?>          <?php } ?>          <?php if ($this->session->group_level!="common" && $this->session->group_level!="admin") { ?>                        <!-- <li><a href="#" class="dropdown-toggle">My Account</a></li> -->          <?php } ?>          <li><a href="<?php echo base_url($this->session->group_level.'/contactus'); ?>">Contact Us</a></li>          <!-- <li class="mega-link dropdown"> <a href="#" class="dropdown-toggle">Dealers</a>              <ul class="mega-menu dropdown-menu">                <div class="mega-wrap row">                  <div class="col-sm-3 col-sm-offset-3">                    <li><a href="<?php echo base_url($this->session->group_level.'/apply/distributorship/usa'); ?>">USA Distributorship</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/apply/distributorship'); ?>">Distributorship</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/apply/dealership/usa'); ?>">USA Dealership</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/apply/dealership'); ?>">Dealership</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/search_dealer/dealer'); ?>">Search Dealer</a></li>                    <li><a href="<?php echo base_url($this->session->group_level.'/search_dealer/distributor'); ?>">Search Distributor</a></li>                  </div>                </div>              </ul>            </li> -->            <li class="mega-link dropdown">                            <a href="#" class="dropdown-toggle">Dealers</a>                            <div class="mega-menu dropdown-menu">                               <div class="mega-wrap row">                                   <div class="col-sm-6">                                       <h6 style="margin-left:75px !important">Distributorship</h6>                                       <ul>                                           <li><a href="<?php echo base_url($this->session->group_level.'/apply/distributorship/usa'); ?>">USA Distributorship</a></li>                                          <li><a href="<?php echo base_url($this->session->group_level.'/apply/distributorship'); ?>">Distributorship</a></li>                                           <li><a href="<?php echo base_url($this->session->group_level.'/search_dealer/distributor'); ?>">Search Distributor</a></li>                                       </ul>                                   </div>                                   <div class="col-sm-6">                                       <h6 style="margin-left:75px !important">Dealership</h6>                                       <ul>                                           <li><a href="<?php echo base_url($this->session->group_level.'/apply/dealership/usa'); ?>">USA Dealership</a></li>                                          <li><a href="<?php echo base_url($this->session->group_level.'/apply/dealership'); ?>">Dealership</a></li>                                           <li><a href="<?php echo base_url($this->session->group_level.'/search_dealer/dealer'); ?>">Search Dealer</a></li>                                       </ul>                                   </div>                                   <!--clothing-->                               </div>                             </div>                        </li>            <li><a  id="search_here">Search</a></li>        </ul>      </div>      <!--container-->     </div>              <!-- /.navbar-collapse -->     <div id="main">
<div class=row">
<br><br>
 <div class="col-sm-6">  <a href="javascript:void(0)" class="rlx-site-nav__openMenu" onclick="openNav()">  <span class="rlx-burger">							<span></span>							<span class="rlx-bacon"></span>							<span></span>							<span></span>						</span>                    <span class="rlx-hide-collapsed">MENU</span></a></div>					    <div class="col-sm-6">        <center><a class="navbar-brand" href="<?php echo base_url() ?>"> <img height="70px" width="70px" src="<?php echo UPLOAD_URL; ?>/site/<?php echo $site_logo ?>" class="hidden-xs" style="" alt="Logo"></a></center>           <h5 style="color: #946F29;" class="hidden-xs maintext">Millage</h6>    </div>
	</div>
	</div>


  
   
    <!-- /.navbar-collapse -->
    
  
</header>
<?php if(isset($this->session->user_id)){?><script src="<?php echo JS_URL; ?>menu.js"></script>  <script type="text/javascript">      var position = 0;      var mainPanel = document.getElementById("mainPanel");      var leftDrawer = document.getElementById("leftDrawer");      var rightDrawer = document.getElementById("rightDrawer");      function toggle(evt) {        position++;                 if (position % 3 == 1) {          leftDrawer.classList.add("open");          rightDrawer.classList.remove("open");        } else {          leftDrawer.classList.remove("open");          rightDrawer.classList.add("open");        }        }      mainPanel.addEventListener("click", toggle);      leftDrawer.addEventListener("click", toggle);      rightDrawer.addEventListener("click", toggle);          </script>	   <script>  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;  e=o.createElement(i);r=o.getElementsByTagName(i)[0];  e.src='//www.google-analytics.com/analytics.js';  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));  ga('create','UA-52746336-1');ga('send','pageview');  var isCompleted = {};  function sampleCompleted(sampleName){    if (ga && !isCompleted.hasOwnProperty(sampleName)) {      ga('send', 'event', 'WebCentralSample', sampleName, 'completed');       isCompleted[sampleName] = true;    }  }</script>    <script type="text/javascript">      function init() {        window.matchMedia("(min-width: 800px)").addListener(hitMQ);        window.matchMedia("(min-width: 500px)").addListener(hitMQ);      }      function hitMQ(evt) {        sampleCompleted("RWDPatterns-OffCanvas");      }      init();    </script>
<script type="text/javascript"> 
  $(".dropdown-menu > .widget_shopping_cart_content > .cart_list > .mini_cart_item > table > tbody > tr > .content > .product-info > li > .btn-xs ").click(function(event) {
    event.preventDefault();
    return false;
  });
  $(".dropdown-menu > .widget_shopping_cart_content > .cart_list > .mini_cart_item > table > tbody > tr > .content > .product-info > li > span > .btn-xs ").click(function(event) {
    $(this).each(function(index, el) {
      id=$(this).attr('id');
      class_name=$(this).attr('class').split(' ')[0];
      if (id=="btn_max") {

        var max=<?php echo $watch_option->dial_stock; ?>;
        var qty=$("."+class_name+"_qty_new_value").val();
        if(max>qty)
        {
            $("."+class_name+"_qty_new_value").val(parseInt($("."+class_name+"_qty_new_value").val())+1);
        }
      }
      if (id=="btn_min") {
        var qty=$("."+class_name+"_qty_new_value").val();
        if(qty>1)
        {
            $("."+class_name+"_qty_new_value").val($("."+class_name+"_qty_new_value").val()-1);
        }
      }
    });
    event.preventDefault();
    return false;
  });
</script>
<?php }?>